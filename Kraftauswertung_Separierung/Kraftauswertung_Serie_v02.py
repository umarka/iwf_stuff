"""
Script based on: "Auswertung_Prozeszkraefte_Zerspanungsversuche.py" @Hagen Klippel

Modified: 17.04.2022
@author: Fabian Kneubühler

Theme: Evaluation of process forces 

#-*- coding:utf-8 -*-
"""

from tkinter import Y
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.widgets import RectangleSelector
import os
import openpyxl
from openpyxl import load_workbook
from openpyxl import Workbook
import ctypes

############################################################
############################################################
# User input

# Data path for .dat files of force signal
path = r"D:\Noah ETHZ\Bachelorarbeit\Kraftmessdaten\20220502_nachmittag\686\DAT" # Path Windows
# path = r"/Users/fabiankneubuhler/Desktop/Kraftrauswertung/Python/Daten/Gruppe1" # Path Mac 

# Data path for Excel file 
filename_Excel = r"" # Path Windows

# Set programme execution style

# Overview, initial figure
InitialFigure = 0

# Drift compensation
DriftCompensationExe = 0

DritfCompensationAuto = 1
DriftCompensationAutoVisualisation = 0
ThresholdPargmFront = 2
ThresholdPargmBack = 2

DriftCompensationManuel = 0

# Evaluation style
AutoEvaluation = 1
AutoEvaluationVisualisation = 0

ManualEvaluation = 0

# Use Excel input
ExcelInput = 0

############################################################
############################################################
# General settings

# Float digits
float_formatter = "{:.3f}".format
np.set_printoptions(formatter={'float_kind': float_formatter})

# Plot settings
# Font size plots
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'axes.titlesize': 20})
# Line width
plt.rcParams["lines.linewidth"] = 2
# Figure size
plt.rcParams["figure.figsize"] = (16,9)
# Reset figure size
#plt.rcParams["figure.figsize"] = plt.rcParamsDefault["figure.figsize"] 

# https://matplotlib.org/users/usetex.html
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

############################################################
############################################################
# Load Excel file

if ExcelInput==1: 
    workbook = load_workbook(
        filename=filename_Excel, data_only=True)

    #sheet = workbook.active
    #sheet = workbook.get_sheet_by_name('Overview')
    MainSheet = workbook.worksheets[0]
    EvalDetails = workbook.worksheets[1]

# Select data of Excel file
# firstRow = 8
# firstCol = 1
# nCols = 30
# nRows = 12
#
# allCells = np.array([[cell.value for cell in row]
#                      for row in MainSheet.iter_rows()])
#
#
# data = allCells[(firstRow - 1):(firstRow - 1 + nRows),
#                 (firstCol - 1):(firstCol - 1 + nCols)]

############################################################
############################################################
# Create folder for figures
directory_fig_overview_png = os.path.join(path, 'Figures', 'PNG', '1_Overview')
directory_fig_overview_eps = os.path.join(path, 'Figures', 'EPS', '1_Overview')
directory_fig_overview_pdf = os.path.join(path, 'Figures', 'PDF', '1_Overview')

directory_fig_driftcompensationAuto_png = os.path.join(path, 'Figures', 'PNG', '2_DriftCompensation', 'Auto')
directory_fig_driftcompensationAuto_eps = os.path.join(path, 'Figures', 'EPS', '2_DriftCompensation', 'Auto')
directory_fig_driftcompensationAuto_pdf = os.path.join(path, 'Figures', 'PDF', '2_DriftCompensation', 'Auto')

directory_fig_driftcompensationManuel_png = os.path.join(path, 'Figures', 'PNG', '2_DriftCompensation', 'Manuel')
directory_fig_driftcompensationManuel_eps = os.path.join(path, 'Figures', 'EPS', '2_DriftCompensation', 'Manuel')
directory_fig_driftcompensationManuel_pdf = os.path.join(path, 'Figures', 'PDF', '2_DriftCompensation', 'Manuel')

directory_fig_ForceEvaluationCut_png = os.path.join(path, 'Figures', 'PNG', '3_ForceEvaluationCut')
directory_fig_ForceEvaluationCut_eps = os.path.join(path, 'Figures', 'EPS', '3_ForceEvaluationCut')
directory_fig_ForceEvaluationCut_pdf = os.path.join(path, 'Figures', 'PDF', '3_ForceEvaluationCut')

directory_fig_SingleForceEvaluationsAuto_png = os.path.join(path, 'Figures', 'PNG', '4_SingleForceEvaluations', 'Auto')
directory_fig_SingleForceEvaluationsAuto_eps = os.path.join(path, 'Figures', 'EPS', '4_SingleForceEvaluations', 'Auto')
directory_fig_SingleForceEvaluationsAuto_pdf = os.path.join(path, 'Figures', 'PDF', '4_SingleForceEvaluations', 'Auto')

directory_fig_SingleForceEvaluationsManuel_png = os.path.join(path, 'Figures', 'PNG', '4_SingleForceEvaluations', 'Manuel')
directory_fig_SingleForceEvaluationsManuel_eps = os.path.join(path, 'Figures', 'EPS', '4_SingleForceEvaluations', 'Manuel')
directory_fig_SingleForceEvaluationsManuel_pdf = os.path.join(path, 'Figures', 'PDF', '4_SingleForceEvaluations', 'Manuel')
# Create folder for evaluation files
directory_eval = os.path.join(path, 'EvaluationFiles')

# Create directories
if not os.path.exists(directory_fig_overview_png):
    os.makedirs(directory_fig_overview_png)
if not os.path.exists(directory_fig_overview_eps):
    os.makedirs(directory_fig_overview_eps)
if not os.path.exists(directory_fig_overview_pdf):
    os.makedirs(directory_fig_overview_pdf)

if not os.path.exists(directory_fig_driftcompensationAuto_png):
    os.makedirs(directory_fig_driftcompensationAuto_png)
if not os.path.exists(directory_fig_driftcompensationAuto_eps):
    os.makedirs(directory_fig_driftcompensationAuto_eps)
if not os.path.exists(directory_fig_driftcompensationAuto_pdf):
    os.makedirs(directory_fig_driftcompensationAuto_pdf)

if not os.path.exists(directory_fig_driftcompensationManuel_png):
    os.makedirs(directory_fig_driftcompensationManuel_png)
if not os.path.exists(directory_fig_driftcompensationManuel_eps):
    os.makedirs(directory_fig_driftcompensationManuel_eps)
if not os.path.exists(directory_fig_driftcompensationManuel_pdf):
    os.makedirs(directory_fig_driftcompensationManuel_pdf)

if not os.path.exists(directory_fig_SingleForceEvaluationsAuto_png):
    os.makedirs(directory_fig_SingleForceEvaluationsAuto_png)
if not os.path.exists(directory_fig_SingleForceEvaluationsAuto_eps):
    os.makedirs(directory_fig_SingleForceEvaluationsAuto_eps)
if not os.path.exists(directory_fig_SingleForceEvaluationsAuto_pdf):
    os.makedirs(directory_fig_SingleForceEvaluationsAuto_pdf)

if not os.path.exists(directory_fig_SingleForceEvaluationsManuel_png):
    os.makedirs(directory_fig_SingleForceEvaluationsManuel_png)
if not os.path.exists(directory_fig_SingleForceEvaluationsManuel_eps):
    os.makedirs(directory_fig_SingleForceEvaluationsManuel_eps)
if not os.path.exists(directory_fig_SingleForceEvaluationsManuel_pdf):
    os.makedirs(directory_fig_SingleForceEvaluationsManuel_pdf)

if not os.path.exists(directory_eval):
    os.makedirs(directory_eval)

# Generate temporary excel file with head for results
# Create a Workbook
WoorkbookResults = Workbook()
WorkSheetResults =  WoorkbookResults.active
WorkSheetResults.title = "Results"

WorkSheetResults.cell(row=1, column=1, value='Experiment')
WorkSheetResults.cell(row=1, column=2, value='AvgFc')
WorkSheetResults.cell(row=1, column=3, value='SigFc')
WorkSheetResults.cell(row=1, column=4, value='MaxFc')
WorkSheetResults.cell(row=1, column=5, value='MinFc')
WorkSheetResults.cell(row=1, column=6, value='AvgFf')
WorkSheetResults.cell(row=1, column=7, value='SigFf')
WorkSheetResults.cell(row=1, column=8, value='MaxFf')
WorkSheetResults.cell(row=1, column=9, value='MinFf')
WorkSheetResults.cell(row=1, column=10, value='AvgFp')
WorkSheetResults.cell(row=1, column=11, value='SigFp')
WorkSheetResults.cell(row=1, column=12, value='MaxFp')
WorkSheetResults.cell(row=1, column=13, value='MinFp')

# Determine incremented filename
FileIteratorTemp = 0

while os.path.exists(os.path.join(directory_eval,f"Temp_{FileIteratorTemp}.xlsx")):
    FileIteratorTemp += 1

WoorkbookResults.save(os.path.join(directory_eval,f'Temp_{FileIteratorTemp}.xlsx'))

############################################################
############################################################
############################################################
############################################################
# Main programme

Dateien = []

# Load all files in directory and sub directories
# r=root, d=directories, f = files
# (https://www.mkyong.com/python/python-how-to-list-all-files-in-a-directory/)
# for r, d, f in os.walk(path):
#     for Datei in f:
#         if ('.dat' in Datei) and ('ProzKraft' in Datei):
#             Dateien.append(os.path.join(r, Datei))

# Load all files in directory
for Datei in os.listdir(path):
    if ('.dat' in Datei) and ('ProzKraft' in Datei):
        Dateien.append(os.path.join(path, Datei))

Auswertegrenzen = Dateien

Experiments = len(Auswertegrenzen) * [0]
ExperimentsArr = np.zeros((len(Auswertegrenzen), 2), dtype='i')

# Initialize arrays
# Forces and standard deviations
F_Leerlauf_vorne = np.zeros((len(Auswertegrenzen), 3))
F_Leerlauf_hinten = np.zeros((len(Auswertegrenzen), 3))
Fc = np.zeros((len(Auswertegrenzen), 1))
Ff = np.zeros((len(Auswertegrenzen), 1))
Fp = np.zeros((len(Auswertegrenzen), 1))

# Initalise array size
AvgFc = np.zeros((len(Auswertegrenzen), 1))
AvgFf = np.zeros((len(Auswertegrenzen), 1))
AvgFp = np.zeros((len(Auswertegrenzen), 1))
SigFc = np.zeros((len(Auswertegrenzen), 1))
SigFf = np.zeros((len(Auswertegrenzen), 1))
SigFp = np.zeros((len(Auswertegrenzen), 1))
MaxFc = np.zeros((len(Auswertegrenzen), 1))
MaxFf = np.zeros((len(Auswertegrenzen), 1))
MaxFp = np.zeros((len(Auswertegrenzen), 1))
MinFc = np.zeros((len(Auswertegrenzen), 1))
MinFf = np.zeros((len(Auswertegrenzen), 1))
MinFp = np.zeros((len(Auswertegrenzen), 1))

# Gradient of linear drift compensation
m_drift = np.zeros((len(Auswertegrenzen), 3))
# Y axis intercept of linear drift compensation
q_drift = np.zeros((len(Auswertegrenzen), 3))
# Time points of linear drift compensation
TimeDrift = np.zeros((len(Auswertegrenzen), 8))

############################################################
############################################################
# Functions
def InitaliseData(Daten):
    Time = np.array(Daten[:, 0])
    # / Wandstaerke = normalisation of forces
    Fc = Daten[:, 1] / Wandstaerke  # Cutting force
    Ff = Daten[:, 3] / Wandstaerke  # Feed force
    Fp = Daten[:, 2] / Wandstaerke  # Passive force

    return Time, Fc, Ff, Fp


def linear_drift_compensation(TimeDrift_start, TimeDrift_end, F_Leerlauf_hinten, F_Leerlauf_vorne):
    m_drift = (F_Leerlauf_hinten - F_Leerlauf_vorne) / \
        (TimeDrift_end - TimeDrift_start)
    q_drift = F_Leerlauf_vorne - m_drift * TimeDrift_start
    return m_drift, q_drift


# Select points
def line_select_callback(eclick, erelease):
    # Returns the selected points via mouse click

    global index_xmax, index_xmin

    x1, y1 = eclick.xdata, eclick.ydata
    x2, y2 = erelease.xdata, erelease.ydata

    mask = (Time > min(x1, x2)) & (Time < max(x1, x2)) & (
        Fc > min(y1, y2)) & (Fc < max(y1, y2))
    xmasked = Time[mask]
    ymasked = Fc[mask]

    if len(ymasked) > 0.0:

        xmax[0] = xmasked.max()
        index_xmaxTest = np.where(Time == xmax[0])
        index_xmax = int(index_xmaxTest[0])
        xmax[1] = Fc[int(index_xmax)]

        xmin[0] = xmasked.min()
        index_xminTest = np.where(Time == xmin[0])
        index_xmin = int(index_xminTest[0])
        xmin[1] = Fc[int(index_xmin)]

        t_max = "xmax: {:.3f}, y(xmax) {:.3f}".format(xmax[0], xmax[1])
        t_min = "xmin: {:.3f}, y(ymin) {:.3f}".format(xmin[0], xmin[1])

        point_min.set_data((xmin[0], xmin[1]))
        point_max.set_data((xmax[0], xmax[1]))

        text_max.set_text(t_max)
        text_max.set_position(((xmax[0] + 0.25), xmax[1]))
        text_min.set_text(t_min)
        text_min.set_position(((xmin[0] - 5), xmin[1]))

        fig.canvas.draw_idle()


def Save_compensation(ZeileExcel, j):
    for ii in range(2, 10):
        EvalDetails.cell(row=ZeileExcel, column=ii,
                         value=TimeDrift[j, (ii - 2)])

    for ii in range(10, 13):
        EvalDetails.cell(row=ZeileExcel, column=ii,
                         value=F_Leerlauf_vorne[j, (ii - 10)])

    for ii in range(13, 16):
        EvalDetails.cell(row=ZeileExcel, column=ii,
                         value=F_Leerlauf_hinten[j, (ii - 13)])

    for ii in range(16, 19):
        EvalDetails.cell(row=ZeileExcel, column=ii,
                         value=m_drift[j, (ii - 16)])

    for ii in range(19, 22):
        EvalDetails.cell(row=ZeileExcel, column=ii,
                         value=q_drift[j, (ii - 19)])

    workbook.save(filename_Excel)
    print("Saved to Excel")


def Save_Forces(ZeileExcel, jj, index_TimeForce, TimeForce):
    MainSheet.cell(row=ZeileExcel, column=jj, value=AvgFc[j, i])
    MainSheet.cell(row=ZeileExcel, column=(jj + 1), value=SigFc[j, i])
    MainSheet.cell(row=ZeileExcel, column=(jj + 2), value=AvgFf[j, i])
    MainSheet.cell(row=ZeileExcel, column=(jj + 3), value=SigFf[j, i])
    MainSheet.cell(row=ZeileExcel, column=(jj + 4), value=AvgFp[j, i])
    MainSheet.cell(row=ZeileExcel, column=(jj + 5), value=SigFp[j, i])

    for ii in range(22, 26):
        EvalDetails.cell(row=ZeileExcel, column=(index_TimeForce + ii),
                         value=TimeForce[j, (index_TimeForce + ii - 22)])
    print("Saved to Excel")

    workbook.save(filename_Excel)


def Load_compensation(ZeileExcel, j):
    #sheet.cell(row=10, column=6).value
    for ii in range(2, 10):
        TimeDrift[j, (ii - 2)] = EvalDetails.cell(row=ZeileExcel,
                                                  column=ii).value

    for ii in range(10, 13):
        F_Leerlauf_vorne[j, (ii - 10)
                         ] = EvalDetails.cell(row=ZeileExcel, column=ii).value

    for ii in range(13, 16):
        F_Leerlauf_hinten[j, (ii - 13)
                          ] = EvalDetails.cell(row=ZeileExcel, column=ii).value

    for ii in range(16, 19):
        m_drift[j, (ii - 16)] = EvalDetails.cell(row=ZeileExcel,
                                                 column=ii).value

    for ii in range(19, 22):
        q_drift[j, (ii - 19)] = EvalDetails.cell(row=ZeileExcel,
                                                 column=ii).value

    return TimeDrift, m_drift, q_drift


def Cut(j, Fc, Ff, Fp, TimeDrift, Time):
    # Cut forces and time signal (drift start to drift end)
    Fc = np.array(Fc[int(TimeDrift[j, 3]):int(TimeDrift[j, 5])])
    # Feed force
    Ff = np.array(Ff[int(TimeDrift[j, 3]):int(TimeDrift[j, 5])])
    # Passive force
    Fp = np.array(Fp[int(TimeDrift[j, 3]):int(TimeDrift[j, 5])])
    Time = np.array(Time[int(TimeDrift[j, 3]):int(TimeDrift[j, 5])])  # Time
    return Fc, Ff, Fp, Time


def Compensate(j, Fc, Ff, Fp, Time, m_drift, q_drift):
    # Drift compensate forces
    Fc = Fc - (Time * m_drift[j, 0] + q_drift[j, 0])
    Ff = Ff - (Time * m_drift[j, 1] + q_drift[j, 1])
    Fp = Fp - (Time * m_drift[j, 2] + q_drift[j, 2])
    return Fc, Ff, Fp


j = -1  # Row index of data arrays (starting at 0)

for Datei in Dateien:
    j = j + 1
    # Read head of "ProzKraft.dat" file (number of experiment, ...)
    D2 = open(Datei, "r")
    Zeile = D2.readline()
    D2.close()

    # Find number of experiment
    VStart = Zeile.find(':') + 1
    VEnde = Zeile.find(' ')
    Versuchsnummer = Zeile[VStart:VEnde]
    CStart = Zeile.find(':', Zeile.find(':') + 1) + 1
    CEnd = Zeile.find('\n')
    Counter = Zeile[CStart:CEnd]
    Experiments[j] = str(Versuchsnummer + '_' + Counter)
    
    # Array for data storrage
    ExperimentsArr[j,0] = int(Versuchsnummer)
    ExperimentsArr[j,1] = int(Counter)

    if ExcelInput == 1:
        # Find row number of experiment in Excel file
        for cell in MainSheet['A']:
            #print(cell.value)
            # if cell.value == int(Versuchsnummer):
            if cell.value == Experiments[j]:
                ZeileExcel = cell.row

        # Find cutting speed
        vc = float(MainSheet.cell(row=ZeileExcel, column=17).value)

        # Find feed
        f = float(MainSheet.cell(row=ZeileExcel, column=12).value)

        # Find clearance angle
        alpha = float(MainSheet.cell(row=ZeileExcel, column=5).value)

        # Find rake angle
        gamma = float(MainSheet.cell(row=ZeileExcel, column=6).value)

        # Find Wandstaerke
        Wandstaerke = float(MainSheet.cell(row=ZeileExcel, column=20).value) * 1000

        # Find # force measurements
        Repetitions = int(MainSheet.cell(row=ZeileExcel, column=18).value)
        #Repetitions = 1
    else:
        vc =  "30"          #"\_"
        f = "0.05"            #"\_"
        alpha = "14"            #"\_"
        gamma =  "10"            #"\_"
        Wandstaerke = 1
        Repetitions = 1

    # Define title part
    title_part = str(Versuchsnummer) + '\_' + str(Counter) + ', $v_c=$ ' + str(vc) + ' [m/min]' + ', $f=$ ' + str(f) + ' [mm]' + r' $\alpha$=' + str(alpha) + ' [$^\circ$] ' + r'$\gamma$=' + str(gamma) + ' [$^\circ$]'
    title_part_save_format = str(Versuchsnummer) + r'_' + str(Counter) + r'_vc' + str(vc) + r'_f' + str(f) + r'_alpha' + str(alpha) + r'_gamma' + str(gamma)
    title_part_save_format=title_part_save_format.replace(".", r"_").replace("=",r"_").replace("\\", "").replace(" ", "")

    if Repetitions !=1:
        # Initalise array size depending on # Repetions
        AvgFc = np.zeros((len(Auswertegrenzen), Repetitions))
        AvgFf = np.zeros((len(Auswertegrenzen), Repetitions))
        AvgFp = np.zeros((len(Auswertegrenzen), Repetitions))
        SigFc = np.zeros((len(Auswertegrenzen), Repetitions))
        SigFf = np.zeros((len(Auswertegrenzen), Repetitions))
        SigFp = np.zeros((len(Auswertegrenzen), Repetitions))
        MaxFc = np.zeros((len(Auswertegrenzen), Repetitions))
        MaxFf = np.zeros((len(Auswertegrenzen), Repetitions))
        MaxFp = np.zeros((len(Auswertegrenzen), Repetitions))
        MinFc = np.zeros((len(Auswertegrenzen), Repetitions))
        MinFf = np.zeros((len(Auswertegrenzen), Repetitions))
        MinFp = np.zeros((len(Auswertegrenzen), Repetitions))
    # Time points of force evaluation
    TimeForce = np.zeros((len(Auswertegrenzen), (4 * Repetitions)))

    #
    Daten = np.loadtxt(Datei, skiprows=2)

    # Initialise data
    Time, Fc, Ff, Fp = InitaliseData(Daten)
    AnzWerte = Daten[:, 1].size

    # An area of the force signal can be selected and saved as initial figure
    if (InitialFigure != 0):

        # Idle forces in the front part
        fig, ax = plt.subplots(constrained_layout=True)
        ax.plot(Time, Fc[:], color="blue", linestyle="-",
                label=r'Cutting force')
        ax.plot(Time, Ff[:], color="red", linestyle="-",
                label=r'Feed force')
        ax.plot(Time, Fp[:], color="green", linestyle="-", label=r'Passive force')

        # Plot settings
        ax.set_xlabel(r'Time [$s$]')
        ax.set_ylabel(r'Force [N/mm]')
        ax.set_title(r'Raw data of experiment ' + title_part)
        ax.legend(loc="lower left")

        # Initiation of interactively selected points
        point_max, = ax.plot([], [], marker="o", color="crimson")
        point_min, = ax.plot([], [], marker="o", color="crimson")
        text_max = ax.text(0, 0, "")
        text_min = ax.text(0, 0, "")

        xmax = [0.0, 0.0]
        xmin = [0.0, 0.0]

        rsa = RectangleSelector(ax, line_select_callback,
                                drawtype='box', useblit=True, button=[1],
                                minspanx=0, minspany=0, spancoords='data',
                                interactive=True)
        
        ctypes.windll.user32.MessageBoxW(0, "Select area of interest with the cursor and close the window", "Instructions", 1)

        plt.show(block=True)
        plt.close()

        # Save selected values as first boundaries of force evaluation
        BeginnMittelung = xmin[0]
        EndeMittelung = xmax[0]

        # Zoomed area of force evaluation
        ZoomBeginn = BeginnMittelung
        ZoomEnde = EndeMittelung
        # Find index of Zoom range
        index_ZoomBeginn = np.where(Time == BeginnMittelung)
        index_ZoomBeginn = int(index_ZoomBeginn[0])
        index_ZoomEnde = np.where(Time == EndeMittelung)
        index_ZoomEnde = int(index_ZoomEnde[0])

        # Plot of zoomed area
        fig, bx = plt.subplots(constrained_layout=True)
        plt.xlim(ZoomBeginn, ZoomEnde)  # Grenzen (min/max) X-Achse
        bx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Fc[index_ZoomBeginn:index_ZoomEnde],
                color="blue", linestyle="-", label=r'Cutting force')
        bx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Ff[index_ZoomBeginn:index_ZoomEnde],
                color="red",  linestyle="-", label=r'Feed force')
        bx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Fp[index_ZoomBeginn:index_ZoomEnde],
                color="green",  linestyle="-", label=r'Passive force')

        # Plot settings
        bx.legend(loc="lower center")
        plt.xlabel(r'Time [$s$]')
        plt.ylabel(r'Force [N/mm]')

        bx.set_title(r'Croped signal of experiment ' + title_part)
        
        ctypes.windll.user32.MessageBoxW(0, "The croped signal will be shown. Close the window to continue.", "Instructions", 1)

        plt.savefig(os.path.join(directory_fig_overview_png, 'CropedSignal_' + title_part_save_format + ".png"), dpi=300)
        plt.savefig(os.path.join(directory_fig_overview_eps, 'CropedSignal_' + title_part_save_format + ".eps"), dpi=300)
        plt.savefig(os.path.join(directory_fig_overview_pdf, 'CropedSignal_' + title_part_save_format + ".pdf"), dpi=300)

        plt.show(block=True)
        plt.close()

    if (DritfCompensationAuto !=0):

        # First, the signal begining and ending is found.
        # The data points in the beginning and ending are approximated by a linear function.
        # This function is shifted in positive y direction by a threshold, which depends on the max value in the initial interval and the ThresholdPargm.
        # A value is searched that exceeds in the beginning and in the end the threshold value.
        # An offset is used from the threshold point and the average idle force for the drift compensations is averaged of a preset duration (DurationDriftForceAveraging).
        # The signal is croped and compensated.

        # Find a rought starting and ending point of the signal (10% of max value)
        RoughtStartPointSignal = np.argwhere((Fc)>(0.2*np.max(Fc)))[0][0]
        RoughtEndPointSignal = np.size(Fc) - np.argwhere((Fc[::-1])>(0.2*np.max(Fc[::-1])))[0][0]

        # Set offsets for drift compensation based on percentage values of run-in period before and after the signal
        TimeOffSetDriftForce = int(0.005*RoughtStartPointSignal)
        DurationDriftForceAveragingFront =int(0.1*RoughtStartPointSignal)
        DurationDriftForceAveragingBack =int(0.1*RoughtEndPointSignal)

        # Initial signal intervals
        StartInterval = range(0, int(0.5*RoughtStartPointSignal))
        EndInterval = range((np.size(Fc)-int(0.5*(np.size(Fc)-RoughtEndPointSignal))),np.size(Fc))

        # Define linear function for the beginning and the ending of the signal
        Coef_FcFront = np.polyfit(Time[StartInterval], Fc[StartInterval], 1)
        Poly1d_FcFront = np.poly1d(Coef_FcFront)
        Coef_FcBack = np.polyfit(Time[EndInterval], Fc[EndInterval], 1)
        Poly1d_FcBack = np.poly1d(Coef_FcBack)

        # Find the maximum deviation from the mean value in the intial interval
        F_Leerlauf_vorne_DeviationFc = np.max(Fc[StartInterval])-np.mean(Fc[StartInterval])
        F_Leerlauf_hinten_DeviationFc = np.max(Fc[EndInterval])-np.mean(Fc[EndInterval])

        # Threshold before and after signal
        ThresholdVorneFc = np.abs(ThresholdPargmFront*F_Leerlauf_vorne_DeviationFc)
        ThresholdHintenFc = np.abs(ThresholdPargmBack*F_Leerlauf_hinten_DeviationFc)
 
        # Define shifted linear function for the begninning and ending of the signalof
        Poly1d_FcFrontThreshold = np.poly1d([Coef_FcFront[0], (Coef_FcFront[1]+ThresholdVorneFc)])
        Poly1d_FcBackThreshold = np.poly1d([Coef_FcBack[0], (Coef_FcBack[1]+ThresholdHintenFc)])      

        #StartPointFc = np.argwhere((Fc[:])>(ThresholdVorneFc))[0][0]
        StartPointFc = np.argwhere((Fc)>(Poly1d_FcFrontThreshold(Time)))[0][0]
        EndPointFc = np.size(Fc) - np.argwhere((Fc[::-1])>(Poly1d_FcBackThreshold(Time[::-1])))[0][0]

        # Before measurement: Time start point for force averaging
        TimeDrift[j, 0] = Time[StartPointFc-TimeOffSetDriftForce-DurationDriftForceAveragingFront]
        # Before measurement: Index start point for force averaging
        TimeDrift[j, 1] = StartPointFc-TimeOffSetDriftForce-DurationDriftForceAveragingFront
        # Before measurement: Time end point for force averaging
        TimeDrift[j, 2] = Time[StartPointFc-TimeOffSetDriftForce]
        # Before measurement: Index end point for force averaging
        TimeDrift[j, 3] = StartPointFc-TimeOffSetDriftForce
        # After measurement: Time start point for force averaging
        TimeDrift[j, 4] = Time[EndPointFc+TimeOffSetDriftForce]
        # After measurement: Index start point for force averaging
        TimeDrift[j, 5] = EndPointFc+TimeOffSetDriftForce
        # After measurement: Time end point for force averaging
        TimeDrift[j, 6] = Time[EndPointFc+TimeOffSetDriftForce+DurationDriftForceAveragingBack]
        # After measurement: Index end point for force averaging
        TimeDrift[j, 7] = EndPointFc+TimeOffSetDriftForce+DurationDriftForceAveragingBack

        # # Check points for drift compensation if needed
        # fig, ax = plt.subplots(constrained_layout=True)
        # ax.plot(Time, Fc[:], color="blue", linestyle="-",
        #         label=r'Cutting force')
        # ax.plot(Time, Ff[:], color="red", linestyle="-",
        #         label=r'Feed force')
        # ax.plot(Time, Fp[:], color="green", linestyle="-", label=r'Passive force')

        # # Plot settings
        # ax.set_xlabel(r'Time [$s$]')
        # ax.set_ylabel(r'Force [N/mm]')
        # ax.set_title(r'Raw data of experiment ' + title_part)
        # ax.legend(loc="upper left")

        # # Rough start point for signal beginning and ending
        # ax.plot(Time[RoughtStartPointSignal],Fc[RoughtStartPointSignal], marker="o", color="k")
        # ax.plot(Time[RoughtEndPointSignal],Fc[RoughtEndPointSignal], marker="o", color="k")

        # # Boundaries for front drift compensation
        # ax.plot(TimeDrift[j, 0],Fc[int(TimeDrift[j, 1])], marker="o", color="k")
        # ax.plot(TimeDrift[j, 2],Fc[int(TimeDrift[j, 3])], marker="o", color="k")
        
        # # Boundaries for back drift compensation
        # ax.plot(TimeDrift[j, 4],Fc[int(TimeDrift[j, 5])], marker="o", color="k")
        # ax.plot(TimeDrift[j, 6],Fc[int(TimeDrift[j, 7])], marker="o", color="k")

        # # Threshold lines
        # ax.plot(Time[0:int(np.size(Time)/3)],Poly1d_FcFrontThreshold(Time[0:int(np.size(Time)/3)]), '--k')
        # ax.plot(Time[int(np.size(Time)/3*2):int(np.size(Time))],Poly1d_FcBackThreshold(Time[int(np.size(Time)/3*2):int(np.size(Time))]), '--k')

        # plt.show(block=True)
        # plt.close()

        # Calculate average drift forces
        F_Leerlauf_vorne[j, 0] = np.mean(
            Fc[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])
        F_Leerlauf_vorne[j, 1] = np.mean(
            Ff[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])
        F_Leerlauf_vorne[j, 2] = np.mean(
            Fp[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])
        
        F_Leerlauf_hinten[j, 0] = np.mean(
            Fc[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])
        F_Leerlauf_hinten[j, 1] = np.mean(
            Ff[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])
        F_Leerlauf_hinten[j, 2] = np.mean(
            Fp[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])
        
        # Crop signal
        Fc, Ff, Fp, Time = Cut(j, Fc, Ff, Fp, TimeDrift, Time)

        # Linear compensation function
        m_drift, q_drift = linear_drift_compensation(
            TimeDrift[j, 2], TimeDrift[j, 4], F_Leerlauf_hinten, F_Leerlauf_vorne)

        Fc_uncompensated = Fc
        Ff_uncompensated = Ff
        Fp_uncompensated = Fp

        # Apply drift compensation
        Fc, Ff, Fp = Compensate(j, Fc, Ff, Fp, Time, m_drift, q_drift)

        # Plot of drift compensation
        fig, ex = plt.subplots(constrained_layout=True)
        ex.plot(Time, Fc, color="blue", 
                linestyle="-", label=r'Cutting force')
        ex.plot(Time, Fc_uncompensated, color="blue", 
                linestyle="--", label=r'Cutting force (uncompensated)')
        ex.plot(Time, Ff, color="red", 
                linestyle="-", label=r'Feed force')
        ex.plot(Time, Ff_uncompensated, color="red", 
                linestyle="--", label=r'Feed force (uncompensated)')
        ex.plot(Time, Fp, color="green", 
                linestyle="-", label=r'Passive force')
        ex.plot(Time, Fp_uncompensated, color="green", 
                linestyle="--", label=r'Passive force (uncompensated)')

        # ex.plot(Time, (Time * m_drift[j, 0] + q_drift[j, 0]), color="blue",
        #         linewidth=2.0, linestyle="--", label=r'Drift')
        # ex.plot(Time[:], (Time[:] * m_drift[j, 1] + q_drift[j, 1]),
        #         color="red", linewidth=2.0, linestyle="--", label=r'Drift')
        # ex.plot(Time[:], (Time[:] * m_drift[j, 2] + q_drift[j, 2]),
        #         color="green", linewidth=2.0, linestyle="--", label=r'Drift')

        # Plot settings
        ex.set_xlabel(r'Time [$s$]')
        ex.set_ylabel(r'Force [N/mm]')
        ex.set_title(r'Drift compensation: Experiment ' + title_part)
        ex.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        plt.grid()

        plt.savefig(os.path.join(directory_fig_driftcompensationAuto_eps, "DriftCompensationAuto_" + title_part_save_format + '.eps'), dpi=300, bbox_inches='tight')
        plt.savefig(os.path.join(directory_fig_driftcompensationAuto_png, "DriftCompensationAuto_" + title_part_save_format + ".png"), dpi=300, bbox_inches='tight')
        plt.savefig(os.path.join(directory_fig_driftcompensationAuto_pdf, "DriftCompensationAuto_" + title_part_save_format + ".pdf"), dpi=300, bbox_inches='tight')

        if (DriftCompensationAutoVisualisation !=0):
            plt.show(block=True)
        plt.close()
   
    # Choose interactively forces for drift compensation
    if (DriftCompensationManuel != 0):

        # Idle forces in the front part
        fig, cx = plt.subplots(constrained_layout=True)
        cx.plot(Time, Fc[:], color="blue",  linestyle="-",
                label=r'Cutting force')
        cx.plot(Time, Ff[:], color="red",  linestyle="-",
                label=r'Feed force')
        cx.plot(Time, Fp[:], color="green",
                 linestyle="-", label=r'Passive force')

        # Plot settings
        cx.set_xlabel(r'Time [$s$]')
        cx.set_ylabel(r'Force [N/mm]')
        cx.set_title(r'Idle force front, Experiment ' + title_part)
        cx.legend(loc="lower left")

        ctypes.windll.user32.MessageBoxW(0, "Select area for drift compensation -> BEFORE signal rise. Close the window to continue.", 1)

        # Initiation of interactively selected points
        point_max, = cx.plot([], [], marker="o", color="crimson")
        point_min, = cx.plot([], [], marker="o", color="crimson")
        text_max = cx.text(0, 0, "")
        text_min = cx.text(0, 0, "")
        xmax = [0.0, 0.0]
        xmin = [0.0, 0.0]

        rsb = RectangleSelector(cx, line_select_callback,
                                drawtype='box', useblit=True, button=[1],
                                minspanx=0, minspany=0, spancoords='data',
                                interactive=True)
        plt.show(block=True)
        plt.close()

        # Stabel = input(('Is the force signal of experiment ' +
        #                 str(Versuchsnummer) + '_' + str(Counter) + ' stable? [y/n] '))

        # if Stabel == "N" or Stabel == "n":
        #     MainSheet.cell(row=ZeileExcel, column=26, value='Nein')
        #     workbook.save(filename_Excel)
        #     print(
        #         ('Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + ' is skipped and not evaluated!'))
        #     continue
        # elif Stabel == "y" or Stabel == "y":
        #     MainSheet.cell(row=ZeileExcel, column=26, value='Ja')
        #     workbook.save(filename_Excel)
        #     print("Drift evaluation is continued!")
        # else:
        #     print(
        #         ('Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + ' is skipped and not evaluated!'))
        #     continue

        # Save selected values and time as first boundaries of force evaluation

        # Before measurement: Time start point for force averaging
        TimeDrift[j, 0] = Time[int(index_xmin)]
        # Before measurement: Index start point for force averaging
        TimeDrift[j, 1] = int(index_xmin)
        #TimeDrift[j, 1] = int(index_xmin)
        # Before measurement: Time end point for force averaging
        TimeDrift[j, 2] = Time[int(index_xmax)]
        # Before measurement: Index end point for force averaging
        TimeDrift[j, 3] = int(index_xmax)

        F_Leerlauf_vorne[j, 0] = np.mean(
            Fc[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])
        F_Leerlauf_vorne[j, 1] = np.mean(
            Ff[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])
        F_Leerlauf_vorne[j, 2] = np.mean(
            Fp[int(TimeDrift[j, 1]):int(TimeDrift[j, 3])])

        # Idle forces in the back part:
        fig, dx = plt.subplots(constrained_layout=True)
        dx.plot(Time, Fc[:], color="blue",  linestyle="-",
                label=r'Cutting force')
        dx.plot(Time, Ff[:], color="red",  linestyle="-",
                label=r'Feed force')
        dx.plot(Time, Fp[:], color="green",
                 linestyle="-", label=r'Passive force')

        # Plot settings
        dx.set_xlabel(r'Time [$s$]')
        dx.set_ylabel(r'Force [N/mm]')
        dx.set_title(r'Idle force back, Experiment ' + title_part)
        dx.legend(loc="lower left")

        ctypes.windll.user32.MessageBoxW(0, "Select are for drift compensation -> AFTER signal fall. Close the window to continue.", 1)

        # Initiation of interactively selected points
        point_max, = dx.plot([], [], marker="o", color="crimson")
        point_min, = dx.plot([], [], marker="o", color="crimson")
        text_max = dx.text(0, 0, "")
        text_min = dx.text(0, 0, "")
        xmax = [0.0, 0.0]
        xmin = [0.0, 0.0]

        rsc = RectangleSelector(dx, line_select_callback,
                                drawtype='box', useblit=True, button=[1],
                                minspanx=0, minspany=0, spancoords='data',
                                interactive=True)
        plt.show(block=True)
        plt.close()

        # Save selected values and time as second boundaries of force
        # evaluation
        # After measurement: Time start point for force averaging
        TimeDrift[j, 4] = Time[int(index_xmin)]
        # After measurement: Index start point for force averaging
        TimeDrift[j, 5] = int(index_xmin)
        # After measurement: Time end point for force averaging
        TimeDrift[j, 6] = Time[int(index_xmax)]
        # After measurement: Index end point for force averaging
        TimeDrift[j, 7] = int(index_xmax)

        F_Leerlauf_hinten[j, 0] = np.mean(
            Fc[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])
        F_Leerlauf_hinten[j, 1] = np.mean(
            Ff[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])
        F_Leerlauf_hinten[j, 2] = np.mean(
            Fp[int(TimeDrift[j, 5]):int(TimeDrift[j, 7])])

        # Linear compensation function
        m_drift, q_drift = linear_drift_compensation(
            TimeDrift[j, 2], TimeDrift[j, 4], F_Leerlauf_hinten, F_Leerlauf_vorne)

        # Save to Excel
        # if EvalDetails.cell(row=ZeileExcel, column=2).value == None:
        #     Save_compensation(ZeileExcel, j)

        # else:
        #     Override = input(
        #         ('Do you want to override the drift compensation of experiment ' + str(Versuchsnummer) + "_" + str(Counter) + '? [y/n] '))
        #     if Override == "N" or Override == "n":
        #         TimeDrift, m_drift, q_drift = Load_compensation(ZeileExcel, j)
        #     else:
        #         Save_compensation(ZeileExcel, j)

        Fc, Ff, Fp, Time = Cut(j, Fc, Ff, Fp, TimeDrift, Time)

        Fc_uncompensated = Fc
        Ff_uncompensated = Ff
        Fp_uncompensated = Fp

        Fc, Ff, Fp = Compensate(j, Fc, Ff, Fp, Time, m_drift, q_drift)

        # Plot of drift compensation
        fig, ex = plt.subplots(constrained_layout=True)
        ex.plot(Time, Fc, color="blue", 
                linestyle="-", label=r'Cutting force')
        ex.plot(Time, Fc_uncompensated, color="blue", 
                linestyle="--", label=r'Cutting force (uncompensated)')
        ex.plot(Time, Ff, color="red", 
                linestyle="-", label=r'Feed force')
        ex.plot(Time, Ff_uncompensated, color="red", 
                linestyle="--", label=r'Feed force (uncompensated)')
        ex.plot(Time, Fp, color="green", 
                linestyle="-", label=r'Passive force')
        ex.plot(Time, Fp_uncompensated, color="green", 
                linestyle="--", label=r'Passive force (uncompensated)')

        # ex.plot(Time, (Time * m_drift[j, 0] + q_drift[j, 0]), color="blue",
        #         linewidth=2.0, linestyle="--", label=r'Drift')
        # ex.plot(Time[:], (Time[:] * m_drift[j, 1] + q_drift[j, 1]),
        #         color="red", linewidth=2.0, linestyle="--", label=r'Drift')
        # ex.plot(Time[:], (Time[:] * m_drift[j, 2] + q_drift[j, 2]),
        #         color="green", linewidth=2.0, linestyle="--", label=r'Drift')

        # Plot settings
        ex.set_xlabel(r'Time [$s$]')
        ex.set_ylabel(r'Force [N/mm]')
        ex.set_title(r'Drift compensation: Experiment ' + title_part)
        ex.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        plt.grid()

        plt.savefig(os.path.join(directory_fig_driftcompensationManuel_eps, "DriftCompensationManual" + "_" + Versuchsnummer + "_" + Counter + ".eps"), dpi=300, bbox_inches='tight')
        plt.savefig(os.path.join(directory_fig_driftcompensationManuel_png, "DriftCompensationManual" + "_" + Versuchsnummer + "_" + Counter + ".png"), dpi=300, bbox_inches='tight')
        plt.savefig(os.path.join(directory_fig_driftcompensationManuel_pdf, "DriftCompensationManual" + "_" + Versuchsnummer + "_" + Counter + ".pdf"), dpi=300, bbox_inches='tight')

        plt.show(block=True)
        plt.close()

    for i in range(int(Repetitions)):

        # Evaluates force signal between automatically found boundaries
        if (AutoEvaluation != 0):

            StartPointFc = np.argwhere((Fc[:])>(0.1*np.max((Fc[:]))))[0][0]
            EndPointFc = np.size(Fc) - np.argwhere((Fc[::-1])>(0.1*np.max((Fc[:]))))[0][0]

            # Next first highest point
            for ii in range(StartPointFc, EndPointFc):
                if Fc[ii+1] < Fc[ii]: 
                    StartPointUpFc = ii
                    break

            # Next last highest point
            for ii in range(EndPointFc, StartPointFc, -1):
                if Fc[ii+1] > Fc[ii]: 
                    EndPointUpFc = ii
                    break
            
            # Calculate average force, standard deviation, max and min forces
            AvgFc[j, i] = np.round(np.mean(Fc[StartPointUpFc:EndPointUpFc]),1)
            SigFc[j, i] = np.round(np.std(Fc[StartPointUpFc:EndPointUpFc]),2)
            AvgFf[j, i] = np.round(np.mean(Ff[StartPointUpFc:EndPointUpFc]),1)
            SigFf[j, i] = np.round(np.std(Ff[StartPointUpFc:EndPointUpFc]),2)
            AvgFp[j, i] = np.round(np.mean(Fp[StartPointUpFc:EndPointUpFc]),1)
            SigFp[j, i] = np.round(np.std(Fp[StartPointUpFc:EndPointUpFc]),2)
            MaxFc[j, i] = np.round(np.max(Fc[StartPointUpFc:EndPointUpFc]),1)
            MaxFf[j, i] = np.round(np.max(Ff[StartPointUpFc:EndPointUpFc]),1)
            MaxFp[j, i] = np.round(np.max(Fp[StartPointUpFc:EndPointUpFc]),1)
            MinFc[j, i] = np.round(np.min(Fc[StartPointUpFc:EndPointUpFc]),1)
            MinFf[j, i] = np.round(np.min(Ff[StartPointUpFc:EndPointUpFc]),1)
            MinFp[j, i] = np.round(np.min(Fp[StartPointUpFc:EndPointUpFc]),1)

            # Plot evaluations
            fig, ax = plt.subplots(constrained_layout=True)
            ax.plot(Time, Fc[:], color="blue", linestyle="-",
                    label=r'Cutting force', zorder=2)
            ax.plot(Time, Ff[:], color="red", linestyle="-",
                    label=r'Feed force', zorder=2)
            ax.plot(Time, Fp[:], color="green", linestyle="-", label=r'Passive force', zorder=2)

            ax.plot(Time[StartPointUpFc],Fc[StartPointUpFc], marker="o", color="crimson")
            ax.plot(Time[EndPointUpFc],Fc[EndPointUpFc], marker="o", color="crimson")
            ax.plot(Time[StartPointUpFc],Ff[StartPointUpFc], marker="o", color="crimson")
            ax.plot(Time[EndPointUpFc],Ff[EndPointUpFc], marker="o", color="crimson")
            ax.plot(Time[StartPointUpFc],Fp[StartPointUpFc], marker="o", color="crimson")
            ax.plot(Time[EndPointUpFc],Fp[EndPointUpFc], marker="o", color="crimson")

            ax.hlines(y = AvgFc[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="-", zorder=8)
            ax.hlines(y = AvgFc[j, i]+SigFc[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)
            ax.hlines(y = AvgFc[j, i]-SigFc[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)

            ax.hlines(y = AvgFf[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="-", zorder=8)
            ax.hlines(y = AvgFf[j, i]+SigFf[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)
            ax.hlines(y = AvgFf[j, i]-SigFf[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)

            ax.hlines(y = AvgFp[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="-", zorder=8)
            ax.hlines(y = AvgFp[j, i]+SigFp[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)
            ax.hlines(y = AvgFp[j, i]-SigFp[j, i], xmin = Time[StartPointUpFc], xmax = Time[EndPointUpFc], color = 'k', linestyle="--", zorder=8)

            text_AvgFc = ax.text(0, 0, "")
            text_SigFc = ax.text(0, 0, "")
            text_AvgFf = ax.text(0, 0, "")
            text_SigFf = ax.text(0, 0, "")
            text_AvgFp = ax.text(0, 0, "")
            text_SigFp = ax.text(0, 0, "")

            t_AvgFc = "Avgerage cutting force: {:.1f} N, SD {:.2f} N, max: {:.1f} N, min: {:.1f} N".format(AvgFc[j, i], SigFc[j, i], MaxFc[j, i],  MinFc[j, i])
            t_AvgFf = "Average feed force: {:.1f} N, SD {:.2f} N, max: {:.1f} N, min: {:.1f} N".format(AvgFf[j, i], SigFf[j, i], MaxFf[j, i],  MinFf[j, i])
            t_AvgFp = "Average passive force: {:.1f} N, SD {:.2f} N, max: {:.1f} N, min: {:.1f} N".format(AvgFp[j, i], SigFp[j, i], MaxFp[j, i],  MinFp[j, i])

            text_AvgFc.set_text(t_AvgFc)
            text_AvgFc.set_position((((Time[EndPointUpFc] - Time[StartPointUpFc]) / 5 + Time[StartPointUpFc]), MaxFc[j, i] + 0 * SigFc[j, i]))
            #text_AvgFc.set_fontsize(14)
            text_AvgFc.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))
            text_AvgFc.set_zorder(10)

            text_AvgFf.set_text(t_AvgFf)
            text_AvgFf.set_position((((Time[EndPointUpFc] - Time[StartPointUpFc]) / 5 + Time[StartPointUpFc]), MaxFf[j, i] + 0 * SigFf[j, i]))
            #text_AvgFf.set_fontsize(14)
            text_AvgFf.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))
            text_AvgFf.set_zorder(10)

            text_AvgFp.set_text(t_AvgFp)
            text_AvgFp.set_position((((Time[EndPointUpFc] - Time[StartPointUpFc]) / 5 + Time[StartPointUpFc]), MaxFp[j, i] + 0 * SigFp[j, i]))
            #text_AvgFp.set_fontsize(14)
            text_AvgFp.set_bbox(dict(facecolor='white', alpha=1, edgecolor='black'))
            text_AvgFp.set_zorder(10)

            # Plot settings
            ax.set_xlabel(r'Time [$s$]')
            ax.set_ylabel(r'Force [N/mm]')
            ax.set_title(r'Force evaluation of experiment ' + title_part)
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            plt.grid()

            plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsAuto_png, "ForceEvaluation_" + title_part_save_format + str(i + 1) + ".png"), dpi=300)
            plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsAuto_eps, "ForceEvaluation_" + title_part_save_format + str(i + 1) + ".eps"), dpi=300)
            plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsAuto_pdf, "ForceEvaluation_" + title_part_save_format + str(i + 1) + ".pdf"), dpi=300)

            if (AutoEvaluationVisualisation !=0):
                plt.show(block=True)
            plt.close()

            # Save data to Excel
            WorkSheetResults.cell(row=j+2, column=1, value=Experiments[j])
            WorkSheetResults.cell(row=j+2, column=2, value=AvgFc[j, i])
            WorkSheetResults.cell(row=j+2, column=3, value=SigFc[j, i])
            WorkSheetResults.cell(row=j+2, column=4, value=MaxFc[j, i])
            WorkSheetResults.cell(row=j+2, column=5, value=MinFc[j, i])

            WorkSheetResults.cell(row=j+2, column=6, value=AvgFf[j, i])
            WorkSheetResults.cell(row=j+2, column=7, value=SigFf[j, i])
            WorkSheetResults.cell(row=j+2, column=8, value=MaxFf[j, i])
            WorkSheetResults.cell(row=j+2, column=9, value=MinFf[j, i])

            WorkSheetResults.cell(row=j+2, column=10, value=AvgFp[j, i])
            WorkSheetResults.cell(row=j+2, column=11, value=SigFp[j, i])
            WorkSheetResults.cell(row=j+2, column=12, value=MaxFp[j, i])
            WorkSheetResults.cell(row=j+2, column=13, value=MinFp[j, i])

            WoorkbookResults.save(os.path.join(directory_eval,f"Temp_{FileIteratorTemp}.xlsx"))

            print("Experiment " + Versuchsnummer + "_" + Counter + " saved to Excel file!")

            # Save data to txt file
            #Head = str('Experiment, Cutting force F_c [N/mm], SD F_c [N/mm], Feed force F_f [N/mm], SD F_f [N/mm], Passive force F_p [N/mm], SD F_p [N/mm], TimeFront1, IndTimeFront1, TimeFront2, IndTimeFront2, TimeBack1, IndTimeBack1, TimeBack2, IndTimeBack2, F_Leerlauf_vorne[0], F_Leerlauf_vorne[1], F_Leerlauf_vorne[2], F_Leerlauf_hinten[0], F_Leerlauf_hinten[1], F_Leerlauf_hinten[2], m_drift[0], m_drift[1], m_drift[2], q_drift[0], q_drift[1], q_drift[2], TimeForce')
            #DataToSave = np.column_stack((Experiments[j], [AvgFc[j, :]], [SigFc[j, :]], [AvgFf[j, :]], [SigFf[j, :]], [AvgFp[j, :]], [SigFp[j, :]], [F_Leerlauf_vorne[j, :]], [F_Leerlauf_hinten[j, :]], [m_drift[j, :]], [q_drift[j, :]], [TimeDrift[j, :]], [TimeForce[j, :]]))

            #Datafile_path = os.path.join(directory_eval, "ProcessForceEvaluation_" + Versuchsnummer + "_" + Counter)

            #np.savetxt(Datafile_path+".txt", DataToSave, fmt='%s', delimiter=' ', header=Head)
            #print("Experiment " + Versuchsnummer + "_" + Counter + " saved as txt file!")

            # Save data as python array
            #np.save(Datafile_path, DataToSave)
            #print("Experiment " + Versuchsnummer + "_" + Counter + " saved as python array!")


        # Evaluates force signal between manually found boundaries
        if (ManualEvaluation != 0):

            # Choose interactively borders for force evaluation
            fig, ax = plt.subplots(constrained_layout=True)
            plt.xlim(TimeDrift[j, 2], TimeDrift[j, 4])
            ax.plot(Time, Fc, color="blue",  linestyle="-",
                    label=r'Cutting force')  # Korrigierte Zuordnung der Prozeßkräfte, HK, Fr, 05.06.2020
            ax.plot(Time, Ff, color="red",  linestyle="-",
                    label=r'Feed force')  # Korrigierte Zuordnung der Prozeßkräfte, HK, Fr, 05.06.2020
            ax.plot(Time, Fp, color="green",
                    linestyle="-", label=r'Passive force')

            # Plot settings
            ax.set_xlabel(r'Time [$s$]')
            ax.set_ylabel(r'Force [N/mm]')
            ax.set_title(r'Process forces, Experiment ' + title_part)
            ax.legend(loc="lower center")

            # User instructions
            ctypes.windll.user32.MessageBoxW(0, "Select force measurement no." + str(i + 1) + " OR close the window twice if the signal is not stable! Start from the LEFT side (=1) and continue to the RIGHT side. Close the window to continue.","Instructions", 1)

            # Initiation of interactively selected points
            point_max, = ax.plot([], [], marker="o", color="crimson")
            point_min, = ax.plot([], [], marker="o", color="crimson")
            text_max = ax.text(TimeDrift[j, 2], 0, "")
            text_min = ax.text(TimeDrift[j, 2], 0, "")
            xmax = [TimeDrift[j, 2], 0.0]
            xmin = [TimeDrift[j, 2], 0.0]

            # Choose rough area of force evaluation
            rs = RectangleSelector(ax, line_select_callback,
                                drawtype='box', useblit=True, button=[1],
                                minspanx=0, minspany=0, spancoords='data',
                                interactive=True)
        
            plt.savefig(os.path.join(directory_fig_driftcompensationManuel_png, "ForceEvaluationCutCompensated_" + title_part_save_format + ".png"), dpi=300)
            plt.savefig(os.path.join(directory_fig_driftcompensationManuel_eps, "ForceEvaluationCutCompensated_" + title_part_save_format + ".eps"), dpi=300)
            plt.savefig(os.path.join(directory_fig_driftcompensationManuel_pdf, "ForceEvaluationCutCompensated_" + title_part_save_format + ".pdf"), dpi=300)

            plt.show(block=True)
            plt.close()

            # Save selected values as first boundaries of force evaluation
            BeginnMittelung = xmin[0]
            EndeMittelung = xmax[0]

            # Zoomed area of force evaluation
            ZoomBeginn = BeginnMittelung
            ZoomEnde = EndeMittelung
            # Find index of Zoom range
            index_ZoomBeginn = np.where(Time == BeginnMittelung)
            index_ZoomBeginn = int(index_ZoomBeginn[0])
            index_ZoomEnde = np.where(Time == EndeMittelung)
            index_ZoomEnde = int(index_ZoomEnde[0])

            ctypes.windll.user32.MessageBoxW(0, "Select area to perform the force averaging with the cursor and close the window.", "Instructions", 1)

            # Plot of zoomed area
            fig, cx = plt.subplots(constrained_layout=True)
            plt.xlim(ZoomBeginn, ZoomEnde)  # Grenzen (min/max) X-Achse
            cx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Fc[index_ZoomBeginn:index_ZoomEnde],
                    color="blue",  linestyle="-", label=r'Cutting force [N/mm]')
            cx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Ff[index_ZoomBeginn:index_ZoomEnde],
                    color="red",  linestyle="-", label=r'Feed force [N/mm]')
            cx.plot(Time[index_ZoomBeginn:index_ZoomEnde], Fp[index_ZoomBeginn:index_ZoomEnde],
                    color="green",  linestyle="-", label=r'Passive force [N/mm]')

            # Plot settings
            cx.legend(loc="lower left")
            plt.xlabel(r'Time [$s$]')
            plt.ylabel(r'Force [N/mm]')
            cx.set_title(r'Process forces, Experiment ' + title_part)

            # Initiation of interactively selected data
            point_max, = cx.plot([], [], marker="o", color="crimson")
            point_min, = cx.plot([], [], marker="o", color="crimson")
            line_FcAvg, = cx.plot([], [], color="darkblue", linestyle='--')
            line_FfAvg, = cx.plot([], [], color="darkred", linestyle='--')
            line_FpAvg, = cx.plot([], [], color="darkgreen", linestyle='--')

            text_max = cx.text(0, 0, "")
            text_min = cx.text(0, 0, "")
            text_AvgFc = cx.text(0, 0, "")
            text_SigFc = cx.text(0, 0, "")
            text_AvgFf = cx.text(0, 0, "")
            text_SigFf = cx.text(0, 0, "")
            text_AvgFp = cx.text(0, 0, "")
            text_SigFp = cx.text(0, 0, "")

            def line_select_callback_force(eclick, erelease):
                x1, y1 = eclick.xdata, eclick.ydata
                x2, y2 = erelease.xdata, erelease.ydata

                # Select x values in rectangle
                #mask_x = (Time > min(x1, x2)) & (Time < max(x1, x2))
                #xmasked = Time[mask_x]

                mask_x = (Time > min(x1, x2)) & (Time < max(x1, x2)) & (
                    Fc > min(y1, y2)) & (Fc < max(y1, y2))
                xmasked = Time[mask_x]

                if len(xmasked) > 0.0:

                    global index_xmin_force
                    global index_xmax_force

                    xmax[0] = xmasked.max()
                    index_xmax_force = np.where(Time == xmax[0])
                    xmax[1] = Fc[int(index_xmax_force[0])]

                    xmin[0] = xmasked.min()
                    index_xmin_force = np.where(Time == xmin[0])
                    xmin[1] = Fc[int(index_xmin_force[0])]

                    # Calculate average force and standard deviation
                    AvgFc[j, i] = np.round(np.mean(
                        Fc[int(index_xmin_force[0]):int(index_xmax_force[0])]), 1)
                    SigFc[j, i] = np.round(np.std(
                        Fc[int(index_xmin_force[0]):int(index_xmax_force[0])]), 2)
                    AvgFf[j, i] = np.round(np.mean(
                        Ff[int(index_xmin_force[0]):int(index_xmax_force[0])]), 1)
                    SigFf[j, i] = np.round(np.std(
                        Ff[int(index_xmin_force[0]):int(index_xmax_force[0])]), 2)
                    AvgFp[j, i] = np.round(np.mean(
                        Fp[int(index_xmin_force[0]):int(index_xmax_force[0])]), 1)
                    SigFp[j, i] = np.round(np.std(
                        Fp[int(index_xmin_force[0]):int(index_xmax_force[0])]), 2)
                    MaxFc[j, i] = np.round(np.max(Fc[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)
                    MaxFf[j, i] = np.round(np.max(Ff[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)
                    MaxFp[j, i] = np.round(np.max(Fp[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)
                    MinFc[j, i] = np.round(np.min(Fc[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)
                    MinFf[j, i] = np.round(np.min(Ff[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)
                    MinFp[j, i] = np.round(np.min(Fp[int(index_xmin_force[0]):int(index_xmax_force[0])]),1)

                    # Setup text on plot
                    #t_max = "xmax: {:.3f}, y(xmax) {:.3f}".format(xmax[0], xmax[1])
                    #t_min = "xmin: {:.3f}, y(ymin) {:.3f}".format(xmin[0], xmin[1])
                    t_AvgFc = "Avgerage cutting force: {:.1f} N, SD {:.2f} N".format(
                        AvgFc[j, i], SigFc[j, i])
                    t_AvgFf = "Average feed force: {:.1f} N, SD {:.2f} N".format(
                        AvgFf[j, i], SigFf[j, i])
                    t_AvgFp = "Average passive force: {:.1f} N, SD {:.2f} N".format(
                        AvgFp[j, i], SigFp[j, i])

                    point_min.set_data((xmin[0], xmin[1]))
                    point_max.set_data((xmax[0], xmax[1]))
                    line_FcAvg.set_data(
                        Time[index_ZoomBeginn:index_ZoomEnde], AvgFc[j, i])
                    line_FfAvg.set_data(
                        Time[index_ZoomBeginn:index_ZoomEnde], AvgFf[j, i])
                    line_FpAvg.set_data(
                        Time[index_ZoomBeginn:index_ZoomEnde], AvgFp[j, i])

                    text_AvgFc.set_text(t_AvgFc)
                    text_AvgFc.set_position(
                        ((((xmax[0]) - xmin[0]) / 5 + xmin[0]), AvgFc[j, i] + 2 * SigFc[j, i] + 1))
                    text_AvgFc.set_fontsize(14)

                    text_AvgFf.set_text(t_AvgFf)
                    text_AvgFf.set_position(
                        ((((xmax[0]) - xmin[0]) / 5 + xmin[0]), AvgFf[j, i] + 2 * SigFf[j, i] + 1))
                    text_AvgFf.set_fontsize(14)

                    text_AvgFp.set_text(t_AvgFp)
                    text_AvgFp.set_position(
                        ((((xmax[0]) - xmin[0]) / 5 + xmin[0]), AvgFp[j, i] + 2 * SigFp[j, i] + 1))
                    text_AvgFp.set_fontsize(14)

                    # X max and Y min points description
                    # text_max.set_text(t_max)
                    # text_max.set_position(((xmax[0]), xmax[1]))
                    # text_min.set_text(t_min)
                    # text_min.set_position(((xmin[0]), xmin[1]))

                    fig.canvas.draw_idle()

                    plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsManuel_png, "ForceEvaluation_" + Versuchsnummer + "_" + Counter + "_" + str(i + 1) + ".png"), dpi=300)
                    plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsManuel_eps, "ForceEvaluation_" + Versuchsnummer + "_" + Counter + "_" + str(i + 1) + ".eps"), dpi=300)
                    plt.savefig(os.path.join(directory_fig_SingleForceEvaluationsManuel_pdf, "ForceEvaluation_" + Versuchsnummer + "_" + Counter + "_" + str(i + 1) + ".pdf"), dpi=300)

            rs = RectangleSelector(cx, line_select_callback_force,
                                drawtype='box', useblit=True, button=[1],
                                minspanx=0, minspany=0, spancoords='pixels',
                                interactive=True)

            plt.show(block=True)
            plt.close()

            ctypes.windll.user32.MessageBoxW(0, "Type in the terminal if the signal is stable.", "Instructions", 1)

            # Is signal stable?
            Stabel = input(('Is the force signal of experiment ' +
                            str(Versuchsnummer) + '_' + str(Counter) + '_' + str(i + 1) + ' stable? [y/n] '))

            if Stabel == "N" or Stabel == "n":
                # Average force and standard deviation = 0
                AvgFc[j, i] = 0.0
                SigFc[j, i] = 0.0
                AvgFf[j, i] = 0.0
                SigFf[j, i] = 0.0
                AvgFp[j, i] = 0.0
                SigFp[j, i] = 0.0
                MaxFc[j, i] = 0.0
                MaxFf[j, i] = 0.0
                MaxFp[j, i] = 0.0
                MinFc[j, i] = 0.0
                MinFf[j, i] = 0.0
                MinFp[j, i] = 0.0

                ctypes.windll.user32.MessageBoxW(0, 'Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + '_' + str(i + 1) + ' is skipped and not evaluated!', "Instructions", 1)
                print(
                    ('Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + '_' + str(i + 1) + ' is skipped and not evaluated!'))
                continue
            elif Stabel == "y" or Stabel == "y":
                ctypes.windll.user32.MessageBoxW(0, 'Force evaluation is continued!', "Instructions", 1)
                print("Force evaluation is continued!")
            else:
                ctypes.windll.user32.MessageBoxW(0, 'Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + '_' + str(i + 1) + ' is skipped and not evaluated!', "Instructions", 1)
                print(
                    ('Experiment ' + str(Versuchsnummer) + '_' + str(Counter) + '_' + str(i + 1) + ' is skipped and not evaluated!'))
                continue

            # Save selected values and time of force evaluation
            # Time start point for force averaging

            index_TimeForce = int(i * 4)

            TimeForce[j, index_TimeForce] = Time[int(index_xmin_force[0])]
            # Index start point for force averaging
            TimeForce[j, (index_TimeForce + 1)] = int(index_xmin_force[0])
            # Time end point for force averaging
            TimeForce[j, (index_TimeForce + 2)] = Time[int(index_xmax_force[0])]
            # Index end point for force averaging
            TimeForce[j, (index_TimeForce + 3)] = int(index_xmax_force[0])

            # Save data to Excel
            WorkSheetResults.cell(row=j+2, column=1, value=Experiments[j])
            WorkSheetResults.cell(row=j+2, column=2, value=AvgFc[j, i])
            WorkSheetResults.cell(row=j+2, column=3, value=SigFc[j, i])
            WorkSheetResults.cell(row=j+2, column=4, value=MaxFc[j, i])
            WorkSheetResults.cell(row=j+2, column=5, value=MinFc[j, i])

            WorkSheetResults.cell(row=j+2, column=6, value=AvgFf[j, i])
            WorkSheetResults.cell(row=j+2, column=7, value=SigFf[j, i])
            WorkSheetResults.cell(row=j+2, column=8, value=MaxFf[j, i])
            WorkSheetResults.cell(row=j+2, column=9, value=MinFf[j, i])

            WorkSheetResults.cell(row=j+2, column=10, value=AvgFp[j, i])
            WorkSheetResults.cell(row=j+2, column=11, value=SigFp[j, i])
            WorkSheetResults.cell(row=j+2, column=12, value=MaxFp[j, i])
            WorkSheetResults.cell(row=j+2, column=13, value=MinFp[j, i])

            WoorkbookResults.save(os.path.join(directory_eval,f'Temp_{FileIteratorTemp}.xlsx'))

            print("Experiment " + Versuchsnummer + "_" + Counter + " saved to Excel file!")

            # Save data to txt file
            Head = str('Experiment, Cutting force F_c [N/mm], SD F_c [N/mm], Feed force F_f [N/mm], SD F_f [N/mm], Passive force F_p [N/mm], SD F_p [N/mm], TimeFront1, IndTimeFront1, TimeFront2, IndTimeFront2, TimeBack1, IndTimeBack1, TimeBack2, IndTimeBack2, F_Leerlauf_vorne[0], F_Leerlauf_vorne[1], F_Leerlauf_vorne[2], F_Leerlauf_hinten[0], F_Leerlauf_hinten[1], F_Leerlauf_hinten[2], m_drift[0], m_drift[1], m_drift[2], q_drift[0], q_drift[1], q_drift[2], TimeForce')
            DataToSave = np.column_stack((Experiments[j], [AvgFc[j, :]], [SigFc[j, :]], [AvgFf[j, :]], [SigFf[j, :]], [AvgFp[j, :]], [SigFp[j, :]], [F_Leerlauf_vorne[j, :]], [F_Leerlauf_hinten[j, :]], [m_drift[j, :]], [q_drift[j, :]], [TimeDrift[j, :]], [TimeForce[j, :]]))

            Datafile_path = os.path.join(directory_eval, "ProcessForceEvaluation_" + Versuchsnummer + "_" + Counter)

            #np.savetxt(Datafile_path+".txt", DataToSave, fmt='%s', delimiter=' ', header=Head)
            #print("Experiment " + Versuchsnummer + "_" + Counter + " saved as txt file!")

            # Save data as python array
            #np.save(Datafile_path, DataToSave)
            #print("Experiment " + Versuchsnummer + "_" + Counter + " saved as python array!")


            # Save data to Excel
            # jj = i * 6 + 27

            # if MainSheet.cell(row=ZeileExcel, column=jj).value == None:
            #     Save_Forces(ZeileExcel, jj, index_TimeForce, TimeForce)

            # else:
            #     Override = input(
            #         ('Do you want to override the force values of experiment ' + str(Versuchsnummer) + "_" + str(Counter) + ' repetition ' + str(i) + '? [y/n] '))
            #     if Override == "N" or Override == "n":
            #         continue
            #     elif Override == "Y" or "y":
            #         Save_Forces(ZeileExcel, jj, index_TimeForce, TimeForce)



# Save Excel file under last experiment number
FileIteratorFinal = 0
while os.path.exists(os.path.join(directory_eval,f"ProcessForceEvaluation_{Versuchsnummer}_Version{FileIteratorFinal}.xlsx")):
    FileIteratorFinal += 1
# Rename Temp file
os.rename(os.path.join(directory_eval,f'Temp_{FileIteratorTemp}.xlsx'),os.path.join(directory_eval,f"ProcessForceEvaluation_{Versuchsnummer}_Version{FileIteratorFinal}.xlsx")) 


# Save data to txt file and pyhon array
Head = str('Versuchsnummer, Counter, AvgFc [N/mm], SigFc [N/mm], MaxFc [N/mm], MinFc [N/mm],AvgFf [N/mm], SigFf [N/mm], MaxFf [N/mm], MinFf [N/mm], AvgFp [N/mm], SigFp [N/mm], MaxFp [N/mm], MinFp [N/mm]')

DataToSave = np.concatenate((ExperimentsArr,AvgFc, SigFc, MaxFc, MinFc,AvgFf, SigFf, MaxFf, MinFf, AvgFp, SigFp, MaxFp, MinFp),axis=1)
Datafile_path = os.path.join(directory_eval, "ProcessForceEvaluation_" + Versuchsnummer)

np.savetxt(Datafile_path+".txt", DataToSave, fmt='%s', delimiter=' ', header=Head)
print("Experiment " + Versuchsnummer + "_" + " saved as txt file!")
# Save data as python array
np.save(Datafile_path, DataToSave)
print("Experiment " + Versuchsnummer + "_" + " saved as python array!")


#############################################
#############################################
#############################################
#############################################

#DataToSave = np.column_stack((Experiments[j], [AvgFc[j, :]], [SigFc[j, :]], [AvgFf[j, :]], [SigFf[j, :]], [AvgFp[j, :]], [SigFp[j, :]], [F_Leerlauf_vorne[j, :]], [F_Leerlauf_hinten[j, :]], [m_drift[j, :]], [q_drift[j, :]], [TimeDrift[j, :]], [TimeForce[j, :]]))
#Datafile_path = os.path.join(directory_eval, "ProcessForceEvaluation_" + Versuchsnummer + "_" + Counter)
#print("Experiment " + Versuchsnummer + "_" + Counter + " saved as txt file!")
#print("Experiment " + Versuchsnummer + "_" + Counter + " saved as python array!")

# Head = str('Experiment, Cutting force F_c [N/mm], SD F_c [N/mm], Feed force F_f [N/mm], SD F_f [N/mm], Passive force F_p [N/mm], SD F_p [N/mm], TimeFront1, IndTimeFront1, TimeFront2, IndTimeFront2, TimeBack1, IndTimeBack1, TimeBack2, IndTimeBack2, F_Leerlauf_vorne[0], F_Leerlauf_vorne[1], F_Leerlauf_vorne[2], F_Leerlauf_hinten[0], F_Leerlauf_hinten[1], F_Leerlauf_hinten[2], m_drift[0], m_drift[1], m_drift[2], q_drift[0], q_drift[1], q_drift[2], TimeForce')
# DataToSave = np.column_stack((Experiments[j], [AvgFc[j, :]], [SigFc[j, :]], [AvgFf[j, :]], [SigFf[j, :]], [AvgFp[j, :]], [SigFp[j, :]], [
#                              F_Leerlauf_vorne[j, :]], [F_Leerlauf_hinten[j, :]], [m_drift[j, :]], [q_drift[j, :]], [TimeDrift[j, :]], [TimeForce[j, :]]))
# Datafile_path = os.path.join(directory_eval, "ProcessForceEvaluation_" + Versuchsnummer + "_" + Counter)
# np.savetxt(Datafile_path+".txt", DataToSave, fmt='%s', delimiter=' ', header=Head)
# print("Text file of experiment " +
#       Versuchsnummer + "_" + Counter + " saved!")
# np.save(Datafile_path, DataToSave)
# print("Python array of experiment " + Versuchsnummer + "_" + Counter + " saved!")
