import numpy as np
class Edge:
    
    def __init__(self):
        pass

    def set_values(self, y_values, z_values):
        self.y = y_values
        self.z = z_values

    def det_max(self):
        self.zmax_pos = np.nanargmax(self.z)
        self.zmax = self.z[self.zmax_pos]
        self.ymax = self.y[self.zmax_pos]

    def get_edge(self):
        ## Array of imaginary numbers: row 0 is real part, row 1 is imaginary part
        self.im_edge = np.zeros(len(self.y), dtype=complex)
        for i in range(len(self.z)):
            self.im_edge[i] = complex(self.y[i], self.z[i])
    
    def get_y_z (self):
        self.z = np.zeros(len(self.im_edge))
        self.y = np.zeros(len(self.im_edge))

        for i in range(len(self.im_edge)):
            self.y[i] = self.im_edge[i].real
            self.z[i] = self.im_edge[i].imag

        self.det_max()

    def trim_edge(self, length_f):
        ##TODO: work in progress, not actually using in MATLAB implementation
        k = 0
        found_l = False
        found_r = False

        edge_temp = self.im_edge

        while not found_l:
            k += 1
            if abs(self.im_edge[k]-self.im_edge[self.zmax_pos])<=length_f:
                found_l = True
        
        k_min = k-1
        if k_min < 0:
            k_min = 0

        k = self.zmax_pos

        while not found_r and k < len(self.im_edge):
            if abs(self.im_edge[k]-self.im_edge[self.zmax_pos])>=length_f:
                found_r = True
            k += 1
        k_max = k + 1

        self.im_edge = self.im_edge[k_min: k_max]
        self.get_y_z()

    def edge_region(self, LS_Toplimit):
        self.LSendl = 1
        k = 1
        while abs(self.zmax - self.z[k]) > LS_Toplimit:
            k = k + 1
        self.Lsendl = k

        self.LSstartr = len(self.z) - 1
        while abs(self.zmax - self.z[k]) > LS_Toplimit:
            k += 1
        self.LSstartr = k
        self.LSendr = len(self.z) - 1
        self.LSstartl = 1

