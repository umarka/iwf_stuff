import numpy as np
import alicona_mesh as al_mesh

def read_dat():
    data = np.genfromtxt(r"D:\01_Programe\Github\iwf_stuff\MATLABSKRIPT_SCHNEIDKANTENRADIEN\649_1_L1_T229.dat", delimiter=';')

    x_values = data[:, 0]/10**3
    y_values = data[:, 1]/10**3
    z_values = data[:, 2]/10**3

    x_dim = np.unique(x_values).shape[0]
    y_dim = np.unique(y_values).shape[0]

    Z = z_values.reshape(y_dim, x_dim)
    X = x_values.reshape(y_dim, x_dim)
    Y = y_values.reshape(y_dim, x_dim)

    mesh = al_mesh.Mesh(X, Y, Z, np.nanmin(z_values), np.nanmax(z_values), x_dim, y_dim)
    return mesh

# mesh = read_dat()
# print(mesh)