function [X_smooth,Y_smooth,Z_smooth]=StraightEdge(file)

n_mean = 5;

addpath('..\')

%% Einlesen

filename=file;
sample=AliconaReader(filename);

%% Vorbereitung
sample_size=size((sample.DepthData)'); % Aufl�sung des Files [Breite(x) H�he(y)]
x_values=(0:sample_size(1)-1)'; 
y_values=(0:sample_size(2)-1)';
x_pixel_size=str2double(sample.Header.PixelSizeXMeter)*1000; % liest die Pixelgr�sse in Millimeter ein
y_pixel_size=str2double(sample.Header.PixelSizeYMeter)*1000;

x_values=x_values*x_pixel_size; % Erzeugt einen Vektor mit den X-Werten, startet bei 0, in Millimeter
y_values=y_values*y_pixel_size; 

X=ones(sample_size(2),1)*x_values'; % Erzeugt eine Matrix mit allen X-Werten, hier k�nnte auch direkt mit exportierten Daten eingestiegen werden
Y=y_values*ones(1,sample_size(1));
sample.DepthData(sample.DepthData==1e10)=NaN;
Z=double(sample.DepthData*1e3); % Matrix mit allen H�henwerten, in Millimeter

X_smooth=X(:,1:n_mean:end);
Y_smooth=Y(:,1:n_mean:end);
Z_smooth=NaN*ones(size(Z,1),ceil(size(Z,2)/n_mean));



for i=1:size(Z,1)
    z_smooth=smooth(Z(i,:),n_mean);
    Z_smooth(i,:)=z_smooth(1:n_mean:end);
end