
function data = SensofarReader(varargin)

%  SensofarReader  opens a .dat file from the SensoFar microscope and returns a
%  DATA structure containing a header and the following (if present): icon,
%  depth and texture data. The PATH can be passed. Otherwise, a select file
%  dialog box appears.
%  
%   Syntax:
%     SensofarReader()
%     SensofarReader(PATH)
%  
%   Input:
%     PATH  = String
%     DATA  = Struct, containing a header and, if available, data
%   Created by Lukas Dinkelmann on the 12th of April 2022
%   
%   
  
if nargin == 0
    [filename, pathname] = uigetfile('*.dat', 'Select the file to open');
elseif  nargin==1
    tmp = strfind(varargin{1}, '\');
    filename = varargin{1}(tmp(end)+1:end);
    pathname = varargin{1}(1:tmp(end));
else    % Modifikation HK, Mo, 03.06.2019
    pathname = varargin{1};
    filename = varargin{2};
end

path = [pathname filename];
if ismember('.', filename)
    [~, idx] = ismember('.', filename);
    imgName = filename(1:idx-1);
else
    imgName = filename;
end
data = struct;
% tags = struct;
% 
% fid = fopen(path, 'r');

%% Read the header
% We are not interested into all those data
% We just need the pixel size, which is read from SensoView
% for the 20x Objective
% This value is specified in WSP_Auswertung_Filter_Sensofar.

%% Read DepthData And Assign it to a Matrix Z
A = readtable(path);
A = table2array(A);
x_All = A(:,1);
y_All = A(:,2);
z_All = A(:,3);

x_vals = sort(unique(x_All));
y_vals = sort(unique(y_All));
iTest = 0;
% [~, idx] = ismember(x_All, x_vals);
% [~, idy] = ismember(y_All, y_vals);
% try
Z = reshape(z_All, length(x_vals), length(y_vals))';
X = reshape(x_All, length(x_vals), length(y_vals))';
Y = reshape(y_All, length(x_vals), length(y_vals))';
% catch
% To be implemented.

% [X,Y] = meshgrid(x_vals, y_vals);
% Z = nan(size(X));
% stepSize = x_All(2)-x_All(1);
% addedElements = 0;
% notFinished = true;
% startVal = 1;
% while notFinished
%     try
%     difference = diff(x_All(startVal:startVal+length(x_vals)-1) - x_vals);
%     catch
%     difference = diff(x_All(startVal:end) - x_vals(1:length(startVal:end)));
%     end
%     num = find(difference>0);
%     newStartVal = find(difference<-10);
%     plot(x_All(1:15000)); hold on
%     xTest = x_All;
%     for iFix = 1:length(num)
%         addElements = round((x_All(startVal + num(iFix)+1)-x_vals(startVal+ num(iFix)+1))/stepSize);
%         z_All = [z_All(startVal+addedElements:addedElements+startVal+num(iFix)); nan(addElements, 1); z_All(addedElements+startVal+num(iFix)+1:end)];
%         xTest = [xTest(startVal+addedElements:addedElements+startVal+num(iFix)); nan(addElements, 1); xTest(addedElements+startVal+num(iFix)+1:end)];
%         addedElements = addedElements+addElements;
%     end
%     startVal = newStartVal;
%     try
%     Z(i) = z_All(x_All==X(i) & y_All == Y(i));
%     catch
%     end
% end
% end
% [gXx, GXy] = gradient(X);
% [gYx, GYy] = gradient(Y);
% if ~(any(gXx(:)) || any(GXy(:))) || ~(any(gYx(:)) || any(GYy(:)))
%     warning('data read wrong')
% end
% one of 
% fig1 = figure;
% subplot(1,2,1);
% imagesc(X);
% subplot(1,2,2);
% imagesc(Y);
% saveas(fig1, ['ReaderTest\', imgName], 'jpg');
data.DepthData = Z;
% close all
% xLimits = [min(x_Temp), max(x_Temp)];
% yLimits = [min(y_Temp), max(y_Temp)];
% Assumption: Axis with more pixels is along cutting edge.
% if diff(xLimits) > diff(yLimits)
%     x_vals = x_Temp;
%     y_vals = y_Temp;
% else
%     y_vals = x_Temp;
%     x_vals = y_Temp;
% end
% xLimits = [min(x_vals), max(x_vals)];
% yLimits = [min(y_vals), max(y_vals)];
% 
% % How many Elements are in Y- direction:
% numElementsY_1 = sum(x_vals == min(x_vals));
% numElementsY_2 = sum(x_vals == max(x_vals));
% if numElementsY_1 == numElementsY_2
%     numElementsY = numElementsY_1;
% else
%     warning('Weird Data Set. Export in Sensofar with NaN values')
% end
% numElementsX_1 = sum(y_vals == min(y_vals));
% numElementsX_2 = sum(y_vals == max(y_vals));
% if numElementsX_1 == numElementsX_2
%     numElementsX = numElementsX_1;
% else
%     warning('Weird Data Set. Export in Sensofar with NaN values')
% end
% x
% 
% 
% 
% 
% %% Read the icon data
% % Check if there is an icon in the data file

end



