from pandas import DataFrame
import pandas as pd
import Edge
import numpy as np
import matlab
import matlab.engine
import matplotlib.pyplot as plt

def acceptable_range(values):
    ### This does not work, but for consistency, the logic of this function was copied from the MATLAB implementation
    acceptvalue = False
    i = 0
    start = 0
    end = len(values)
    while not acceptvalue and i < len(values):
        if not np.isnan(values[i]):
            acceptvalue = True
            start = i
        i += 1

    while i < len(values):
        if np.isnan(values[i]):
            break
        i += 1
    end = i-1

    return (start, end)


    
def get_ridge_info(y_values, z_values):
    ## Converts 2 arrays representing y & z values into an object containing:
    ##      y, z value
    ##      edge values as complex numbers
    
    edge = Edge.Edge()
    edge.set_values(y_values, z_values)
    edge.det_max()
    edge.get_edge()
    
    return edge

def matlab_edge_conversion (edge):
    pass_edge = vars(edge).copy()
    pass_edge['y'] = matlab.double(pass_edge['y'].tolist())
    pass_edge['z'] =  matlab.double(np.nan_to_num(pass_edge['z']).tolist())
    pass_edge['edge'] =  matlab.double(pass_edge['im_edge'].tolist(), is_complex=True)
    pass_edge['zmax_pos'] = int(pass_edge['zmax_pos']+1)
    pass_edge['zmax'] = float(pass_edge['zmax'])
    pass_edge['ymax'] = float(pass_edge['ymax'])
    del(pass_edge['im_edge']) 
    return pass_edge

def python_edge_conversion (matlab_edge):
    python_edge = vars(matlab_edge).copy()
    python_edge['y'] = np.asarray(matlab_edge['y'].tolist())
    python_edge['z'] =  np.asarray(np.nan_to_num(matlab_edge['z']).tolist())
    python_edge['edge'] =  np.asarray(matlab_edge['im_edge'].tolist(), is_complex=True)
    python_edge['zmax_pos'] = int(matlab_edge['zmax_pos']+1)
    python_edge['zmax'] = float(matlab_edge['zmax'])
    python_edge['ymax'] = float(matlab_edge['ymax'])
    return python_edge

def save_cutting_edge_radius_plot(MP, RadiusPratt, x, y, cutting_edge_x, cutting_edge_y, r_center, radius, r_n_center, r_n, save_path):
    plt.ioff()
    #fig = plt.figure(figsize=10)
    fig = plt.figure()

    ax = fig.gca()
    ax.set_aspect('equal', 'datalim')
    ax.set_title(f"Cutting edge cross section with cutting edge radius [mm]", fontsize=16, fontweight="bold", pad=10)

    ax.plot(x, y, zorder=1)
    ax.plot(cutting_edge_x, cutting_edge_y, "r", zorder=2, label = "cutting edge")

    ##Plot r
    # ax.scatter([r_center[0]], [r_center[1]], marker="+", color="g", zorder=2)
    # cir_r = plt.Circle(r_center, radius, color='g', zorder=2, fill = False, label = "r")
    # ax.add_patch(cir_r)

    ##Plot r_n
    ax.scatter([r_n_center[0]], [r_n_center[1]], marker="+", color="r", zorder=2)
    cir_r_n = plt.Circle(r_n_center, r_n, color='r', zorder=2, fill = False, label = "r_n")
    ax.add_patch(cir_r_n)

    # ##Plot pratt
    # ax.scatter([MP[0]], [MP[1]], marker="+", color="b", zorder=2)
    # cir_pratt = plt.Circle(MP, RadiusPratt, color='b', zorder=2, fill = False, label = "Pratt")
    # ax.add_patch(cir_pratt)


    ax.set_xlabel('x [mm]', fontsize=16, fontweight="bold")
    ax.set_ylabel('y [mm]', fontsize=16, fontweight="bold")
    ax.legend(loc="upper right")

    plt.savefig(save_path, facecolor='white', transparent=False, dpi=300, bbox_inches='tight')
    plt.close()

def save_results_to_csv (results, file_path, relevant_cols = ['x_value', 'r_squared_f', 'r_n', 'r', 'skewness']) -> None:

    ## Convert to pandas dictionary
    ## First insert nan when value not present
    for res in results:
        for col in relevant_cols:
            if col not in res:
                res[col] = np.nan

    filtered_results = [dict((k, res[k])  for k in relevant_cols) for res in results]
    df = pd.DataFrame(filtered_results)
    df.to_csv(file_path, na_rep='nan')


def main_conti_profiles (edge: Edge, LS_Toplimit: float, x_value: float, save_radius_plot: bool, save_location: str, matlab_engine) -> dict:
    
    ##Python implemntation of the MATLAB function "main_conti_profiles"
    err = 0.0
    params_in = {}
    params_in["bman"] = 0
    params_in["lowerlimit1"]=200e-6;
    params_in["upperlimit1"]=30e-6;
    params_in["lowerlimit2"]=100e-6;
    params_in["filter"]=2.0;
    params_in["polydegree"]=5.0;
    params_in["u_p"] = 3e-6;

    params_out = {}

    matlab_edge = matlab_edge_conversion(edge)
    for i in range(2):
        matlab_edge = matlab_engine.trim_edge(matlab_edge, complex(3e-3/1j), nargout=1)
        matlab_edge = matlab_engine.kantenbereich(matlab_edge, float(LS_Toplimit), nargout=1)

        matlab_edge = matlab_engine.str_line(matlab_edge, 'b', 'c', 0, 0, nargout=1)
        matlab_edge = matlab_engine.shift_any(matlab_edge, matlab_edge["y_inter"], matlab_edge["z_inter"], nargout=1)
        matlab_edge = matlab_engine.str_line(matlab_edge, 'b', 'c', 0, 0, nargout=1)
        alpha = (matlab_edge["angle_l"]+matlab_edge["angle_r"])/2.0
        matlab_edge = matlab_engine.rotate_edge(alpha, matlab_edge, 1, nargout=1)
        matlab_edge = matlab_engine.str_line(matlab_edge, 'b', 'c', 0, 0, nargout=1)

        #print(matlab_edge)

    #%DETRMINE DELTA_R and b-SECTION--------------------------------------------------------
    if max(np.asarray(matlab_edge["edge"])[0].imag) < matlab_edge["z_inter"]:


        # choose new limits for LS-straight lines (set by user)
        lowerlimit1=float(max(np.asarray(matlab_edge["edge"])[0].imag) + params_in["lowerlimit1"])
        upperlimit1=float(max(np.asarray(matlab_edge["edge"])[0].imag) + params_in["upperlimit1"])
        matlab_edge = matlab_engine.startpositions(matlab_edge, upperlimit1, lowerlimit1, nargout=1)

        #determine indexes of points lying withing the limits
        matlab_edge = matlab_engine.re_alignment(matlab_edge, nargout=1)

        #determine delta_r------------------------------------------
        matlab_edge,z_bisect,plc1,plc2 = matlab_engine.getdelta_r(matlab_edge,0, nargout=4);

        # determine b-section-----------------------------------------
        kl, kr, pg, matlab_edge = matlab_engine.getb(params_in, matlab_edge, z_bisect, 0, nargout=4);

        # second time (LS lines with new regions)______________________________________
        # %choose new limits for LS-straight lines (also set by user)
        lowerlimit2 = max(np.asarray(matlab_edge["edge"])[0].imag) + params_in ["lowerlimit2"]

        if kl>1:
            upperlimit2=min(np.asarray(matlab_edge["edge"])[0][int(kl)-2].imag, np.asarray(matlab_edge["edge"])[0][int(kr)].imag)
        else:
            upperlimit2 = 0.0;

        matlab_edge = matlab_engine.startpositions(matlab_edge, float(upperlimit2), float(lowerlimit2), nargout=1)
        matlab_edge = matlab_engine.re_alignment(matlab_edge, nargout=1)

        # new delta_r------------------------------------------------------
        matlab_edge,z_bisect,plc1,plc2 = matlab_engine.getdelta_r(matlab_edge, 0, nargout=4);

        # new b-section----------------------------------------------------
        kl,kr,pg,matlab_edge = matlab_engine.getb(params_in, matlab_edge, z_bisect, 0, nargout=4);
    else:
        err = 1.0
    
    ## CALCULATION OF S, SKEWNESS (Schiefe/Schräge), kurtosis (Wölbung)

    # calculate Skewness and kurtosis
    p_distr, matlab_edge = matlab_engine.convert_to_probability(matlab_edge, nargout = 2) # converts geometric "distribution" (point coordinate information) into probability distribution

    #calculate S
    matlab_edge = matlab_engine.calc_S(matlab_edge, kl, kr, plc1, plc2, z_bisect, nargout = 1);

    if err==0: # (there are points in [-b,+b])

        #determine dl_schn and dr_schn-------------------------------------

        # 1.dl and dr are determined at the outer most points of the b-section
        sl = matlab_edge["bl"];
        sr = matlab_edge["br"];

        dl_b,dr_b = matlab_engine.getdldr(sl, sr, z_bisect, kl, kr, matlab_edge, nargout = 2);



        # 2.dl and dr are determined at points of intersection between horizontal (green) line and clearance and
        # rake straight lines
        #print(matlab_edge["strline_left"][0][1])
        sl=(z_bisect-matlab_edge["strline_left"][0][1])/matlab_edge["strline_left"][0][0];
        sr=(z_bisect-matlab_edge["strline_right"][0][1])/matlab_edge["strline_right"][0][0];
        
        dl_schn, dr_schn = matlab_engine.getdldr(sl, sr, z_bisect, kl, kr, matlab_edge, nargout = 2);
        

        # Fit (black) circle into edge Geometry (into green region
        # [-b;+b])------------------------------------------------------------
        #print(np.real(matlab_edge["edge"][int(kl):int(kr)]))
        #print()

        ## Ugly work around to pass numpy array into matlab
        transpose_real = matlab.double(np.transpose(np.real(matlab_edge["edge"]))[int(kl)-1:int(kr)-1].tolist())
        transpose_imag = matlab.double(np.transpose(np.imag(matlab_edge["edge"]))[int(kl)-1:int(kr)-1].tolist())
        xc, yc, R, a, R_squared = matlab_engine.circfit(transpose_real, transpose_imag, nargout = 5)
        th = np.arange(0, 2*np.pi+0.1, 0.1, dtype=float)

        #xn=xc+R*math.sin(th)
        xn = [xc + R*math.sin(th_) for th_ in th]
        #yn=yc+R*math.cos(th);
        yn = [yc + R*math.cos(th_) for th_ in th]

        real = np.real(matlab_edge["edge"][0])*1e3  ## Get cutting edge and convert to mm
        imaginary = np.imag(matlab_edge["edge"][0])*1e3  ## Get cutting edge and convert to mm

        if R_squared.real<=0.90 or R_squared.imag != 0 : # %if circle doesnt fit geometry sufficiently assume that there is a chamfer
            params_out["fase_angle"], params_out["fase_lmatlab_engineth"], params_out["r_squared_f"] = matlab_engine.fasecalc(kl,kr,pg,matlab_edge, nargout = 3);
        else:
            params_out["fase_angle"]=0;
            params_out["fase_lmatlab_engineth"]=0;
            params_out["r_squared_f"]=0;

        # ideal fitted circle (red) with radius r_n between LS-lines for face
        # and flank


        # r_n=kante.delta_r./((1/sin(pi/180*kante.openingangle/2))-1);%radius of ideal circle fitted between face and flank
        r_n = matlab_edge["delta_r"] / ((1/math.sin(math.pi/180*matlab_edge["openingangle"]/2))-1)
        xic=0;
        yic=z_bisect-r_n;
        #xi=xic+r_n*sin(th);
        xi = [xic + r_n*math.sin(th_) for th_ in th]
        # yi=yic+r_n*cos(th);
        yi = [yic + r_n*math.cos(th_) for th_ in th]

        # determine angular range of points b of fitted circle
        #  line1=polyfit([xc real(kante.edge(kl))],[yc imag(kante.edge(kl))],1);
        line1 = np.polyfit([xc, matlab_edge["edge"][0][int(kl)-1].real], [yc, matlab_edge["edge"][0][int(kl)-1].imag], 1)
        x1 = [xc, matlab_edge["edge"][0][int(kl)-1].real]
        y1=[line1[0]*x + line1[1] for x in x1]

        # line2=polyfit([xc real(kante.edge(kr))],[yc imag(kante.edge(kr))],1);
        line2 = np.polyfit([xc, matlab_edge["edge"][0][int(kr)-1].real], [yc, matlab_edge["edge"][0][int(kr)-1].imag], 1)
        x2=[xc, matlab_edge["edge"][0][int(kr)-1].real];
        #y2=line2(1).*x2+line2(2);
        y2=[line2[0]*x + line2[1] for x in x1]

        arr = math.atan(line1[1])-math.atan(line2[0]);
        arr = 180-abs(arr/math.pi*180) #%angular range in degrees
        if save_radius_plot:
            save_cutting_edge_radius_plot (real, imaginary, real[int(kl)-1:int(kr)-1], imaginary[int(kl)-1:int(kr)-1],(xc*1e3, yc*1e3), R*1e3, (xic*1e3, yic*1e3), r_n*1e3, save_location)

    params_out["r_n"]=r_n   # radius of ideal circle
    params_out["r"]=R       # radius of fitted circle
    params_out["xc"]=xc     # position of circle centre point
    params_out["yc"]=yc     # position of circle centre point
    params_out["b"]=pg      # number of points in b-region
    params_out["kante"]=matlab_edge;
    params_out["zmax"]=max([x.imag for x in matlab_edge["edge"][0]]);
    params_out["arr"]=arr   # angular range of points in b
    params_out["dl_schn"]=dl_schn;
    params_out["dr_schn"]=dr_schn;
    params_out["dl_b"]=dl_b;
    params_out["dr_b"]=dr_b;
    params_out["r_squared"]=R_squared;
    return params_out     



