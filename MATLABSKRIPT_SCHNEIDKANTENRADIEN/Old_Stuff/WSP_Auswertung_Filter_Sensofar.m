% Auswertung WSP-Aufnahmen
% Hagen Klippel, Do, 23.05.2019; nach Vorlage von Linus Meier
%
clear all;
close all;
demoMode = 1;
%
warning('off', 'all');
%
addpath('Version_1_7');
%%% Einlesen
AktPfad = strcat(pwd, '/');
% WSP001AB = 'WSP_001_AB_dem.al3d';
% WSP015CD = 'WSP_015_CD_dem.al3d';
% WSP016AB = 'WSP_016_AB_dem.al3d';
% WSP149CD = 'WSP_149_CD.al3d';
%Bilddaten=AliconaReader(AktPfad, WSP001AB);
%Bilddaten=AliconaReader(AktPfad, WSP015CD);
%Bilddaten=AliconaReader(AktPfad, WSP016AB);
if demoMode
    pathList =1;
    demoDatFile = 'T210_L1_edge.dat';
else
pathList = [...
    "C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_004";...
    'C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_005\Box_005_10_7';...
    'C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_006\Box_006_0_14';...
    'C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_006\Box_006_10_14';...
    'C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_007\Box_007_10_7';...
    'C:\Users\Lukas\Downloads\Titanzerspanung\Titanzerspanung\Sensofar_Messungen\Box_008\Box_008_0_14';...
    ];
end

% pathList =["C:\Users\Lukas\OneDrive - Eidgenössische Technische Hochschule Zürich 1\Shared\Titanzerspanung\Sensofar_Messungen\Noah"]
for iPath = 1:length(pathList)
    radius_n = zeros(100,2);
    nameList = string(zeros(100,1));
    if demoMode
        Sensofar_Dateien = 1;
    else
        Sensofar_Dateien = dir([char(pathList(iPath)), '\/*.dat']);
    end
    
    %
    Gradientenlimit = 1e-4; % Übergang von den Eckenradien zur Schneide
    %
    Schneidkantenradienbestimmen = true;    % Auswertung der Schneidkantenradien? HK, Mo, 18.05.2020
    pixelSize = 0.69e-6; % read from SensoView, 20x zoom
    %
    for WSP_Bild=1:length(Sensofar_Dateien)
        %for WSP_Bild=68:68  % Bild mit Fehlern
        disp(['We are at WSP Bild', num2str(WSP_Bild)]);
        disp(iPath)
        %         clearvars -except AktPfad Sensofar_Dateien WSP_Bild Schneidkantenradienbestimmen Gradientenlimit pixelSize T    % Alle Variablen bis auf die angegebenen löschen
        close all force
        if demoMode
            Bilddaten = SensofarReader('./', demoDatFile);
        else
            Pfad = strcat(Sensofar_Dateien(WSP_Bild).folder, '/');
            Sensofar_Dateien(WSP_Bild).name
            Bilddaten=SensofarReader(Pfad, Sensofar_Dateien(WSP_Bild).name); % HK, So, 23.02.2020
        end
        %Bilddaten=AliconaReader('~/Schreibtisch/ALICONA_WSP/', WSP149CD); % HK, So, 23.02.2020
        %
        %% Vorbereitung; kopiert aus "StraighEdge.m", HK, Mo, 03.06.2019
        %n_mean = 2; % Glättungsdistanz -> Achtung, Daten vorher filtern, sonst kommt Unfug raus! HK, Di, 04.06.2019
        sample_size=size((Bilddaten.DepthData)'); % Aufl�sung der Datei [Breite(x) H�he(y)]
        x_values=(0:sample_size(1)-1)';
        y_values=(0:sample_size(2)-1)';
        x_pixel_size = pixelSize*1e3; % liest die Pixelgr�sse in Millimeter ein
        y_pixel_size = pixelSize*1e3;
        %
        x_values=x_values*x_pixel_size; % Erzeugt einen Vektor mit den X-Werten, startet bei 0, in Millimeter
        y_values=y_values*y_pixel_size;
        %
        X=ones(sample_size(2),1)*x_values'; % Erzeugt eine Matrix mit allen X-Werten, hier k�nnte auch direkt mit exportierten Daten eingestiegen werden
        Y=y_values*ones(1,sample_size(1));
        Bilddaten.DepthData(Bilddaten.DepthData==1e10)=NaN; % Werte "1e10" mit "NaN" �berschreiben
        Z=double(Bilddaten.DepthData/1e3); % Matrix mit allen H�henwerten, in Millimeter
        %
        %figure(444); plot(Z(:,2000)); % Plot durch den Querschnitt an (Längs-)Position 2000
        %
        %%
        % Partikel generieren
        %[Partikel] = InitPartikel(1.5, x_pixel_size, X, Y);

        %% Daten hier filtern, so daß nur die WSP-Kante betrachtet wird und nicht der Rest
        %Z_neu = WSPKanteMinMaxFiltern(Z, -0.05, 0.05); % Daten filtern: Werte kleiner/größer als Min/Max abschneiden
        % Level Fix Filter: added by Lukas Dinkelmann
        maxZ = max(Z);
        xMaxTemp = 1:length(maxZ);
        P = polyfit(xMaxTemp,maxZ,1);
        maxZF = P(1)*xMaxTemp + P(2);
        Z1 = Z - maxZ;
        % addition end
        Z_neu = WSPKanteMinMaxFiltern(Z1, -0.05, 0.1); % Daten filtern: Werte kleiner/größer als Min/Max abschneiden
        Z_neu = WSPKanteFDMFilter(Z_neu, 0.0001);
        Z_neu = WSPKanteFiltern(Z_neu); % Daten hier filtern, so daß nur die WSP-Kante betrachtet wird und nicht der Rest
        figure(1); subplot(2,2,1); contour(X,Y,Z);
        %figure; subplot(2,2,1); contour(X,Y,Z);
        subplot(2,2,2); contour(X,Y,Z_neu);
        subplot(2,2,3); s=surf(X,Y,Z,Z); s.EdgeColor = 'none'; axis equal;
        subplot(2,2,4); s=surf(X,Y,Z_neu,Z_neu); s.EdgeColor = 'none'; axis equal;
        %
        if ~demoMode
            Bildname = strcat(Sensofar_Dateien(WSP_Bild).folder, '/Results/', Sensofar_Dateien(WSP_Bild).name(1:10), '_1.png');
        else

            Bildname = strcat('./Results/', 'demo_1.png');
        end
            saveas(gcf,Bildname);
            close(gcf);
        %%
        % Kante ausrichten, dazu zun�chst sektionsweise (jeden Schnitt durchs Profil) h�chsten Punkt der Schneidkante finden - diese dann an Y-Achse ausrichten (wenn nötig Drehungen um X- und Z- Achse
        %
        [IndexMax, Z_Max] = MaxZfinden(Z_neu);% Indizes der Z-Maxima speichern
        Median_Xmax = median(X(1,IndexMax)); % Medianwert der X-Werte zu Z_Max bestimmen -> Finden des Kantenbereichs zwischen den beiden Eckenradien
        Abweichung_Median_Xmax = (X(1,IndexMax) - Median_Xmax)/Median_Xmax; % Relative Abweichung vom Medianwert entlang der Schneidkante
        %
        %maxFehler = 0.10
        %Abweichung_Median_Xmax_gefiltert = MedianAbweichungsfilter(Abweichung_Median_Xmax, maxFehler); % Abweichungen vom Median <>maxFehler ausfiltern
        Zmax_mavg = movmean(Z_Max, 50); % gleitender Mittelwert über 50 Punkte
        Zmax_mavg_gradient = movmean(gradient(Zmax_mavg), 50);
        %Xmax_mavg = movmean(Z_Max, 50); % gleitender Mittelwert über 50 Punkte
        %
        BeginnSchneide = find(Zmax_mavg_gradient<Gradientenlimit,1,'first');
        EndeSchneide = find(Zmax_mavg_gradient>-Gradientenlimit,1,'last');

        % Ghetto Fix by LD, 12.04.2022
        if isempty(BeginnSchneide)
            BeginnSchneide = 1;
            EndeSchneide = length(Zmax_mavg_gradient);
        end
        %BeginnSchneide = find(Zmax_mavg>-0.05,1);
        %EndeSchneide = find(Zmax_mavg>-0.05,1,'last');
        Schneidenbeginn = BeginnSchneide*x_pixel_size;
        Schneidenende = EndeSchneide*x_pixel_size;
        Schneidenbreite = Schneidenende - Schneidenbeginn % Gerader Bereich zwischen Eckenradien
        %
        figure(2);
        title('Schneidenbreite: ' + string(Schneidenbreite) + "mm");
        %plot(X(1,IndexMax)); hold on; plot(Z_Max); plot(Zmax_mavg); plot(find(Zmax_mavg>-0.05,1),0.0,'g*'); plot(find(Zmax_mavg>-0.05,1,'last'),0.0,'r*'); hold off; legend('X-Koordinate von maxZ','Z','Zmax_mavg','Schneidenbeginn','Schneidenende');
        %plot(X(1,IndexMax)); hold on; plot(Z_Max); plot(Zmax_mavg); plot(find(Zmax_mavg>-0.05,1),0.0,'g*'); plot(find(Zmax_mavg_gradient<1e-5,1),0.0,'b*'); plot(find(Zmax_mavg>-0.05,1,'last'),0.0,'r*'); hold off; legend('X-Koordinate von maxZ','Z','Zmax_mavg','Schneidenbeginn','Schneidenende');
        plot(X(1,IndexMax)); hold on; plot(Z_Max); plot(Zmax_mavg); plot(BeginnSchneide,0.0,'g*'); plot(EndeSchneide,0.0,'r*'); hold off; legend('X-Koordinate von maxZ','Z','Zmax_mavg','Schneidenbeginn','Schneidenende'); legend('Location','southoutside')
        title('Schneidenbreite: ' + string(Schneidenbreite) + "mm");
        %subplot(2,2,1); plot(X(1,IndexMax)); hold on; plot(Z_Max); hold off; legend('X-Koordinate von maxZ','Z');
        %subplot(2,2,2); plot(Abweichung_Median_Xmax*100); ylim([-5 5]); legend('Prozentuale Abweichung vom Medianwert');
        %subplot(2,2,3); plot(Abweichung_Median_Xmax_gefiltert); legend('Prozentuale Abweichung vom Medianwert, gefiltert');
        %subplot(2,2,4); plot(Abweichung_Median_Xmax_gefiltert); ylim([-maxFehler maxFehler]); legend('Prozentuale Abweichung vom Medianwert, gefiltert');
        %
        if ~demoMode
        Bildname = strcat(Sensofar_Dateien(WSP_Bild).folder, '/Results/', Sensofar_Dateien(WSP_Bild).name(1:10), '_2.png');
        else
            Bildname = strcat('./Results/', 'demo_2.png');
       
        end
        saveas(gcf,Bildname);
        close(gcf);
        %%
        %[AkzeptableWerte_Index] = AkzeptBereicheBestimmen(Abweichung_Median_Xmax_gefiltert); % Bereiche mit g�ltigen Werten bestimmen
        %[Abweichung_Median_Xmax_gefiltert] = Lueckenschlusz(Abweichung_Median_Xmax_gefiltert, AkzeptableWerte_Index); % L�cken per linearer Interpolation schlie�en
        %[BeginnSchneide, EndeSchneide] = IdentEckenradien(Abweichung_Median_Xmax_gefiltert); %Endradien links und rechts finden -> Bestimmung Nullpunkt ab Eckenradius (Detektion der genauen Schnittpositionen nach den Versuchen anhand Mikroskopie der gebrauchten WSP)
        %     BeginnSchneide = find(Zmax_mavg>-0.05,1);
        %     EndeSchneide = find(Zmax_mavg>-0.05,1,'last');
        %     Schneidenbeginn = BeginnSchneide*x_pixel_size;
        %     Schneidenende = EndeSchneide*x_pixel_size;
        %     Schneidenbreite = Schneidenende - Schneidenbeginn % Gerade Bereich zwischen Eckenradien
        %
        %%
        %disp(Schneidkantenradienbestimmen)
        if (Schneidkantenradienbestimmen == true)
            MaxScanTiefe = zeros(1,EndeSchneide); %length(Z_Max));
            AnzVerwertbarerWerteImSchnitt = zeros(1,EndeSchneide); %,length(Z_Max));
            Kreisradien = zeros(1,EndeSchneide); %length(Z_Max));
            % Initialisierung von results evtl über: https://www.mathworks.com/matlabcentral/answers/12912-how-to-create-an-empty-array-of-structs
%             results = zeros(1,EndeSchneide-BeginnSchneide); % Initialisierung des Feldes für schnellere Ausführung
            %for j=1:size(Z,2) % Original Linus
            for j=BeginnSchneide:EndeSchneide   % Modifikation HK; Do, 12.06.2019
                if mod(j,100)==0
                    disp('Progress in %')
                    disp(num2str(j/(EndeSchneide-BeginnSchneide)*100))
                end
                notNanValues = sum(~isnan(Z_neu(:,j)));
                if notNanValues > 0
                % Zun�chst NaN-Werte beschneiden und Feld entsprechend verk�rzen, danach Radienerkennung starten
                %[AkzeptableWerte_Index] = AkzeptBereicheBestimmen(Z(:,j)); % Bereiche mit g�ltigen Werten bestimmen
                [AkzeptableWerte_Index] = AkzeptBereicheBestimmen(Z_neu(:,j)); % Bereiche mit g�ltigen Werten bestimmen
                Idx_Beginn = AkzeptableWerte_Index.Anfang;
                Idx_Ende = AkzeptableWerte_Index.Ende;
                AnzVerwertbarerWerteImSchnitt(j) =  Idx_Ende - Idx_Beginn + 1; % Absichern, wieviele Werte im Schnitt überhaupt verwendet werden konnten
                % Kreisanpassung
                %%{
                Kreisradien(j)=NaN;
                try
                    %XY = [Y(Idx_Beginn:Idx_Ende,j),Z_neu(Idx_Beginn:Idx_Ende,j)];
                    [Zmax, ZmaxIdx] = max(Z_neu(Idx_Beginn:Idx_Ende,j));
                    ZmaxIdx = ZmaxIdx + Idx_Beginn - 1; % "Umrechnen" auf Gesamtfeld Z_neu
                    XY = [Y(ZmaxIdx-10:ZmaxIdx+10,j),Z_neu(ZmaxIdx-10:ZmaxIdx+10,j)];
                    if (Idx_Ende-Idx_Beginn > 5)
                        %Kreisparam=CircleFitByPratt(XY);
                        %disp(Kreisparam(3)*1000+string("nm"));
                        %Kreisradien(j)=Kreisparam(3)*1000;
                    end
                catch
                end


                %            ang=0:0.01:2*pi; xp=Kreisparam(3)*cos(ang); yp=Kreisparam(3)*sin(ang);
                %            figure(666); %plot(Z_neu(:,j)); hold on; plot(Y(Idx_Beginn:Idx_Ende,j),Z_neu(Idx_Beginn:Idx_Ende,j)); hold on; plot(Kreisparam(1)+xp,Kreisparam(2)+yp); hold off;
                %%}

                try
                    %kante=getinfolinus_ridge(Y(:,j),Z(:,j),dirinfo(i).name);
                    %kante=getinfolinus_ridge(Y(:,j),Z(:,j),'WSP001AB');
                    kante=getinfolinus_ridge(Y(Idx_Beginn:Idx_Ende,j),Z(Idx_Beginn:Idx_Ende,j),'WSP001AB');
                    %kante=getinfolinus_ridge(X(j,:),Z_smooth(j,:),'WSP001AB');
                    %kante=getinfolinus_ridge(Y_smooth(:,j),Z_smooth(:,j),'WSP001AB');

                    HoechsterNiedrigsterPunkt_Z_aktEbene = max(Z_neu(Idx_Beginn,j), Z_neu(Idx_Ende,j)); % höherer der beiden Endpunkte links und rechts
                    MaxTiefe = Zmax-HoechsterNiedrigsterPunkt_Z_aktEbene;
                    MaxScanTiefe(j) = MaxTiefe;

                    %LS_toplimit=30e-6;%150m� vertically down from maximum point / %LS_toplimit=10e-6;% Modifikation HK
                    %LS_Toplimit = MaxTiefe*1e-3
                    LS_Toplimit = min(MaxTiefe*1e-3/2.0, 30e-6); % Halbierung der maximalen Tiefe
                    results(j)=main_conti_profiles_HK(kante, LS_Toplimit);
                    %
                    %             kante_2=getinfolinus_ridge(Y(181:240,1000),Z(181:240,1000),'WSP001AB');
                    %             results=main_conti_profiles(kante_2);
                catch
                    result.r_n=NaN;%radius of ideal circle
                    result.r=NaN;%radius of fitted circle
                    result.xc=NaN;%position of circle centre point
                    result.yc=NaN;%position of circle centre point
                    result.b=NaN;%number of points in b-region
                    kante2.z=[];
                    kante2.y=[];
                    kante2.zmax=[];
                    kante2.zmax_pos = NaN;
                    kante2.ymax = NaN;
                    kante2.edge = [];
                    kante2.name = '';
                    result.kante=kante2;
                    result.zmax=NaN;
                    result.arr=NaN;%angular range of points in b
                    result.dl_schn=NaN;
                    result.dr_schn=NaN;
                    result.dl_b=NaN;
                    result.dr_b=NaN;
                    result.r_squared=NaN;
                    result.totalpoints=NaN;
                    result.fase_angle=NaN;
                    result.fase_length=NaN;
                    result.r_squared_f=NaN;
                    results(j)=result;
                    %
                    MaxScanTiefe(j) = NaN;
                end
            end

            %%
            % results .r & .r_n aussieben
            % aus "main_conti_profiles.m":
            %   params_out.r_n=r_n; %radius of ideal circle
            %   params_out.r=R;     %radius of fitted circle
            %
            yPos     = zeros(1,length(results));    % initialisieren
            Radien_n = zeros(1,length(results));    % initialisieren
            Radien2  = zeros(1,length(results));    % initialisieren
            RQuadrat = zeros(1,length(results));    % initialisieren
            AnzPkt   = zeros(1,length(results));    % initialisieren
            Fasenwinkel = zeros(1,length(results));    % initialisieren
            Fasenlaenge = zeros(1,length(results));    % initialisieren
            RQuadrat_f  = zeros(1,length(results));    % initialisieren
            for i=1:length(results)
                yPos(i) = (i-BeginnSchneide) * x_pixel_size;   % Koordinate entlang der Schneidkante in [mm], ab Ende Eckenradius
                if isempty(results(i).r_n)
                    Radien_n(i) = NaN;
                else
                    Radien_n(i) = results(i).r_n;
                end
                if isempty(results(i).r)
                    Radien2(i) = NaN;
                else
                    Radien2(i) = results(i).r;
                end
                if isempty(results(i).r_squared)
                    RQuadrat(i) = NaN;
                else
                    RQuadrat(i) = results(i).r_squared;
                end
                if isempty(results(i).totalpoints)
                    AnzPkt(i) = NaN;
                else
                    AnzPkt(i) = results(i).totalpoints;
                end
                if isempty(results(i).fase_angle)
                    Fasenwinkel(i) = NaN;
                else
                    Fasenwinkel(i) = results(i).fase_angle;
                end
                if isempty(results(i).fase_length)
                    Fasenlaenge(i) = NaN;
                else
                    Fasenlaenge(i) = results(i).fase_length;
                end
                if isempty(results(i).r_squared_f)
                    RQuadrat_f(i) = NaN;
                else
                    RQuadrat_f(i) = results(i).r_squared_f;
                end
            end
            end
            %
            % Save variable names to table
            meanRadius_n = mean(Radien_n, 'omitnan')*1E6;
            medianRadius_n = median(Radien_n, 'omitnan')*1E6;
            maxRadius_n = max(Radien_n,[],  'omitnan')*1E6;
            minRadius_n = min(Radien_n,[], 'omitnan')*1E6;
            stdRadius_n = std(Radien_n, 'omitnan')*1E6;
            numValidData = sum(~isnan(Radien_n));
            numInValidData = sum(isnan(Radien_n));
            totalData = length(Radien_n);
            if demoMode
                imgName = 'demoFile';
            else
                imgName = Sensofar_Dateien(WSP_Bild).name(1:10); % added by Lukas D
            end
            newEntry = {imgName, 'r_n', date, numValidData, numInValidData, totalData, medianRadius_n, meanRadius_n, maxRadius_n, minRadius_n, stdRadius_n};
            if WSP_Bild==1
                if demoMode
                    pathName = strcat('./Results/');
                else
                    pathName = strcat(Sensofar_Dateien(WSP_Bild).folder, '/Results/');
                end
                excelFilePath = strcat(pathName, 'Measured_Radii_All.xlsx');
                if isfile(excelFilePath)
                    T = readtable(excelFilePath);
                else
                    T=cell2table(newEntry);
                    T.Properties.VariableNames = {'Cutting edge name', 'Radius_Type', 'Date', 'Num Valid Data Points', 'Num invalid Data Points', 'Total Data Points', 'Median', 'Mean', 'Max', 'Min', 'std'};
                end
                T = [T; newEntry];
            else
                T = [T; newEntry];
            end
            writetable(T,excelFilePath);
            figure(3);
            plot(yPos, Radien_n*1E6);
            hold on;
            plot(yPos, Radien2*1E6);
            %plot(yPos, Kreisradien);
            hold off;
            xlabel('Position entlang Schneidkante [mm]');
            ylabel('Schneidkantenradius [\mu m]');
            legend('Wyen 1: (r_n) Radius idealer Zirkel','Wyen 2: (r) Radius eingepaßter Zirkel');%, 'HK: Algorithmus nach Pratt');
            legend('Location','southoutside')
            %figure; plot(yPos, Radien_n*1E6); hold on; plot(yPos, Radien2*1E6); hold off; xlabel('Position entlang Schneidkante [mm]'); ylabel('Schneidkantenradius [\mu m]'); legend('r_n Radius idealer Zirkel','r Radius eingap�ter Zirkel');
            if ~demoMode
                Bildname = strcat(Sensofar_Dateien(WSP_Bild).folder, '/Results/', Sensofar_Dateien(WSP_Bild).name(1:10), '_3.png');
            else
                Bildname = strcat('./Results/', 'demo_3.png');
            end
            saveas(gcf,Bildname);
            close(gcf);
        else
            disp("Schneidkantenradienbestimmen = false -> keine Auswertung der Radien")
        end
        % Daten in Textdatei speichern, HK, Di, 26.05.2020
        % Dateikopf: Schneidennummer & -seite, Schneidenbreite
        % Spalte 01: Position entlang der Schneide % (yPos)
        % Spalte 02: Radius - Methode 1, Wyen A (idealer Kreis) % (Radien_n)
        % Spalte 03: Radius - Methode 2, Wyen B (Radius eingepaßt) % (Radien2)
        % Spalte 04: Radius - Methode 3, HK / Pratt % (Kreisradien)
        % Spalte 05: Anzahl verwertbarer Punkte pro Schnitt    % (AnzVerwertbarerWerteImSchnitt)
        % Spalte 06: Verwertbare Tiefe % (MaxScanTiefe)
        % Spalte 07: result.r_squared=NaN;   % (RQuadrat)
        % Spalte 08: result.totalpoints=NaN; % (AnzPkt)
        % Spalte 09: result.fase_angle=NaN;  % (Fasenwinkel)
        % Spalte 10: result.fase_length=NaN; % (Fasenlaenge)
        % Spalte 11: result.r_squared_f=NaN; % (RQuadrat_f)
        if ~demoMode
        Datendateiname = strcat(Sensofar_Dateien(WSP_Bild).folder, '/Results/', Sensofar_Dateien(WSP_Bild).name(1:10), '_Daten.dat');
        %SBreite = string(Schneidenbreite);
        %ZK = strcat('Schneidenbreite: ', SBreite, ' mm');
        dlmwrite(Datendateiname,string(strcat('Schneidenbreite: ', string(Schneidenbreite), ' mm')).char, 'delimiter', '');    % Name der Ursprungsdatendatei in Datei schreiben (Schneidennummer & -seite, Schneidenbreite)
        %dlmwrite(Datendateiname, ZK.char, 'delimiter', '');    % Name der Ursprungsdatendatei in Datei schreiben (Schneidennummer & -seite, Schneidenbreite)
        dlmwrite(Datendateiname, string(strcat('WSP-Nummer und -Seite: ', Sensofar_Dateien(WSP_Bild).name(1:10))).char, 'delimiter', '', '-append');    % Schneidenbreite [mm]
        dlmwrite(Datendateiname, 'yPosition [mm], Radius idealer Kreis (Wyen A) [müm], Radius eingepaßter Kreis (Wyen B) [müm], Radius HK / Pratt [müm], Punkte pro Schnitt, Tiefenbereich (max-min) [mm], RQuadrat, AnzPkt, Fasenwinkel, Fasenlaenge, RQuadrat_f', 'delimiter', '', '-append');    % Schneidenbreite [mm]);  % Tabellenkopf
        dlmwrite(Datendateiname, [yPos', Radien_n'*1E6, Radien2'*1E6, Kreisradien', AnzVerwertbarerWerteImSchnitt', MaxScanTiefe', RQuadrat', AnzPkt', Fasenwinkel', Fasenlaenge', RQuadrat_f'], 'delimiter', '\t', '-append');    % Eigenwerte + lfd. Nummer in Datei schreiben% Schneidennummer & -seite: Alicona_Dateien(WSP_Bild).name(1:10)
        
        else
            Datendateiname = strcat('./Results/', 'testMode_Daten.dat');
            dlmwrite(Datendateiname,string(strcat('Schneidenbreite: ', string(Schneidenbreite), ' mm')).char, 'delimiter', '');    % Name der Ursprungsdatendatei in Datei schreiben (Schneidennummer & -seite, Schneidenbreite)
        %dlmwrite(Datendateiname, ZK.char, 'delimiter', '');    % Name der Ursprungsdatendatei in Datei schreiben (Schneidennummer & -seite, Schneidenbreite)
%         dlmwrite(Datendateiname, string('WSP-Nummer und -Seite: ', 'test', 'delimiter', '', '-append');    % Schneidenbreite [mm]
        dlmwrite(Datendateiname, 'yPosition [mm], Radius idealer Kreis (Wyen A) [müm], Radius eingepaßter Kreis (Wyen B) [müm], Radius HK / Pratt [müm], Punkte pro Schnitt, Tiefenbereich (max-min) [mm], RQuadrat, AnzPkt, Fasenwinkel, Fasenlaenge, RQuadrat_f', 'delimiter', '', '-append');    % Schneidenbreite [mm]);  % Tabellenkopf
        dlmwrite(Datendateiname, [yPos', Radien_n'*1E6, Radien2'*1E6, Kreisradien', AnzVerwertbarerWerteImSchnitt', MaxScanTiefe', RQuadrat', AnzPkt', Fasenwinkel', Fasenlaenge', RQuadrat_f'], 'delimiter', '\t', '-append');    % Eigenwerte + lfd. Nummer in Datei schreiben% Schneidennummer & -seite: Alicona_Dateien(WSP_Bild).name(1:10)
        end
        
    end

    writetable(T,excelFilePath);

end
%
%%
% Zeilenweises durchlaufen der Werte (Längs) und wenn Mindestanzahl an Nicht-NaN-Punkten unterschritten wird, Zeile löschen
% HK, Mi ,05.06.2019
%MinNNan = 50; % Mindestanzahl Nicht-NaN-Einträge in der Zeilte
%
% for i=1:
%     for j=1:
%         % Zähle  Nicht-NaN-Einträge
%
%         if isnan(Z(i,j))
%         end
%     end
% end
%%
% kurze L�cken per linearer Interpolation schlie�en
function [Werte] = Lueckenschlusz(Werte, Indizes)
disp('L�ckenschlu� validieren (lineare Interpolation)')
for i = 1:length(Indizes)-1
    Idx_Ende = Indizes(i).Ende;
    Idx_Anfang = Indizes(i+1).Anfang;
    if (Idx_Anfang - Idx_Ende) < 3
        Anfangswert = Werte(Idx_Ende);
        Endwert = Werte(Idx_Anfang);
        m = (Anfangswert - Endwert)/(Idx_Ende-Idx_Anfang); % Steigung
        n = (Anfangswert*1)-m;
        for j=Idx_Ende+1:Idx_Anfang-1   % NaN-Werte �berschreiben (lineare Interpolation aus g�ltigen Werten)
            Werte(j) = m*(j+1) + n;
        end
        %disp('Kurze L�cke gefunden')
    end
end
end
%%
function [NaN_Liste] = NaNBereicheBestimmen(Abweichung_Median_Xmax_gefiltert) % NaN-Werte filtern
NNaN = 0; % Z�hler f�r Nicht-NaN-Eintr�ge
VorgaengerNaN = false;
NaNBereichAnfang = 0;
NaNBereichEnde = 0;
%
LaengeXmaxMedianGefiltert = length(Abweichung_Median_Xmax_gefiltert)
%
for i = 1:LaengeXmaxMedianGefiltert
    if ~isnan(Abweichung_Median_Xmax_gefiltert(i))
        NNaN = NNaN + 1;
        if VorgaengerNaN == true
            NaNBereichEnde = NaNBereichEnde + 1
            NaN_Liste(NaNBereichEnde).Ende = i-1; % Ende NaN-Bereich sichern
        end
        VorgaengerNaN = false;
    elseif VorgaengerNaN == false
        NaNBereichAnfang = NaNBereichAnfang + 1
        VorgaengerNaN = true;
        NaN_Liste(NaNBereichAnfang).Anfang = i; % Beginn NaN-Bereich sichern
    else
        VorgaengerNaN = true;
    end
    %
    if (i == LaengeXmaxMedianGefiltert) && VorgaengerNaN % wenn letztes Element NaN ist, dann als Endposition sichern
        NaNBereichEnde = NaNBereichEnde + 1
        NaN_Liste(NaNBereichEnde).Ende = i; % Ende NaN-Bereich sichern
    end
end
end
%%
function [AkzeptableWerte_Index] = AkzeptBereicheBestimmen(Werte) % Bereiche (Indizes) von Nicht-NaN-Werten zur�ckgeben
%
ListenLaenge = length(Werte);
%
AkzeptWert = false;
AkzeptBereich = 1;
%
if ~isnan(Werte(1))
    AkzeptWert = true;
    AkzeptableWerte_Index(AkzeptBereich).Anfang = 1;
end
%
for i = 1:ListenLaenge
    if isnan(Werte(i)) && AkzeptWert
        AkzeptableWerte_Index(AkzeptBereich).Ende = i-1;
        AkzeptWert = false;
        AkzeptBereich = AkzeptBereich + 1;
    elseif ~isnan(Werte(i)) && ~AkzeptWert
        AkzeptWert = true;
        AkzeptableWerte_Index(AkzeptBereich).Anfang = i;
    end
end
%
if ~isnan(Werte(ListenLaenge)) % wenn letztes Element Nicht-NaN ist, dann als Endposition sichern
    AkzeptableWerte_Index(AkzeptBereich).Ende = ListenLaenge;
end
end
%%
function [Abweichung_Median_Xmax_gefiltert] = MedianAbweichungsfilter(Abweichung_Median_Xmax, maxFehler) % Abweichungen vom Median <>maxFehler ausfiltern
%maxFehler = 0.05;  % 5% Abweichung
%
for i = 1:length(Abweichung_Median_Xmax)
    if abs(Abweichung_Median_Xmax(i)) <= maxFehler
        Abweichung_Median_Xmax_gefiltert(i) = Abweichung_Median_Xmax(i);
    else
        Abweichung_Median_Xmax_gefiltert(i) = NaN;
    end
end
end
%%
function [IndexMax, Z_Max] = MaxZfinden(Z)% Sektionsweise (jeden Schnitt durchs Profil) höchste Punkt der Schneidkante finden - diese dann an Y-Achse ausrichten (wenn nötig Drehungen um X- und Z- Achse
%
AnzY = length(Z(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
for i=1:AnzY
    [W,I] = max(Z(:,i));
    IndexMax(i) = I;
    Z_Max(i) = W;
end
end
%% Daten hier filtern, so daß nur die WSP-Kante betrachtet wird und nicht der Rest
function [Z_neu] = WSPKanteFiltern(Z)
AnzX = length(Z(:,1));   % Anzahl X-Werte (über die Schnittkantenbreite)
AnzY = length(Z(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
%
Z_neu=zeros(AnzX, AnzY); % Feld initialisieren
%
for j=1:AnzY
    for i=1:AnzX
        % Zähle, wieviel nicht NaN-Nachbarn auf dieser Linie existieren
        %if (j < AnzY-15)
        if (i < AnzX-19)
            AnzNaN = 0;
            %for k=j:(j+15)
            for k=i:(i+19)
                %if isnan(Z(i,k))    % Anzahl NaN-Nachbarn zählen
                if isnan(Z(k,j))    % Anzahl NaN-Nachbarn zählen
                    AnzNaN = AnzNaN + 1;
                end
            end
            if AnzNaN > 5
                Z_neu(i,j) = NaN;
                %disp('Wert gefiltert')
            else
                Z_neu(i,j) = Z(i,j);
            end
        else
            Z_neu(i,j) = NaN; % letzte 5 Werte am Rand sowieso auf NaN setzen!
        end
    end
end
end
%% Daten hier filtern, so daß nur die WSP-Kante betrachtet wird und nicht der Rest
% Modifikation HK, 18.05.2020; Werte kleiner/größer als maxZ/minZ ausfiltern (NaN-setzen)
function [Z_neu] = WSPKanteMinMaxFiltern(Z, minZ, maxZ)
AnzX = length(Z(:,1));   % Anzahl X-Werte (über die Schnittkantenbreite)
AnzY = length(Z(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
%
Z_neu=zeros(AnzX, AnzY); % Feld initialisieren
%
for j=1:AnzY
    for i=1:AnzX
        Z_neu(i,j) = Z(i,j);
        if Z(i,j) > maxZ
            Z_neu(i,j) = NaN;
        end
        if Z(i,j) < minZ
            Z_neu(i,j) = NaN;
        end
    end
end
end
%% Daten hier filtern: wenn Werte aus den Umgebungswerten abweichen NaN setzen (Analog zu FDM-Stempel)
% Modifikation HK, 20.05.2020;
function [Z_neu] = WSPKanteFDMFilter(Z, Toleranz)
AnzX = length(Z(:,1));   % Anzahl X-Werte (über die Schnittkantenbreite)
AnzY = length(Z(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
%
Z_neu=zeros(AnzX, AnzY); % Feld initialisieren
%
for j=1:AnzY      % Ränder nicht betrachten
    for i=1:AnzX  % Ränder nicht betrachten
        Zmodif = Z(i,j); % aktuelles Z speichern
        if (i>1 && j>1) && (i<AnzX && j<AnzY) && (~isnan(Zmodif))
            minZ = (1.0-Toleranz)*Z(i,j); % -Toleranz
            maxZ = (1.0+Toleranz)*Z(i,j); % +Toleranz

            % Werte links, rechts, unten und oben von Z(i,j) auslesen
            Zl=Z(i-1,j);
            Zr=Z(i+1,j);
            Zu=Z(i,j-1);
            Zo=Z(i,j+1);

            if (isnan(Zl) || isnan(Zr) || isnan(Zu) || isnan(Zo))
                Zmodif = NaN;
            else
                if minZ < Zl || minZ < Zr || minZ < Zu || minZ < Zo
                    Zmodif = (Zl+Zr+Zu+Zo)/4.0;
                elseif maxZ > Zl || maxZ > Zr || maxZ > Zu || maxZ > Zo
                    Zmodif = (Zl+Zr+Zu+Zo)/4.0;
                end
            end
        end
        Z_neu(i,j) = Zmodif;
    end
end
end
%%
%Endradien links und rechts finden -> Bestimmung Nullpunkt ab Eckenradius (Detektion der genauen Schnittpositionen nach den Versuchen anhand Mikroskopie der gebrauchten WSP)
% HK, KW23/2019
function [BeginnSchneide, EndeSchneide] = IdentEckenradien(DatenZMax)
Schneidenbeginn_gefunden = false;
Schneidenende = false;
disp('Eckenradienfindung mu� noch verbessert werden! --> NaN-Leerstellen ausfiltern');
for i = 1:length(DatenZMax)
    if (~isnan(DatenZMax(i)) && ~Schneidenbeginn_gefunden) % Erstes Auftreten (nach der Filterung) eines Nicht-NaN = Schneidenbeginn
        BeginnSchneide = i;
        Schneidenbeginn_gefunden = true;
    end
    %
    if (isnan(DatenZMax(i)) && Schneidenbeginn_gefunden && ~Schneidenende)
        EndeSchneide = i-1;
        Schneidenende = true;
    end
end
end
%% Adaptiert aus MRs SPH-Tutorial; HK, Mi, 05.06.2019 (aus "rings.m")
% Partikelstruktur anlegen
%
%h = 5; % analog Glättungslänge -> wieviele Gitterabstände pro Zelle
%[particles] = InitPartikel(h, x_pixel_size, X, Y)
%[cells, out_particles] = cell_lists(particles);
%[particles] = NachbarInteraktionen(particles, cells, Z_neu);
%[Z_neu_SPH] = PartikelFiltern(particles, X, Y, Z);
%
%%
function [Z_neu_SPH] = PartikelFiltern(particles, X, Y, Z)
AnzX = length(X(:,1));   % Anzahl X-Werte (über die Schnittkantenbreite)
AnzY = length(Y(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
% Partikel anzeigen, die Nachbarn haben
AnzPartikel = length(particles);
PartikelMitNachbarn = 0;
Z_neu_SPH = ones(AnzX, AnzY) * NaN; % neues Höhenfeld mit NaN initialisieren
%Z_neu_SPH = ones(AnzX, AnzY); % neues Höhenfeld mit NaN initialisieren
%
for i=1:AnzPartikel
    Nachbarn = particles(i).Nachbarn;
    %if ((Nachbarn < 11) && (Nachbarn > 0))
    %     if (Nachbarn > 20)
    %         disp(strcat('Partikel ', string(i), ' hat ', string(Nachbarn), ' Nachbarn'))
    %         PartikelMitNachbarn = PartikelMitNachbarn + 1;
    %     end
    if (Nachbarn > 15)
        %disp(strcat('Partikel ', string(i), ' hat ', string(Nachbarn), ' Nachbarn'))
        Index_i = particles(i).i;
        Index_j = particles(i).j;
        Z_neu_SPH(Index_i, Index_j) = Z(Index_i,Index_j);
    end
end
%
figure(5);
contour(X,Y,Z_neu_SPH)
%
figure(6);
s=surf(X,Y,Z_neu_SPH,Z_neu_SPH)
s.EdgeColor = 'none'
axis equal
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function [particles] = InitPartikel(h, x_pixel_size, X, Y)
AnzX = length(X(:,1));   % Anzahl X-Werte (über die Schnittkantenbreite)
AnzY = length(Y(1,:));   % Anzahl Y-Werte (über die Schnittkantenl�nge)
for i = 1:AnzX
    for j = 1:AnzY
        PartikelID = (i -1) * AnzY + j;
        particles(PartikelID).x = X(i,j);
        particles(PartikelID).y = Y(i,j);
        particles(PartikelID).i = i;
        particles(PartikelID).j = j;
        particles(PartikelID).h = h * x_pixel_size;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function [particles] = NachbarInteraktionen(particles, cells, Z)
%particle interactions, cell list iteration
num_box = cells.nx*cells.ny;
%
for b=1:num_box

    bi = ceil(b/cells.ny);
    bj = b - (bi-1)*cells.ny;

    for p = cells.start(b):(cells.end(b)-1)

        if (p==-1)
            continue;
        end

        %load data at p
        xi = particles(p).x;
        yi = particles(p).y;
        hi = particles(p).h;
        particles(p).Nachbarn = 0;
        for nbi = max(bi-1,1):min(bi+1,cells.nx)
            for nbj = max(bj-1,1):min(bj+1,cells.ny)

                nb_idx = (nbi-1)*cells.ny+nbj;

                for q = cells.start(nb_idx):(cells.end(nb_idx)-1)

                    Index_i = particles(p).i;
                    Index_j = particles(p).j;
                    if  ~isnan(Z(Index_i, Index_j))    % auf den Alicona{-Roh}daten arbeiten -> je nachdem, was übergeben wurde
                        %if  ~isnan(Z_neu(Index_i, Index_j)) % auf den vorgefilterten Alicona-Rohdaten arbeiten
                        %load data at q
                        xj = particles(q).x;
                        yj = particles(q).y;
                        %hj = particles(q).h;

                        %[w,w_x,w_y] = cubic_spline_2d([xi,yi],[xj,yj],hi);
                        [w,~,~] = cubic_spline_2d([xi,yi],[xj,yj],hi);             % die Ableitungen werden nicht gebraucht

                        if (w == 0)
                            continue;
                        end

                        particles(p).Nachbarn = particles(p).Nachbarn + 1;
                    end
                end
            end
        end
        %disp(strcat('Partikel ', string(p), ' hat ', string(particles(p).Nachbarn), ' Nachbarn'))
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Adaptiert aus MRs SPH-Tutorial; HK, Mi, 05.06.2019 (aus "rings.m")
% Zelllisten erstellen
function [cells, out_particles] = cell_lists(particles)
N = length(particles);

bbmin_x = min([particles.x])-1e-8;  %nudge because of floating point arithmetic / round off errors
bbmin_y = min([particles.y])-1e-8;

bbmax_x = max([particles.x])+1e-8;
bbmax_y = max([particles.y])+1e-8;

h_max = max([particles.h]);

%%%%

nx = ceil((bbmax_x - bbmin_x)/(2*h_max));
ny = ceil((bbmax_y - bbmin_y)/(2*h_max));

cells = struct('nx',nx,'ny',ny,'bbmin_x',bbmin_x,'bbmin_y',bbmin_y,'h_max',h_max,...
    'start',zeros(size(nx)),'end',zeros(size(nx)));

%assign hashes
hashes = zeros(size(particles));
for i = 1:N
    p = particles(i);
    bix = ceil((p.x-bbmin_x)/(2*h_max));
    biy = ceil((p.y-bbmin_y)/(2*h_max));
    hashes(i) = (bix-1)*ny + biy;
end

%sort particles by hashes
[hashes, sort_idx] = sort(hashes);
particles = particles(sort_idx);
out_particles = particles;

%%%%

%scan particle array to construct the cell lists
cells.start = ones(1,nx*ny)*-1;
cells.end   = ones(1,nx*ny)*-1;

current_box = hashes(1);
cells.start(current_box) = 1;
for i=1:N-1
    if (hashes(i) ~= hashes(i+1))
        cells.end(current_box) = i+1;
        current_box = hashes(i+1);
        cells.start(current_box) = i+1;
    end
end
cells.end(current_box) = N+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Adaptiert aus MRs SPH-Tutorial; HK, Mi, 05.06.2019 (aus "cubic_spline_2d.m")
%
function [w, w_x, w_y, w_xy, w_xx, w_yy] = cubic_spline_2d(xi, xj, h)

h1 = 1./h;

xij   = xi(1)-xj(1);
yij   = xi(2)-xj(2);
rij   = sqrt(xij*xij + yij*yij);
rij3  = rij*rij*rij;

fac = 10/(7.0*pi)*h1*h1;

q    = rij*h1;
q_x  = xij*h1/rij;
q_y  = yij*h1/rij;
q_xy = xij*yij*h1/rij3;
q_xx = yij*yij*h1/rij3;
q_yy = xij*xij*h1/rij3;

w    = 0.;
w_x  = 0.;
w_y  = 0.;
w_xy = 0.;
w_xx = 0.;
w_yy = 0.;

if (q >= 2.)
    return;

elseif (q >= 1.)
    der  = -0.75*(2-q)*(2-q);
    der2 = 3-1.5*q;

    w    =  fac*(0.25*(2-q)*(2-q)*(2-q));
    w_x  = (der*q_x*fac);
    w_y  = (der*q_y*fac);
    w_xy = (der2*q_x*q_y + der*q_xy)*fac;
    w_xx = (der2*q_x*q_x + der*q_xx)*fac;
    w_yy = (der2*q_y*q_y + der*q_yy)*fac;

else
    der  = -3.0*q*(1-0.75*q);
    der2 = 4.5*q-3;
    w    = fac*(1 - 1.5*q*q*(1-0.5*q));

    if (rij > 1e-12)
        w_x  = der*q_x*fac;
        w_y  = der*q_y*fac;
        w_xy = (der2*q_x*q_y + der*q_xy)*fac;
        w_xx = (der2*q_x*q_x + der*q_xx)*fac;
        w_yy = (der2*q_y*q_y + der*q_yy)*fac;

    else
        w_xx = fac*der2;
        w_yy = fac*der2;
    end
end

end
