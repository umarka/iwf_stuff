% Auswertung WSP-Aufnahmen
% Hagen Klippel, Do, 23.05.2019; nach Vorlage von Linus Meier
%
clear all; 
close all;
%
addpath('Version_1_7');
%% Einlesen
AktPfad = strcat(pwd, '/')
WSP001AB = 'WSP_001_AB_dem.al3d'
Bilddaten=AliconaReader(AktPfad, WSP001AB);
%
%% Vorbereitung; kopiert aus "StraighEdge.m", HK, Mo, 03.06.2019
n_mean = 1; % 
sample_size=size((Bilddaten.DepthData)'); % Aufl�sung des Files [Breite(x) H�he(y)]
x_values=(0:sample_size(1)-1)'; 
y_values=(0:sample_size(2)-1)';
x_pixel_size=str2double(Bilddaten.Header.PixelSizeXMeter)*1000; % liest die Pixelgr�sse in Millimeter ein
y_pixel_size=str2double(Bilddaten.Header.PixelSizeYMeter)*1000;

x_values=x_values*x_pixel_size; % Erzeugt einen Vektor mit den X-Werten, startet bei 0, in Millimeter
y_values=y_values*y_pixel_size; 

X=ones(sample_size(2),1)*x_values'; % Erzeugt eine Matrix mit allen X-Werten, hier k�nnte auch direkt mit exportierten Daten eingestiegen werden
Y=y_values*ones(1,sample_size(1));
Bilddaten.DepthData(Bilddaten.DepthData==1e10)=NaN;
Z=double(Bilddaten.DepthData*1e3); % Matrix mit allen H�henwerten, in Millimeter

X_smooth=X(:,1:n_mean:end);
Y_smooth=Y(:,1:n_mean:end);
Z_smooth=NaN*ones(size(Z,1),ceil(size(Z,2)/n_mean));

for i=1:size(Z,1)
    z_smooth=smooth(Z(i,:),n_mean);
    Z_smooth(i,:)=z_smooth(1:n_mean:end);
end
%
X=X_smooth;
Y=Y_smooth;
Z=Z_smooth;
%
figure(1);
contour(X,Y,Z)
%
figure(2);
s=surf(X,Y,Z,Z)
s.EdgeColor = 'none'
axis equal

%% 
results = nan(1, size(Z,2));
for j=1:size(Z,2)
    try
        %kante=getinfolinus_ridge(Y(:,j),Z(:,j),dirinfo(i).name);
        kante=getinfolinus_ridge(Y(:,j),Z(:,j),'WSP001AB');
        results(1,j)=main_conti_profiles(kante);
        disp('ALRIGHT')
    catch
        disp('WE HAVE A PROBLEM')
    end
end
figure
hold on

r=NaN*ones(size(results));
r_n=NaN*ones(size(results));
S=NaN*ones(size(results));
bl=NaN*ones(size(results));
br=NaN*ones(size(results));
delta_r=NaN*ones(size(results));

for i=1:size(results,1)
    for j=1:size(results,2)
        try
            r(i,j)=results(i,j).r;
            r_n(i,j)=results(i,j).r_n;
            S(i,j)=results(i,j).kante.S;
            bl(i,j)=results(i,j).kante.bl;
            br(i,j)=results(i,j).kante.br;
            delta_r(i,j)=results(i,j).kante.delta_r;
        end
    end
    
    plot(r(i,:))
    
end