clear all; 
close all;

%path='D:\Server\linmeier\Projekt_KSS_Blaser\AP2_Referenzmessungen\Referenzschnitt\dritte Versuchsreihe\Alicona\';
%path='D:\Server\linmeier\Projekt_KSS_Blaser\AP2_Referenzmessungen\Referenzschnitt\zweite Versuchsreihe\Alicona neu\';
%addpath('D:\Server\linmeier\Projekt_KSS_Blaser\AP2_Referenzmessungen\Auswertesoftware\Methode Wyen mod Linus\Version 1.7');
path = '../'  % HK, Mo, 03.06.2019
addpath('Version_1_7');

dirinfo = dir(path);
dirinfo(~[dirinfo.isdir]) = [];

tf = ismember( {dirinfo.name}, {'.', '..'});
dirinfo(tf) = [];  %remove current and parent directory.

%colors=distinguishable_colors(length(dirinfo));

for i=1:length(dirinfo)
    dirinfo_tex(i).name=strrep(dirinfo(i).name,'_','\_');
end

for i=1:length(dirinfo)
    file=[path, dirinfo(i).name,'\dem.al3d'];
%     % Eckenradius symmetrisch
%     [R,x_m,y_m,z_m]=ExtractRidge(file);
%     [X_trafo, Y_trafo,Z_trafo]=StraightTrafo(R,x_m,y_m,z_m,file);
%     [X,Y,Z]=ProfilSchnittSchneidkanteRidge(X_trafo, Y_trafo,Z_trafo);
    % Gerade (zweite Serie)
    [X,Y,Z] = StraightEdge(file);
    figure
    contour(X,Y,Z)
    
    for j=1:size(Z,2)
        try
            kante=getinfolinus_ridge(Y(:,j),Z(:,j),dirinfo(i).name);
            results(i,j)=main_conti_profiles(kante);
        catch
        end
    end
end
figure
hold on

r=NaN*ones(size(results));
r_n=NaN*ones(size(results));
S=NaN*ones(size(results));
bl=NaN*ones(size(results));
br=NaN*ones(size(results));
delta_r=NaN*ones(size(results));

for i=1:size(results,1)
    for j=1:size(results,2)
        try
            r(i,j)=results(i,j).r;
            r_n(i,j)=results(i,j).r_n;
            S(i,j)=results(i,j).kante.S;
            bl(i,j)=results(i,j).kante.bl;
            br(i,j)=results(i,j).kante.br;
            delta_r(i,j)=results(i,j).kante.delta_r;
        end
    end
    
    plot(r(i,:))
    
end
