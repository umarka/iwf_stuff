import numpy as np

class Mesh:
    def __init__(self, X, Y, Z, z_min, z_max, x_dim, y_dim):
        self.X = X
        self.Y = Y
        self.Z = Z
        self.Z_original = Z
        self.z_min = z_min
        self.z_max = z_max

        ## Number of pixels in x and y direction
        self.x_dim = x_dim
        self.y_dim = y_dim

        ## ranges of x and y values
        self.x_range = (X[0][0], X[0][-1])
        self.y_range = (Y[0][0], Y[-1][0])

    def min_max_filter(self, min, max):
        ## Sets any values outside [min, max] to nan
        self.Z[self.Z > max] = np.nan 
        self.Z[self.Z < min] = np.nan 

    def shift_to_zero(self):
        ## for every column in array shift by maximum
        ## alligns cutting edge with Z=0 plane
        for x_ind in range(self.x_dim):
            z_values = self.Z[:, x_ind]
            max_z = np.nanmax(z_values)
            z_values = z_values - max_z
            self.Z[:, x_ind] = z_values

    def gaussian_filter(self, tolerance):
        ## Average cell's value with its neighbours (up, down, left, right)
        Z_new = np.zeros((self.y_dim, self.x_dim))
        for i in range(0, Z_new.shape[0]):
            for j in range(0, Z_new.shape[1]):
                Zmodif = self.Z[i][j]
                if (i > 0 and i < Z_new.shape[0]-1) and (j > 0 and j < Z_new.shape[1]-1):
                    minZ = (1.0-tolerance)*self.Z[i][j]; 
                    maxZ = (1.0+tolerance)*self.Z[i][j]; 

                    Zl=self.Z[i-1][ j ];
                    Zr=self.Z[i+1][ j ];
                    Zu=self.Z[i  ][j-1];
                    Zo=self.Z[i  ][j+1];

                    if np.isnan(Zl) or np.isnan(Zr) or np.isnan(Zu) or np.isnan(Zo):
                        Zmodif = np.nan;
                    else:
                        if minZ < Zl or minZ < Zr or minZ < Zu or minZ < Zo:
                            Zmodif = (Zl+Zr+Zu+Zo)/4.0;
                        elif maxZ > Zl or maxZ > Zr or maxZ > Zu or maxZ > Zo:
                            Zmodif = (Zl+Zr+Zu+Zo)/4.0;

                Z_new[i][j] = Zmodif
        self.Z = Z_new

    def filter_cutting_edge(self):
        ## TODO: Why tf is this being done???
        for i in range(0, self.y_dim):
            for j in range(0, self.x_dim):  

                ## Count number of Nan neighbours in this row
                if j < self.x_dim - 19: ## ???
                    no_nan = 0
                    for k in range(j, j+19):
                        if np.isnan(self.Z[i][k]):
                            no_nan += 1
                    if no_nan > 5:
                        self.Z[i][j] = np.nan
                else:
                    self.Z[i][j] = np.nan
