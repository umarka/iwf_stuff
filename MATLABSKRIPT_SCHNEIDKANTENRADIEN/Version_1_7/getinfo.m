function [kante]=getinfo(tab,h,which,reverse);

%determine units (mm, m, etc.) for data in tab
%read data from tab into kante.y and kante.z

if h(3)=='m'
    a=3;% z in mm
else
    if h(3)=='�'
        a=6;% z in �m
    else
        if h(3)=='n'
            a=9;% z in nm
        else
            a=6;%m�krometer if weird character
        end
    end
end


if h(2)=='m'
    b=3;% y in mm
else
    if h(2)=='�'
        b=6;% y in �m
    else
        b=9;% y in nm
    end
end

if h(1)=='m'
    c=3;% y in mm
else
    if h(2)=='�'
        c=6;% y in �m
    else
        c=9;% y in nm
    end
end


if h(4)=='m'
    d=3;% l in mm
else
    if h(4)=='�'
        d=6;% l in �m
    else
        d=9;% l in nm
    end
end
%z
kante.z=tab(:,5)*10^(-a);
kante.z=kante.z(3:length(kante.z));%remove invalid points in vector

%choose between y,x,l
if which=='l'
    w=4;
else
    if which=='x'
        w=2;
    else
        w=3;%which=='l'
    end
end

if w==2
    b=c;
else
    if w==4
        b=d;
    end
end


kante.y=tab(:,w)*10^(-b);%kante.y can be changed to kante.l and kante.x, depending on the user's preference
kante.y=kante.y(3:length(kante.y));%remove invalid points in vector

%reverse point order if user requires this
if reverse==1
    kante.y=seqreverse(kante.y')';
    kante.z=seqreverse(kante.z')';
end

if kante.y(1)>kante.y(end)
   kante.y=-1*kante.y; %ensure that first point coordinate in kante.y is samller than the last point coordinate
end

[kante]=det_max(kante);% determine coordinates and point number of maximum
[kante]=get_edge(kante);% determine cross section in complex numbers

