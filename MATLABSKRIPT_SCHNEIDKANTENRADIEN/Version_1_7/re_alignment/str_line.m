%determine fitted straight lines for both edge sides. also determines
%angle of  each side

function [kante]=str_line(kante,colour1,colour2,plotl,plotr);

%determine least squares straight line for left side and right side of profile
%left
yl=kante.y(kante.LSstartl:kante.LSendl);
zl=kante.z(kante.LSstartl:kante.LSendl);
if plotl,plot(yl,zl,'om');end
%right
yr=kante.y(kante.LSstartr:kante.LSendr);
zr=kante.z(kante.LSstartr:kante.LSendr);
if plotr,plot(yr,zr,'om');end

[kante.strline_left]=polyfit(yl,zl,1);
[kante.strline_right]=polyfit(yr,zr,1);

%define straight line by two points
y_left=[-10 10];
z_left(1)=kante.strline_left(1)*y_left(1)+kante.strline_left(2);%LS line equation 
z_left(2)=kante.strline_left(1)*y_left(2)+kante.strline_left(2);

y_right=[-10 10];
z_right(1)=kante.strline_right(1)*y_right(1)+kante.strline_right(2);%LS line equation
z_right(2)=kante.strline_right(1)*y_right(2)+kante.strline_right(2);

%calculate point of intersection of both LS-lines
[kante.y_inter,kante.z_inter]=polyxpoly(y_left,z_left,y_right,z_right,'unique');

%plots of the sides
if plotr==true
plot(y_right,z_right,colour1);
end
if plotl==true
plot(y_left,z_left,colour2);
end

%determine angles of both edge sides (by using gradient of fitted straight line)
kante.angle_l=atan(kante.strline_left(1));
kante.angle_l=round(kante.angle_l*1e4)/1e4;
kante.angle_r=atan(kante.strline_right(1));
kante.angle_r=round(kante.angle_r*1e4)/1e4;
kante.openingangle=180-180/pi*(abs(kante.angle_l)+abs(kante.angle_r));
%kante.openingangle=round(kante.openingangle*1e2)/1e2;


