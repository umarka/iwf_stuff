function [kante]=det_max(kante);

%determine characteristics of maximum profile point

kante.zmax=max(kante.z);%maximum z-value
kante.zmax_pos=min(find(kante.z==kante.zmax));%position of the maximum z-value, min():in case there is more than one maximum 
kante.ymax=kante.y(min(kante.zmax_pos));% determine corresponding y-value