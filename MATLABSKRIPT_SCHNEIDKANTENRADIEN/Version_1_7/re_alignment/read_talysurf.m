function [Header,Tab,filename] = read_Talysurf(Datei,n);
% Einlesen einer mit Talysurf geschriebenen Datei

Datei
a=length(Datei);
while Datei(a)~='\' 
    a=a-1;
end
filename=Datei(a+1:end);
% 
fid = fopen(Datei, 'r', 'n');


Tab=[];
j=1;
while ~feof(fid) && j<1e6
   j=j+1;
   tmp=fgetl(fid);
   if ~isstr(tmp), break, end
   tmpdata=sscanf(tmp,'%f');
   if isempty(Tab),
      Tab=zeros(n,length(tmpdata));
   end
   Tab(j,1:length(tmpdata))=tmpdata';
   if j/1000==floor(j/1000),

   end
end;
fclose(fid);

k=12;
found=false;
while not(found)
   k=k+1;
   if (abs(Tab(k))-abs(Tab(k+1)))>1, found=true; ,end;
    
end

stop=k;
z=Tab(stop+1:(stop+1)+(stop-13))';
y=Tab(13:stop)';
   
num=[1:1:length(z)]';
x=ones(length(z),1);
l=y;

pan=zeros(length(z),1);
tilt=pan;
Tab=[num,x,y,l,z,pan,tilt];
Header=['mm';'mm';'mm'];