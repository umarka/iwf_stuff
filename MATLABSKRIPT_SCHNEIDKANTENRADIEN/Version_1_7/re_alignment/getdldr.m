function [dl,dr]=getdldr(z_bisect,kl,kr,kante);
% this function first determines the horizontal coordinate of the points of
% intercection between the horizontal line through "z_bisect" and the
% straight lines kante.strline_left and kante.strline_right of the measured
% edge. Then the vertical distance between the line through z_bisect and
% the measured edge on the left and right is determined. these values are
% dl and dr. the value of the ratio between dl and dr can be used to determine the asymmetry
% of the measured edge.

%determine horizontal coordinates (sl (left), sr (right)) of points of intersection between horizontal (green) line and clearance and rake straight lines

sl=(z_bisect-kante.strline_left(2))/kante.strline_left(1);
sr=(z_bisect-kante.strline_right(2))/kante.strline_right(1);

%determine which point of the measured edge lies closest to the vertical
%line through sl
k=kl;

while real(kante.edge(k))<sl
    k=k+1;
end
if (abs(real(kante.edge(k-1))-sl))<(abs(real(kante.edge(k))-sl))
    k=k-1;
end
%determine dl (vertical distance between z_bisect and point k on kante.edge, which lies closest to sl)
if imag(kante.edge(k))<z_bisect, signn=1;,else,signn=-1;end;
dl=signn*abs(z_bisect-imag(kante.edge(k)));


%determine which point of the measured edge lies closest to the vertical
%line through sr
k=kr;

while real(kante.edge(k))>sr
    k=k-1;
end

if (abs(real(kante.edge(k+1))-sr))<(abs(real(kante.edge(k))-sr))
    k=k+1;
end
%determine dr (vertical distance between z_bisect and point k on
%kante.edge, which lies closest to sr)
if imag(kante.edge(k))<z_bisect, signn=1;,else,signn=-1;end;
dr=signn*abs(z_bisect-(imag(kante.edge(k))));