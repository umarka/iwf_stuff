function [kante]=trim_edge(kante,length_f);

% trims length of flank and face to 'length_f' 
k=0;
found_l=false;
found_r=false;

edge_temp=kante.edge;

while not(found_l)
    k=k+1;
    if abs(kante.edge(k)-kante.edge(kante.zmax_pos))<=length_f
        found_l=true;
    end
    
end 
k_min=k-1;
if k_min<=0,k_min=1;,end

k=kante.zmax_pos;%jump forward to get to the right side

while not(found_r)
    
    if abs(kante.edge(k)-kante.edge(kante.zmax_pos))>=length_f
        found_r=true;
    end
    k=k+1;
    if k>length(kante.edge)
        found_r=true;
    end
    
end 
k_max=k-1;

kante.edge=[0];
kante.edge=edge_temp(k_min:k_max);
[kante]=get_y_z(kante);

