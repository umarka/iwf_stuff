
function [kante,z_bisect,plc1,plc2]=getdelta_r(kante,plotdr);
%calculate delta_r

t=30;%number of points to include in the search for point closest to angle bisector

for k=kante.zmax_pos-t:kante.zmax_pos+t
    diffr(k-(kante.zmax_pos-t)+1)=abs(real(kante.edge(k))); %horizontal distance to angle bisector
    %if point lies on angle bisector then diffr==0;
    pos(k-(kante.zmax_pos-t)+1)=k;%store position of points
end
together=[diffr' pos'];%Matrix with differences in 1st column and the point index in 2nd column
a=find(min(together(:,1))==together(:,1));
plc1=together(a,2);%plc1: position (in kante.edge) of the point closest to opening angle bisector

if together(a-1,1)<together(a+1,1)%find second closest point to angle bisector
    plc2=together(a-1,2);%plc2: position of point 2nd closest to angle bisector.
else
    plc2=together(a+1,2);
end

%x- and y-coordinates of points in plc1 and plc2
px1=real(kante.edge(plc1));
py1=imag(kante.edge(plc1));
px2=real(kante.edge(plc2));
py2=imag(kante.edge(plc2));

%straight line through points at plc1 and plc2
line_params=polyfit([px1 px2],[py1 py2],1);
z_bisect=line_params(2);%z-coordinate of intercection between angle bisector and straight line (line_params)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kante.delta_r=abs(z_bisect);%delta_r
kante.delta_r=round(kante.delta_r*10e7)/10e7;%round delta_r
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%plot([0 real(kante.edge(plc1))],[0 imag(kante.edge(plc1))],'m');
if plotdr==1, plot([0 0],[0 z_bisect],'r');%plot delta_r
    if sign(px1)==sign(px2)
        disp('Warning: both points closest to angle bisector have the same sign! delta_r might have a small error!')
    end
end