function [kante_shift]= shift_any(kante,y,z);

%shift a profile by y and z 
kante_shift=kante;

for i=1:length(kante_shift.z)

    kante_shift.z(i)=kante_shift.z(i)-z;
    kante_shift.y(i)=kante_shift.y(i)-y;

end

[kante_shift]=get_edge(kante_shift);
[kante_shift]=det_max(kante_shift);