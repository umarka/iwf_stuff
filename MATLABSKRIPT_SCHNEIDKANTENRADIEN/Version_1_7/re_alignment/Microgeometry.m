function varargout = Microgeometry(varargin)
% MICROGEOMETRY M-file for Microgeometry.fig
%      MICROGEOMETRY, by itself, creates a new MICROGEOMETRY or raises the existing
%      singleton*.
%
%      H = MICROGEOMETRY returns the handle to a new MICROGEOMETRY or the handle to
%      the existing singleton*.
%
%      MICROGEOMETRY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MICROGEOMETRY.M with the given input arguments.
%
%      MICROGEOMETRY('Property','Value',...) creates a new MICROGEOMETRY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Microgeometry_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Microgeometry_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Microgeometry

% Last Modified by GUIDE v2.5 26-Jan-2009 13:49:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Microgeometry_OpeningFcn, ...
    'gui_OutputFcn',  @Microgeometry_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Microgeometry is made visible.
function Microgeometry_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Microgeometry (see VARARGIN)

% Choose default command line output for Microgeometry
handles.output = hObject;
set(0,'Units','pixels')
scnsize = get(0,'ScreenSize');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Microgeometry wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Microgeometry_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_open.
function btn_open_Callback(hObject, eventdata, handles)
% hObject    handle to btn_open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global pathname
global filename
global Ref_t Ref_z
global richtung
global parsing
global viewr
global viewl

viewl=1.03;
viewr=1.03;

set(0,'Units','pixels')
scnsize = get(0,'ScreenSize');

if not(parsing)
    [filename, pathname] = uigetfile('*.txt;*.mod', 'Open File...');
end


%sres=sscanf(filename,'K%i %i %s P%i T%i D%i W%i N%i');

if isequal(filename,0) || isequal(pathname,0)

else
    set(handles.pnl_limits,'Visible','on');
    set(handles.plot_left,'Visible','on');
    set(handles.plot_right,'Visible','on');
    set(handles.txt_location,'String',pathname);
    set(handles.txt_kname,'String',filename);
    set(handles.pnl_info,'Visible','on');
    set(handles.pnl_filter,'Visible','on');
    set(handles.pnl_original_plot,'Visible','on');
    set(handles.pnl_plot_man,'Visible','on');
    set(handles.pnl_uncertainty,'Visible','on');
    set(handles.main,'toolbar','figure');
    
    params_in.bman=0; %b-section is determined automatically
    [params_in]=getparams(handles,filename,params_in);%get parameters from editboxes, radiobuttons etc.
    params_in.view=viewl;
    %normal b-section calculation
    axes(handles.plot_left);
    cla;    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [params]=main(pathname,filename,params_in,handles);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    setboxes(handles,params); %show information gained from main() in GUI
    set(handles.btn_saveplot_left,'Visible','on');
    set(handles.btn_save,'Visible','on');

    
    if not(parsing)
        
    %calculation for manual b-section plot
    axes(handles.plot_right);
    cla;
    params_in.bman=1; 
    params_in.bl=str2num(get(handles.edt_bl,'String'));
    params_in.br=str2num(get(handles.edt_br,'String'));
    params_in.view=viewr;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [params]=main(pathname,filename,params_in,handles);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    set(handles.txt_r2d,'String',num2str((round(params.r*1e7))/10));
    set(handles.txt_ur2d,'String',num2str((round(params.ur*1e7))/10));
    
    setboxesb(handles,params);


    end
end
% --------------------------------------------------------------------


function Main_Callback(hObject, eventdata, handles)
% hObject    handle to Main (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in execute.
function execute_Callback(hObject, eventdata, handles)
% hObject    handle to execute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function txt_kname_Callback(hObject, eventdata, handles)
% hObject    handle to txt_kname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_kname as text
%        str2double(get(hObject,'String')) returns contents of txt_kname as a double


% --- Executes during object creation, after setting all properties.
function txt_kname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_kname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_location_Callback(hObject, eventdata, handles)
% hObject    handle to txt_location (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_location as text
%        str2double(get(hObject,'String')) returns contents of txt_location as a double


% --- Executes during object creation, after setting all properties.
function txt_location_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_location (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function txt_rn_original_Callback(hObject, eventdata, handles)
% hObject    handle to txt_rn_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_rn_original as text
%        str2double(get(hObject,'String')) returns contents of txt_rn_original as a double


% --- Executes during object creation, after setting all properties.
function txt_rn_original_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_rn_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_r_original_Callback(hObject, eventdata, handles)
% hObject    handle to txt_r_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_r_original as text
%        str2double(get(hObject,'String')) returns contents of txt_r_original as a double


% --- Executes during object creation, after setting all properties.
function txt_r_original_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_r_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_rn_extb_Callback(hObject, eventdata, handles)
% hObject    handle to txt_rn_extb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_rn_extb as text
%        str2double(get(hObject,'String')) returns contents of txt_rn_extb as a double


% --- Executes during object creation, after setting all properties.
function txt_rn_extb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_rn_extb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_r_man_Callback(hObject, eventdata, handles)
% hObject    handle to txt_r_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_r_man as text
%        str2double(get(hObject,'String')) returns contents of txt_r_man as a double


% --- Executes during object creation, after setting all properties.
function txt_r_man_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_r_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_recalc.
function btn_recalc_Callback(hObject, eventdata, handles)
% hObject    handle to btn_recalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewl
global viewr

%recalculates the edge data based on user input through text- and editboxes
location=get(handles.txt_location,'String');
kname=get(handles.txt_kname,'String');

%automatic b-section calculation
params_in.bman=0; %automatic calculation of b-section
[params_in]=getparams(handles,kname,params_in);

axes(handles.plot_left);
cla;
params_in.bl=-1;
params_in.br=1;
params_in.view=viewl;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[params]=main(location,kname,params_in,handles);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

setboxes(handles,params);



% --- Executes on button press in btn_viewentireedge.
function btn_view_Callback(hObject, eventdata, handles)
% hObject    handle to btn_viewentireedge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewl
%changes size of plot axis
axes(handles.plot_left);
viewl=0.12;
axis([-0.99/viewl*1e-4 0.99/viewl*1e-4 -1.59/viewl*1e-4 0.39/viewl*1e-4]);




% --- Executes on button press in btn_directory.
function btn_directory_Callback(hObject, eventdata, handles)
% hObject    handle to btn_directory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filegenlist
global pathname

%open all '.txt' files in a directory
dname = uigetdir('O:\Offener Ordner\Felix Rohner\Bachelorarbeit HS2008\Alicona\repeatability of measurements\','Select Directory to Open for Parsing...');
pathname=dname;
pathname=sprintf('%s',pathname,'\');
if (dname==0)

    set(handles.lst_directory,'String','')
else
    set(handles.lst_directory,'Visible','on');
    set(handles.btn_parsing,'Visible','on');
    D = dir(dname);
    ss=size(D);
    filegenlist=[];
    for i=3:ss(1),
        filegenlist=strvcat(filegenlist,[D(i).name]);
    end;

    sis=size(filegenlist);
    k=1;
    for i=1:sis(1)
        fname=strcat(filegenlist(i,:));
        sz=size(fname);
        if sz(2)>3
            if fname(:,(end-3:end))=='.txt'
                flist(k,:)=filegenlist(i,:);
                k=k+1;
            end
        end
    end
    set(handles.lst_directory,'String',flist);
    %set(handles.TagFileList,'String',filegenlist);
end


% --- Executes on selection change in lst_directory.
function lst_directory_Callback(hObject, eventdata, handles)
% hObject    handle to lst_directory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns lst_directory contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lst_directory


% --- Executes during object creation, after setting all properties.
function lst_directory_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lst_directory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in btn_parsing.
function btn_parsing_Callback(hObject, eventdata, handles)
% hObject    handle to btn_parsing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%automatically opens and runs all files in filegenlist

global filegenlist
global parsing
global filename
global pathname
global save_filename
global save_path
global xlsarray

xlsarray=[];

if size(get(handles.lst_directory,'String'))~=0
    parsing=1;
    [save_filename,save_path]=uiputfile('Parameters.xls','Save File As...');
    delete([save_path,save_filename]);
    xlswrite([save_path,save_filename],'results','0');
    ss=size(filegenlist)

    for i=1:ss(1)
        wb=waitbar(i/ss(1),'Parsing...')
        close(wb);
        filename=strcat(filegenlist(i,:));
        sz=size(filename);
        if sz(2)>3
            if filename(:,(end-3:end))=='.txt'
                if filename(:,(1:7))~='results'
                btn_open_Callback(hObject, eventdata, handles);
                btn_save_Callback(hObject, eventdata, handles);
                end
            end
        end
    end
     d={'Keilwinkel' 'Plattennummer' 'Schneidkante' 'Druck' 'Zeit' 'Abstand' 'Strahlwinkel' 'Position' 'Skewness' 'Skewness (2*D)' 'S' 'S (2*D)' 'dl' 'dr' 'Kurtosis' 'r_n' 'delta_r' 'Number of points in b' 'r' 'r (2*D)' 'horizontal position of circle centre' 'ratio of circle centre position and circle radius r' 'point measurement uncertainty' 'uncertainty of r' 'uncertainty of r (2*D)' 'angular range of points in b' 'Calculated Wedge Angle'};
     xlswrite([save_path,save_filename],d,'results','A1');
    xlswrite([save_path,save_filename],[xlsarray],'results','A2');
            % Schneidkantenseite in Buchstaben
    vec=sprintf('%c',xlsarray(:,3));
    xlswrite([save_path,save_filename], vec', 'results', 'C2');
end
parsing=0;



% --- Executes during object creation, after setting all properties.
function main_CreateFcn(hObject, eventdata, handles)
% hObject    handle to main (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global parsing
global limits
global limitsb
parsing=0;
limits=0;
limitsb=0;


% --- Executes on button press in btn_save.
function btn_save_Callback(hObject, eventdata, handles)
% hObject    handle to btn_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%saves the data of left plot to excel sheet
global save_filename
global save_path
global parsing
global xlsarray

filename=get(handles.txt_kname,'String');
if length(filename)>14
[pp]=FileFormat(filename);

else
    pp=[0 0 0 0 0 0 0 0];
end

if not(parsing)
    [save_filename,save_path]=uiputfile('Parameters.xls','Save File As...');
    delete([save_path,save_filename]);
    d={'Keilwinkel' 'Plattennummer' 'Schneidkante' 'Druck' 'Zeit' 'Abstand' 'Strahlwinkel' 'Position' 'Skewness' 'Skewness (2*D)' 'S' 'S (2*D)' 'dl' 'dr' 'Kurtosis' 'r_n' 'delta_r' 'Number of points in b' 'r' 'r (2*D)' 'horizontal position of circle centre' 'ratio of circle centre position and circle radius r' 'point measurement uncertainty' 'uncertainty of r' 'uncertainty of r (2*D)' 'angular range of points in b' 'Calculated Wedge Angle'};
    xlswrite([save_path,save_filename],d,'results','A1');
end

params(1)=str2num(get(handles.txt_skewness_num,'String'));
params(2)=str2num(get(handles.txt_skewness2d,'String'));
params(3)=str2num(get(handles.txt_S_numnew,'String'));
params(4)=str2num(get(handles.txt_S2d,'String'));
params(5)=str2num(get(handles.txt_dl,'String'));
params(6)=str2num(get(handles.txt_dr,'String'));
params(7)=str2num(get(handles.txt_kurtosis_num,'String'));
params(8)=str2num(get(handles.txt_rn_original,'String'));
params(9)=str2num(get(handles.txt_delta_r,'String'));
params(10)=str2num(get(handles.txt_bnumoriginal,'String'));
params(11)=str2num(get(handles.txt_r_original,'String'));
params(12)=str2num(get(handles.txt_r2d,'String'));
params(13)=str2num(get(handles.txt_centr_left,'String'));
params(14)=str2num(get(handles.txt_quot_left,'String'));
params(15)=str2num(get(handles.edt_txt_up,'String'));
params(16)=str2num(get(handles.txt_uroriginal,'String'));
params(17)=str2num(get(handles.txt_ur2d,'String'));
params(18)=str2num(get(handles.txt_arr,'String'));
params(19)=str2num(get(handles.txt_openingangle,'String'));


if not(parsing)
    xlswrite([save_path,save_filename],[pp params],'results','A2');
        % Schneidkantenseite in Buchstaben
    vec=sprintf('%c',pp(:,3));
    xlswrite([save_path,save_filename], vec', 'results', 'C2');
else

    sz=size(xlsarray);
    xlsarray(sz(1)+1,:)= [pp params];
end



% --- Executes on button press in btn_saveplot_left.
function btn_saveplot_left_Callback(hObject, eventdata, handles)
% hObject    handle to btn_saveplot_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%saves printscreen of entire GUI
h=handles.plot_left;
axes(h);
[save_i,save_pathi]=uiputfile('Plot1.bmp','Save Image As...');
if isequal(save_i,0) || isequal(save_pathi,0)

else
    wb2=waitbar(0.5,'Saving image');
    saveas(h,[save_pathi,save_i]);
    close(wb2);
    wb2=waitbar(1,'Done');
    close(wb2);
end;


function edt_upperlimit1_Callback(hObject, eventdata, handles)
% hObject    handle to edt_upperlimit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_upperlimit1 as text
%        str2double(get(hObject,'String')) returns contents of edt_upperlimit1 as a double


% --- Executes during object creation, after setting all properties.
function edt_upperlimit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_upperlimit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_lowerlimit1_Callback(hObject, eventdata, handles)
% hObject    handle to edt_lowerlimit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_lowerlimit1 as text
%        str2double(get(hObject,'String')) returns contents of edt_lowerlimit1 as a double


% --- Executes during object creation, after setting all properties.
function edt_lowerlimit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_lowerlimit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on mouse press over axes background.
function plot_right_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to plot_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global what %determines which type of limits the user is setting when he/she clicks onto plot_right

ppoint=get(handles.plot_right,'CurrentPoint');
axes(handles.plot_right);
%zmax=str2num(get(handles.txt_zmax,'String'));
%if ppoint(1,2)<=zmax
zmax=str2num(get(handles.txt_zmax,'String'));

if what==1
    if ppoint(1,2)<=zmax
        plot([-1 1],[ppoint(1,2) ppoint(1,2)],'--m');
        set(handles.edt_upper_man,'String',num2str(ppoint(1,2)));
        set(handles.txt_todo,'String','Select LOWER limit for LS-Straight lines!');
        set(handles.txt_currentpos,'String',num2str(ppoint(1,2)));
        what=2;
    end
else

    if what==2
        if ppoint(1,2)<=str2num(get(handles.txt_currentpos,'String'))
            plot([-1 1],[ppoint(1,2) ppoint(1,2)],'--m');
            set(handles.edt_lower_man,'String',num2str(ppoint(1,2)));
            set(handles.txt_todo,'Visible','off');
            what=0;
                    set(handles.btn_recalcr,'enable','on');
                    set(handles.btn_blimits,'enable','on');
        end
    else
        if what==3
            if ppoint(1,1)<0
                plot([ppoint(1,1) ppoint(1,1)],[-1 1],'--g');
                set(handles.edt_bl,'String',num2str(ppoint(1,1)));
                set(handles.txt_todo,'String','Select RIGHT limit for b-section!');
                set(handles.txt_currentpos,'String',num2str(ppoint(1,1)));
                what=4;
            end
        else
            if what==4
                if ppoint(1,1)>str2num(get(handles.txt_currentpos,'String'))
                    plot([ppoint(1,1) ppoint(1,1)],[-1 1],'--g');
                    set(handles.edt_br,'String',num2str(ppoint(1,1)));
                    set(handles.txt_todo,'String','done');
                    set(handles.txt_todo,'Visible','off');
                    set(handles.btn_recalcr,'enable','on');
                    set(handles.btn_limits,'enable','on');
                    what=0;
                end
            end
        end
    end
end


function setboxes(handles,params)

set(handles.txt_rn_original,'String',num2str(round(params.r_n*1e7)/10));
set(handles.txt_r_original,'String',num2str(round(params.r*1e7)/10));
set(handles.txt_bnumoriginal,'String',num2str(params.b));
set(handles.txt_skewness_num,'String',num2str(params.kante.skewness));
set(handles.txt_S_numnew,'String',num2str(params.kante.Snew));
set(handles.txt_kurtosis_num,'String',num2str(params.kante.kurtosis));
set(handles.txt_openingangle,'String',num2str(params.kante.openingangle));
set(handles.txt_delta_r,'String',num2str(round(params.kante.delta_r*1e7)/10));
set(handles.txt_arr,'String',num2str(params.arr));
set(handles.txt_uroriginal,'String',num2str(round(params.ur*1e7)/10));
set(handles.txt_centr_left,'String',num2str(round(params.xc*1e7)/10));
set(handles.txt_quot_left,'String',num2str(abs(params.xc)/params.r));
set(handles.txt_num_outliers,'String',num2str(params.num_outliers));
set(handles.txt_dl,'String',num2str(round(params.dl*1e7)/10));
set(handles.txt_dr,'String',num2str(round(params.dr*1e7)/10));
    set(handles.edt_bl,'String',num2str(-params.r_n*2));
    set(handles.edt_br,'String',num2str(params.r_n*2));    
    set(handles.txt_totalpoints,'String',num2str(params.totalpoints));

function setboxesb(handles,params)
set(handles.txt_zmax,'String',num2str(params.zmax));
set(handles.txt_rn_man,'String',num2str(round(params.r_n*1e7)/10));
set(handles.txt_delta_r_man,'String',num2str(round(params.kante.delta_r*1e7)/10));
set(handles.txt_skewness_num_man,'String',num2str(params.kante.skewness));
set(handles.txt_S_numnew_man,'String',num2str(params.kante.Snew));
set(handles.txt_kurtosis_num_man,'String',num2str(params.kante.kurtosis));
set(handles.txt_bnum_man,'String',num2str(params.b));
set(handles.txt_r_man,'String',num2str(round(params.r*1e7)/10));
set(handles.txt_arr_man,'String',num2str(params.arr));
set(handles.txt_openingangle_man,'String',num2str(params.kante.openingangle));
set(handles.txt_ur_man,'String',num2str(round(params.ur*1e7)/10));
set(handles.txt_centr_man,'String',num2str(round(params.xc*1e7)/10));
set(handles.txt_quot_man,'String',num2str(abs(params.xc)/params.r));
set(handles.txt_num_outliers,'String',num2str(params.num_outliers));
set(handles.txt_dl_man,'String',num2str(round(params.dl*1e7)/10));
set(handles.txt_dr_man,'String',num2str(round(params.dr*1e7)/10));
    set(handles.edt_bl,'String',num2str(params.kante.bl));
    set(handles.edt_br,'String',num2str(params.kante.br));
    set(handles.txt_S2d,'String',num2str(params.kante.Snew));
    set(handles.txt_skewness2d,'String',num2str(params.kante.skewness));

function [params_in]=getparams(handles,filename,params_in)

    %determine if file used is an alicona or a talysurf file
    if filename(end-2:end)=='txt'
        params_in.which=1;
    else
        params_in.which=0;
    end

    %determine if x,y or l information from textfile is used
    if get(handles.radio_l,'Value')==1
        params_in.type='l';
    else
        if get(handles.radio_y,'Value')==1
            params_in.type='y';
        else
            if get(handles.radio_x,'Value')==1
                params_in.type='x';
            end
        end
    end

    %user can reverse the x,y or l data
    if get(handles.chk_reverse,'Value')==1
        params_in.reverse=1;
    else
        params_in.reverse=0;
    end
    
    %user determines where dl and dr are determined (for calculation of asymmetry)
    if get(handles.radio_b,'Value')==1
        params_in.d_type=1;
    else
        params_in.d_type=2;
    end
   
    
  if params_in.bman==0
    %limits for LS Straight line calculations
    params_in.upperlimit1=-str2num(get(handles.edt_upperlimit1,'String'))*1e-6;
    params_in.lowerlimit1=-str2num(get(handles.edt_lowerlimit1,'String'))*1e-6;
    params_in.upperlimit2=-str2num(get(handles.edt_upperlimit2,'String'))*1e-6;
    params_in.lowerlimit2=-str2num(get(handles.edt_lowerlimit2,'String'))*1e-6;
  else
          params_in.upperlimit1=str2num(get(handles.edt_upper_man,'String'));
    params_in.lowerlimit1=str2num(get(handles.edt_lower_man,'String'));
  end
    %measurementuncertainty of one point
    params_in.u_p=str2num(get(handles.edt_txt_up,'String'))*1e-6;
    
    params_in.filter=str2num(get(handles.edt_filter,'String'));

function edt_txt_up_Callback(hObject, eventdata, handles)
% hObject    handle to edt_txt_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_txt_up as text
%        str2double(get(hObject,'String')) returns contents of edt_txt_up as a double


% --- Executes during object creation, after setting all properties.
function edt_txt_up_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_txt_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press over edt_txt_up with no controls selected.
function edt_txt_up_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to edt_txt_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num=str2num(get(handles.edt_txt_up,'String'));
if num<20
    num=num+1;
end
set(handles.edt_txt_up,'String',num2str(num));

% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num=str2num(get(handles.edt_txt_up,'String'));
if num>1
    num=num-1;
end
set(handles.edt_txt_up,'String',num2str(num));



% --- Executes on button press in chk_reverse.
function chk_reverse_Callback(hObject, eventdata, handles)
% hObject    handle to chk_reverse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_reverse





function edt_upperlimit2_Callback(hObject, eventdata, handles)
% hObject    handle to edt_upperlimit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_upperlimit2 as text
%        str2double(get(hObject,'String')) returns contents of edt_upperlimit2 as a double


% --- Executes during object creation, after setting all properties.
function edt_upperlimit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_upperlimit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_lowerlimit2_Callback(hObject, eventdata, handles)
% hObject    handle to edt_lowerlimit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_lowerlimit2 as text
%        str2double(get(hObject,'String')) returns contents of edt_lowerlimit2 as a double


% --- Executes during object creation, after setting all properties.
function edt_lowerlimit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_lowerlimit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on mouse press over axes background.
function plot_left_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to plot_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


    %{   

%}



% --- Executes on button press in btn_recalcr.
function btn_recalcr_Callback(hObject, eventdata, handles)
% hObject    handle to btn_recalcr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global viewr

location=get(handles.txt_location,'String');
kname=get(handles.txt_kname,'String');

params_in.bman=1;
[params_in]=getparams(handles,kname,params_in);

axes(handles.plot_right);
cla;

params_in.bl=str2num(get(handles.edt_bl,'String'));
params_in.br=str2num(get(handles.edt_br,'String'));

params_in.lowerlimit1=str2num(get(handles.edt_lower_man,'String'));
params_in.upperlimit1=str2num(get(handles.edt_upper_man,'String'));

if params_in.bl<params_in.br
    params_in.view=viewr;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [params]=main(location,kname,params_in,handles);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    setboxesb(handles,params);
else
    msgbox('The left limit for the b section must be chosen smaller than the right region!','Warning','warn')
end


% --- Executes on button press in btn_viewentireedger.
function btn_viewentireedger_Callback(hObject, eventdata, handles)
% hObject    handle to btn_viewentireedger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btn_viewr.
function btn_viewr_Callback(hObject, eventdata, handles)
% hObject    handle to btn_viewr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewr
axes(handles.plot_right);
viewr=0.12;
axis([-0.99/viewr*1e-4 0.99/viewr*1e-4 -1.59/viewr*1e-4 0.39/viewr*1e-4]);





% --- Executes on button press in btn_limits.
function btn_limits_Callback(hObject, eventdata, handles)
% hObject    handle to btn_limits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global what
set(handles.txt_todo,'Visible','on');
set(handles.txt_todo,'String','Select UPPER limit for LS-Straight lines!');
set(handles.btn_blimits,'enable','off');
set(handles.btn_recalcr,'enable','off');
what=1;

% --- Executes on button press in btn_blimits.
function btn_blimits_Callback(hObject, eventdata, handles)
% hObject    handle to btn_blimits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global what
set(handles.txt_todo,'Visible','on');
set(handles.txt_todo,'String','Select LEFT limit for b-section!');
set(handles.btn_limits,'enable','off');
set(handles.btn_recalcr,'enable','off');
what=3;

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_lower_man_Callback(hObject, eventdata, handles)
% hObject    handle to edt_lower_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_lower_man as text
%        str2double(get(hObject,'String')) returns contents of edt_lower_man as a double


% --- Executes during object creation, after setting all properties.
function edt_lower_man_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_lower_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_bl_Callback(hObject, eventdata, handles)
% hObject    handle to edt_bl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_bl as text
%        str2double(get(hObject,'String')) returns contents of edt_bl as a double


% --- Executes during object creation, after setting all properties.
function edt_bl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_bl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_br_Callback(hObject, eventdata, handles)
% hObject    handle to edt_br (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_br as text
%        str2double(get(hObject,'String')) returns contents of edt_br as a double


% --- Executes during object creation, after setting all properties.
function edt_br_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_br (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function edt_upper_man_Callback(hObject, eventdata, handles)
% hObject    handle to edt_upper_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_upper_man as text
%        str2double(get(hObject,'String')) returns contents of edt_upper_man as a double


% --- Executes during object creation, after setting all properties.
function edt_upper_man_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_upper_man (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in btn_filter.
function btn_filter_Callback(hObject, eventdata, handles)
% hObject    handle to btn_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edt_filter_Callback(hObject, eventdata, handles)
% hObject    handle to edt_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_filter as text
%        str2double(get(hObject,'String')) returns contents of edt_filter as a double


% --- Executes during object creation, after setting all properties.
function edt_filter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num=str2num(get(handles.edt_filter,'String'));
if num<10
    num=num+1;
end
set(handles.edt_filter,'String',num2str(num));

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num=str2num(get(handles.edt_filter,'String'));
if num>0
    num=num-1;
end
set(handles.edt_filter,'String',num2str(num));



% --- Executes on button press in btn_zoomin_l.
function btn_zoomin_l_Callback(hObject, eventdata, handles)
% hObject    handle to btn_zoomin_l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewl
axes(handles.plot_left);
viewl=viewl+0.1;
axis([-0.99/viewl*1e-4 0.99/viewl*1e-4 -1.59/viewl*1e-4 0.39/viewl*1e-4]);


% --- Executes on button press in btn_zoomout_l.
function btn_zoomout_l_Callback(hObject, eventdata, handles)
% hObject    handle to btn_zoomout_l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewl
axes(handles.plot_left);
viewl=viewl-0.1;
axis([-0.99/viewl*1e-4 0.99/viewl*1e-4 -1.59/viewl*1e-4 0.39/viewl*1e-4]);

% --- Executes on button press in btn_zoomin_r.
function btn_zoomin_r_Callback(hObject, eventdata, handles)
% hObject    handle to btn_zoomin_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewr
axes(handles.plot_right);
viewr=viewr+0.1;
axis([-0.99/viewr*1e-4 0.99/viewr*1e-4 -1.59/viewr*1e-4 0.39/viewr*1e-4]);

% --- Executes on button press in btn_zoomout_r.
function btn_zoomout_r_Callback(hObject, eventdata, handles)
% hObject    handle to btn_zoomout_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global viewr
axes(handles.plot_right);
viewr=viewr-0.1;
axis([-0.99/viewr*1e-4 0.99/viewr*1e-4 -1.59/viewr*1e-4 0.39/viewr*1e-4]);


% --- Executes on button press in btn_img_parsing.
function btn_img_parsing_Callback(hObject, eventdata, handles)
% hObject    handle to btn_img_parsing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


