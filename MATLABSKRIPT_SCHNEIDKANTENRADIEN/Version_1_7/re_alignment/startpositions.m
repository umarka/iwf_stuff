function [kante]=startpositions(kante,upperlimit,lowerlimit);
%determine for straight line calculations:
%left side:
%kante.LSstartl
%kante.LSendl
%right side:
%kante.LSstartr
%kante.LSendr

if lowerlimit<upperlimit
    
    if imag(kante.edge(1))<lowerlimit
        k=1;
        while (imag(kante.edge(k))<lowerlimit)
            k=k+1;
        end
        kante.LSstartl=k;

        if imag(kante.edge(end))<lowerlimit
            k=length(kante.edge);
            while (imag(kante.edge(k))<lowerlimit)
                k=k-1;
            end
            kante.LSendr=k;
        else
             msgbox('Lower limit is lower than lowest point on measured edge. Lowest point has been chosen as lower limit. ','Warning','warn');
            kante.LSendr=length(kante.edge);
            pause(5)
        end
    else
        kante.LSstartl=1;
        kante.LSendr=length(kante.edge);
        msgbox('Lower limit is lower than lowest point on measured edge. Lowest point has been chosen as lower limit. ','Warning','warn');
        pause(5)
    end;
    
    
    if imag(kante.edge(2))<upperlimit
        k=1;
        while (imag(kante.edge(k))<upperlimit)
            k=k+1;
        end
        kante.LSendl=k;

        if imag(kante.edge(end-1))<upperlimit
            k=length(kante.edge)-1;
            while (imag(kante.edge(k))<upperlimit)
                k=k-1;
            end
            kante.LSstartr=k;
        else
            kante.LSstartr=length(kante.edge)-1;
            msgbox('Upper limit is lower than lowest point on measured edge!','Warning','warn');
            pause(5)
        end
    else
        kante.LSendl=1;
        kante.LSstartr=length(kante.edge)-1;
        msgbox('Upper limit is lower than lowest point on measured edge! ','Warning','warn');
        pause(5)
    end


end