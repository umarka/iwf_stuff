function [kante]=re_alignment(kante)
    
    [kante]=str_line(kante,'b','c',0,0); 
    [kante]=shift_any(kante,kante.y_inter,kante.z_inter);
    [kante]=str_line(kante,'b','c',0,0);
    % angle for rotation
    alpha=(kante.angle_l+kante.angle_r)/2;
    % rotate again
    [kante]=rotate_edge(alpha,kante,1);
    [kante]=str_line(kante,'b','c',0,0);
    [kante]=shift_any(kante,kante.y_inter,kante.z_inter);
    [kante]=str_line(kante,'b','c',0,0);