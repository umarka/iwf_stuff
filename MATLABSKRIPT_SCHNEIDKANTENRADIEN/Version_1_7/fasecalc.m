function [fase_angle,fase_length,R_squared_f]=fasecalc(kl,kr,pg,kante)

%fit straight line to both the left and the right hand side of the b section (seperated by the angle bisector)
yl=kante.y(kl:(kl+round(pg/2)));
zl=kante.z(kl:(kl+round(pg/2)));
%plot(yl,zl,'oc');

yr=kante.y((kl+round(pg/2)):kr);
zr=kante.z((kl+round(pg/2)):kr);
%plot(yr,zr,'ob');

[bs.strline_left]=polyfit(yl,zl,1);
[bs.strline_right]=polyfit(yr,zr,1);

%determine goodness of fit
[zl_est] = polyval(bs.strline_left,yl);
[zr_est] = polyval(bs.strline_right,yr);

diffl=sum(abs(zl_est-zl));
diffr=sum(abs(zr_est-zr));

%define straight line by two points
y_left=[-10 10];
z_left(1)=bs.strline_left(1)*y_left(1)+bs.strline_left(2);%LS line equation
z_left(2)=bs.strline_left(1)*y_left(2)+bs.strline_left(2);

y_right=[-10 10];
z_right(1)=bs.strline_right(1)*y_right(1)+bs.strline_right(2);%LS line equation
z_right(2)=bs.strline_right(1)*y_right(2)+bs.strline_right(2);

%determine which side is better estimated by the straight line
if diffl<diffr


    fase_angle=atan(bs.strline_left(1));
    fase_angle=round(fase_angle*1e4)/1e4;
    %determine point of intersection btw. chamfer str.line and flank and face
    %with face: y_1!=y_2 --> x=(c_2-c_1)/(m_1/m_2)
    y_inter_face=(bs.strline_left(2)-kante.strline_left(2))/(-bs.strline_left(1)+kante.strline_left(1));
    z_inter_face=bs.strline_left(1)*y_inter_face+bs.strline_left(2);
    %with flank
    y_inter_flank=(bs.strline_left(2)-kante.strline_right(2))/(-bs.strline_left(1)+kante.strline_right(1));
    z_inter_flank=bs.strline_left(1)*y_inter_flank+bs.strline_left(2);
    %fase length is length of straight line between both points of
    %intersection.
    fase_length=norm([y_inter_face z_inter_face]-[y_inter_flank z_inter_flank]);
    %plot([y_inter_face y_inter_flank],[z_inter_face z_inter_flank],'k');

    %R^2 goodness of fit if z-deviation is calculated

    SSE=sum((zl-zl_est).^2);
    SST=sum((zl-mean(zl)).^2);

    R_squared_f=1-SSE/SST;
else


    fase_angle=atan(bs.strline_right(1));
    fase_angle=round(fase_angle*1e4)/1e4;

    %determine point of intersection btw. chamfer str.line and flank and face
    %with face: y_1!=y_2 --> x=(c_2-c_1)/(m_1/m_2)
    y_inter_face=(bs.strline_right(2)-kante.strline_left(2))/(-bs.strline_right(1)+kante.strline_left(1));
    z_inter_face=bs.strline_right(1)*y_inter_face+bs.strline_right(2);
    %with flank
    y_inter_flank=(bs.strline_right(2)-kante.strline_right(2))/(-bs.strline_right(1)+kante.strline_right(1));
    z_inter_flank=bs.strline_right(1)*y_inter_flank+bs.strline_right(2);
    %fase length is length of straight line between both points of
    %intersection.
    fase_length=norm([y_inter_face z_inter_face]-[y_inter_flank z_inter_flank]);

    plot([y_inter_face y_inter_flank],[z_inter_face z_inter_flank],'k');
    
    %R^2 goodness of fit if z-deviation is calculated

    SSE=sum((zr-zr_est).^2);
    SST=sum((zr-mean(zr)).^2);

    R_squared_f=1-SSE/SST;

end
