function [params_out]=main(location,kname,params_in,handles)
format long;

err=0;%used to detect error in calculation of delta_r

%READ DATA FROM ALICONA OR TALYSURF------------------------------------------

% if params_in.which==1
%     [h,tab,name]=read_alicona201f(sprintf('%s%s',location,kname),5000);
% else
%     %read data from Talysurf text file
%     [h,tab,name]=read_talysurf(sprintf('%s%s',location,kname),5000);
% end

bdwidth = 5; %width of window border at sides�
topbdwidth = 70; %width of window at top


% DETERMINE CROSS SECTION POINT COORDINATES--------------------------------
disp(sprintf('%s%s',location,kname))
[kante]=getinfolinus(sprintf('%s%s',location,kname));




if params_in.bman==0 %automatic calculation of b-section
    axes(handles.plot_left)
else
    axes(handles.plot_right)
end

hold on
grid on
xlabel('m');
ylabel('m');
axis equal
%SHIFT AND ROTATE KANTE------------------------------------------------------------

for i=1:2 %done twice since measured edge could be skew and therefore in the function 'kantenbereich()' the value
    %abs(kante.zmax-kante.z(k)) is not equivalent to the vertical distance
    %'LS_region'. After a first rotation the value
    %abs(kante.zmax-kante.z(k)) will be a vertical value (distance)

    %shorten cross section's flank and face to 2mm the first time (i==1) and then to 1mm the second time (i==2) (if flank or face is shorter than 1.5mm, then nothing is done)
    [kante]=trim_edge(kante,3e-3/i);
    LS_toplimit=80e-6;%150m� vertically down from maximum point
    [kante]=kantenbereich(kante,LS_toplimit);
    % LS lines for flank and face calculated from bottom most point of
    % measurement up to 'LS_region'
    [kante]=str_line(kante,'b','c',0,0);
    %shift point of straight lines intersection onto (0,0)
    [kante]=shift_any(kante,kante.y_inter,kante.z_inter);
    [kante]=str_line(kante,'b','c',0,0);
    %determine angle for rotation
    alpha=(kante.angle_l+kante.angle_r)/2;
    %rotate
    [kante]=rotate_edge(alpha,kante,1);
    [kante]=str_line(kante,'b','c',0,0);

end
params_out.totalpoints=length(kante.edge);
view=params_in.view;
% FILTER CROSS SECTION DATA------------------------------------------------

if params_in.filter~=0
    [kante,params_out.num_outliers]=outlier_filter(kante,params_in);
else
    params_out.num_outliers=0;
end


if params_in.bman==0
    axes(handles.plot_left)
else
    axes(handles.plot_right)
end

%DETRMINE DELTA_R and b-SECTION--------------------------------------------------------

if params_in.bman==0 %AUTOMATIC b-SECTION
    if max(imag(kante.edge))<kante.z_inter


        %first time (LS lines)___________________________________________
        %choose new limits for LS-straight lines (set by user)
        lowerlimit1=max(imag(kante.edge))+params_in.lowerlimit1;
        upperlimit1=max(imag(kante.edge))+params_in.upperlimit1;
        [kante]=startpositions(kante,upperlimit1,lowerlimit1);%determine indexes of points lying withing the limits

        [kante]=re_alignment(kante);

        %determine delta_r------------------------------------------
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [kante,z_bisect,plc1,plc2]=getdelta_r(kante,0);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %determine b-section-----------------------------------------
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,0);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %second time (LS lines with new regions)______________________________________
        %choose new limits for LS-straight lines (also set by user)
        lowerlimit2=max(imag(kante.edge))+params_in.lowerlimit2;
        if kl>1
            upperlimit2=min(imag(kante.edge(kl-1)),imag(kante.edge(kr+1)));%defined by b-section calculated previously
        else
            upperlimit2=0;
            %msgbox('upperlimit2 cannot be calculated','Warning','warn')
        end
        [kante]=startpositions(kante,upperlimit2,lowerlimit2);%determine point indices for new LS-region

        [kante]=re_alignment(kante);%shift, rotate

        view=params_in.view;
        axis([-1/view*1e-4 1/view*1e-4 -1.9/view*1e-4 0.1/view*1e-4]);
        [kante]=str_line(kante,'b','c',1,1);%calculate LS-lines and display them
        plot(kante.edge,'.k');

        %new delta_r------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [kante,z_bisect,plc1,plc2]=getdelta_r(kante,1);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %new b-section----------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,1);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%


    else
        err=1;%detect error when delta_r cannot be calculated
        legend('delta_r is not defined for this profile!');
    end
else
    %MANUAL b-SECTION (limits set by user)
    lowerlimit1=max(imag(kante.edge))+params_in.lowerlimit1;
    upperlimit1=max(imag(kante.edge))+params_in.upperlimit1;
    [kante]=startpositions(kante,upperlimit1,lowerlimit1);%determine indexes for points lying withing the limits

    [kante]=re_alignment(kante);

    view=params_in.view;
    axis([-1/view*1e-4 1/view*1e-4 -1.9/view*1e-4 0.1/view*1e-4]);
    [kante]=str_line(kante,'b','c',1,1);
    plot(kante.edge,'.k');

    %determine delta_r------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kante,z_bisect,plc1,plc2]=getdelta_r(kante,1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %determine b-section-----------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%


end

%CALCULATION OF S, SKEWNESS, kurtosis------------------------------

%calculate Skewness and kurtosis
[p_distr,kante]=convert_to_probability(kante,location);%converts geometric "distribution" (point coordinate information) into probability distribution

%calculate S
[kante]=calc_S(kante,kl,kr,plc1,plc2,z_bisect);

if err==0 %(there are points in [-b,+b])

    %determine dl_schn and dr_schn-------------------------------------

    %1.dl and dr are determined at the outer most points of the b-section
    sl=kante.bl;
    sr=kante.br;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [dl_b,dr_b]=getdldr(sl,sr,z_bisect,kl,kr,kante);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %2.dl and dr are determined at points of intersection between horizontal (green) line and clearance and
    %rake straight lines
    sl=(z_bisect-kante.strline_left(2))/kante.strline_left(1);
    sr=(z_bisect-kante.strline_right(2))/kante.strline_right(1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [dl_schn,dr_schn]=getdldr(sl,sr,z_bisect,kl,kr,kante);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %Fit (black) circle into edge Geometry (into green region
    %[-b;+b])------------------------------------------------------------
    
    [xc, yc, R, a,R_squared]=circfit([real(kante.edge(kl:kr))'],[imag(kante.edge(kl:kr))']);
    th=[0:0.1:2*pi+0.1];
    xn=xc+R*sin(th);
    yn=yc+R*cos(th);


    if R_squared<=0.90 || imag(R_squared)~=0 %if circle doesnt fit geometry sufficiently assume that there is a chamfer

        [params_out.fase_angle,params_out.fase_length,params_out.r_squared_f]=fasecalc(kl,kr,pg,kante);
        plot(xn,yn,'-.k');%plot fitted black circle
    else
        params_out.fase_angle=0;
        params_out.fase_length=0;
        params_out.r_squared_f=0;
        plot(xn,yn,'k');%plot fitted black circle
    end


    %ideal fitted circle (red) with radius r_n between LS-lines for face
    %and flank
    r_n=kante.delta_r./((1/sin(pi/180*kante.openingangle/2))-1);%radius of ideal circle fitted between face and flank
    xic=0;
    yic=z_bisect-r_n;
    xi=xic+r_n*sin(th);
    yi=yic+r_n*cos(th);
    plot(xi,yi,'r');

    %determine angular range of points b of fitted circle
    line1=polyfit([xc real(kante.edge(kl))],[yc imag(kante.edge(kl))],1);
    x1=[xc real(kante.edge(kl))];
    y1=line1(1).*x1+line1(2);
    plot(x1,y1,'g');
    line2=polyfit([xc real(kante.edge(kr))],[yc imag(kante.edge(kr))],1);
    x2=[xc real(kante.edge(kr))];
    y2=line2(1).*x2+line2(2);
    plot(x2,y2,'g');

    arr=atan(line1(1))-atan(line2(1));
    arr=180-abs(arr/pi*180);%angular range in degrees

end

%DETERMINE UNCERTAINTY OF r (using values given by Lotze)------------------

u_p=params_in.u_p;%uncertainty of measurement for one point

%%%%%%%%%%%%%%%%%%%%%%%
if pg>100, pg_lotze=100;else pg_lotze=pg;end
%if arr>180,arr=180;end
%%%%%%%%%%%%%%%%%%%%%%%

if params_in.ZI(pg_lotze-4,round(arr)-29)*u_p>u_p
    ur=params_in.ZI(pg_lotze-4,round(arr)-29)*u_p;
else
    ur=params_in.u_p;
end;

%{
OLD METHOD USED TO DETERMINE UNCERTAINTY

%u_r by Lotze depending on number of points available for inscription (pg) and
%angular range of points (arr)
%u_r matrix:
%row 1: 5 points
%row 2: 10 points
%row 3: 25 points
%row 4: 100 points
%row 5: 1000 points
%column 1: 30degrees
%column 2: 45degrees
%column 3: 60degrees
%column 4: 90degrees
%column 5: 120degrees
%column 6: 180degrees
%
% arr=round(arr);
u_r=[149 67 36 15 8 3.4; 81 34 19 8 4 1.9; 49 21 12 5 2.7 1.1; 24 11 6 2.5 1.3 0.5; 8 4 2 0.8 0.5 0.2];
n_points=[5 5 5 5 5 5 5 5 10 10 10 10 10 10 10 10 25 25 25 25 25 25 25 25 100 100 100 100 100 100 100 100 1000 1000 1000 1000 1000 1000 1000 1000];
a_range=[30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360];
u_p=params_in.u_p;%uncertainty of measurement for one point

if pg<=7,b_lotze=5;end
if pg>7&&pg<=15,b_lotze=10;end
if pg>15&&pg<=50,b_lotze=25;end
if pg>50&&pg<=400,b_lotze =100;end
if pg>400,b_lotze=1000;end


if arr<=30,arr_lotze=30;end
if arr>30&&arr<=50,arr_lotze=45;end
if arr>50&&arr<=70,arr_lotze=60;end
if arr>70&&arr<=100,arr_lotze=90;end
if arr>100&&arr<=140,arr_lotze=120;end
if arr>140,arr_lotze=180;end

switch b_lotze
case {5}
b_l=1;
case {10}
b_l=2;
case {25}
b_l=3;
case {100}
b_l=4;
case {1000}
b_l=5;
otherwise
disp('b is invalid')
end

switch arr_lotze
case {30}
arr_l=1;
case {45}
arr_l=2;
case {60}
arr_l=3;
case {90}
arr_l=4;
case {120}
arr_l=5;
case {180}
arr_l=6;
otherwise
disp('arr is invalid')
end

if u_r(b_l,arr_l)*u_p>u_p
ur=u_r(b_l,arr_l)*u_p;
else
ur=params_in.u_p;
end;
%}

%"export" parameters sothat they can be displayed in the GUI

params_out.r_n=r_n;%radius of ideal circle
params_out.r=R;%radius of fitted circle
params_out.xc=xc;%position of circle centre point
params_out.b=pg;%number of points in b-region
params_out.kante=kante;
params_out.zmax=max(imag(kante.edge));
params_out.ur=ur;%uncertainty of fitted radius
params_out.arr=arr;%angular range of points in b
params_out.dl_schn=dl_schn;
params_out.dr_schn=dr_schn;
params_out.dl_b=dl_b;
params_out.dr_b=dr_b;
params_out.r_squared=R_squared;
