function [kante]=get_y_z(kante);

% convert complex edge into real coordinates y and z
kante.z=[0];
kante.y=[0];

for i=1:length(kante.edge)
    kante.z(i)=imag(kante.edge(i));
    kante.y(i)=real(kante.edge(i));
end
[kante]=det_max(kante);