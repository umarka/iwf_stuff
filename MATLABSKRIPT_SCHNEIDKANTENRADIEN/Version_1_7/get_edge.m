function [kante]=get_edge(kante)
%convert real profile point coordinates into complex profile coordinates
kante.edge=[0];

for i=1:length(kante.z)
    kante.edge(i)=kante.y(i)+(kante.z(i)*1i);
end




