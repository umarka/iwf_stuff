function [vec]=remove_doubllines(vec)

n=size(vec);
sz=n(1);
k=1;
while k<sz
    search_ln=vec(k,:);
    for i=k+1:sz
        while vec(i,:)==search_ln
            vec(i,:)=[];
            n=size(vec);
            sz=n(1);
            if i>=sz,break,end
        end
    if i>=sz,break,end
    end
    k=k+1;
end


