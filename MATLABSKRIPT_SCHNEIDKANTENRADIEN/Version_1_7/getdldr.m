function [dl,dr]=getdldr(sl,sr,z_bisect,kl,kr,kante);

% the value of the ratio between dl and dr can be used to determine the asymmetry
% of the measured edge. dl and dr are distances between z_bisect and
% specific points on the measured edge around the tool tip (the location of the specific points
% at which dl nd dr are calculated is determined by the user, using variable params_in.d_type)

%calculate dl--------------------------
k=kl;

while real(kante.edge(k))<sl
    k=k+1;
end

%determine dl (vertical distance between z_bisect and point k on kante.edge, which lies closest to sl)
if imag(kante.edge(k))<z_bisect, signn=1;,else,signn=-1;end;

%y- and z-coordinates of points in k and k-1
py1=real(kante.edge(k));
pz1=imag(kante.edge(k));
py2=real(kante.edge(k-1));
pz2=imag(kante.edge(k-1));

%straight line through points at k and k-1 (linear interpolation)
line_params=polyfit([py1 py2],[pz1 pz2],1);
z_point=line_params(1)*sl+line_params(2);
dl=signn*abs(z_bisect-z_point);


%calculate dr--------------------------
k=kr;

while real(kante.edge(k))>sr
    k=k-1;
end

%determine dr (vertical distance between z_bisect and point k on
%kante.edge, which lies closest to sr)
if imag(kante.edge(k))<z_bisect, signn=1;,else,signn=-1;end;
%y- and z-coordinates of points in k and k-1
py1=real(kante.edge(k));
pz1=imag(kante.edge(k));
py2=real(kante.edge(k+1));
pz2=imag(kante.edge(k+1));

%straight line through points at k and k-1 (linear interpolation)
line_params=polyfit([py1 py2],[pz1 pz2],1);
z_point=line_params(1)*sr+line_params(2);
dr=signn*abs(z_bisect-z_point);