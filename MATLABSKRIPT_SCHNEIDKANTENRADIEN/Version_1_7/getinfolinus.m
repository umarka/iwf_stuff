function [kante]=getinfolinus(Dateiname)

%read data from tab into kante.y and kante.z


a=length(Dateiname);
while Dateiname(a)~='\'
    a=a-1;
end
name=Dateiname(a+1:end);

addpath('D:\Server\linmeier\Projekt_KSS_Blaser\AP2_Referenzmessungen\Auswertesoftware\Matlab_Auswertesoftware')

tab=ProfilSchnittSchneidkante_NLX(Dateiname);


kante.z=tab(:,2)*10^(-3);
kante.y=tab(:,1)*10^(-3);


[kante]=det_max(kante);% determine coordinates and point number of maximum
[kante]=get_edge(kante);% determine cross section in complex numbers
kante.name=name;

