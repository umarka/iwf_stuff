function [kante_shift]= shift_to_zero(kante);

%shift a profile onto point (0,0) 
kante_shift=kante;

for i=1:length(kante_shift.z)

    kante_shift.z(i)=kante_shift.z(i)-kante_shift.zmax;
    kante_shift.y(i)=kante_shift.y(i)-kante_shift.ymax;

end

[kante_shift]=get_edge(kante_shift);
[kante_shift]=det_max(kante_shift);