function [kante]=kantenbereich(kante,LS_toplimit);

%determine kantenbereich = number of points on the cutting edge to be omitted when calculating LS-lines for flank and face
kante.LSendl=1;
k=1;
while abs(kante.zmax-kante.z(k))>LS_toplimit  
    k=k+1;
end
kante.LSendl=k;%point at which Kantenbereich starts

kante.LSstartr=length(kante.z);
k=length(kante.z);
while abs(kante.zmax-kante.z(k))>LS_toplimit 
    k=k-1;
end
kante.LSstartr=k;%point at which Kantenbereich starts

kante.LSendr=length(kante.z);
kante.LSstartl=1;
%
% kante.LSendl = 30
% kante.LSstartl = 5
% kante.LSendr = 50
% kante.LSstartr = 80