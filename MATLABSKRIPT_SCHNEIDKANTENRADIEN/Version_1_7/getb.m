function  [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,plotb);


if params_in.bman==0 %automatic limits for b (by calculation with formula)
    %determine range for calculating symmetry
    b=(kante.delta_r*cos(kante.openingangle*pi/180/2))/((1/sin(kante.openingangle*pi/180/2))-1);

    %%%%%%%%%%%%%%%%%
    b=1*b;%can be changed to enlarge or decrease b-section
    %%%%%%%%%%%%%%%%%

    if plotb,plot([-b b],[z_bisect z_bisect],'g');end

    %determine which points lie in the determined range [+b,-b]
    kl=1;

    while real(kante.edge(kl))<-b
        kl=kl+1;
    end
    kr=kl;
    while real(kante.edge(kr))<b
        kr=kr+1;
    end
    
    if kr>kl,kr=kr-1;end

    if kr-kl>1 %there is at least one point in region [-b,b]
        kante.symm=imag(kante.edge(kl:kr));

        %EXTENDED b-SECTION FOR BETTER CALCULATION OF SYMMETRY---------
            
            
            %FIRST STEP:even out b-section in
            %z-direction (lowest points on left and right side have comparable z-coordinate)
            symm_min=min(kante.symm);
            if symm_min==kante.symm(end)%right end of kante.symm is "lower" than left side
                p=kl;
                while imag(kante.edge(p))>symm_min
                    p=p-1;
                end
                kl=p+1;
            else
                p=kr;
                while imag(kante.edge(p))>symm_min
                    p=p+1;
                end
                kr=p-1;
            end

            %SECOND STEP:furthest most points (in b-section) to the left and
            %to the right of the angle bisector must have comparable distance to
            %the angle bisector.

            if abs(real(kante.edge(kl)))<abs(real(kante.edge(kr)))
                while abs(real(kante.edge(kl)))<abs(real(kante.edge(kr)))
                    kl=kl-1;
                end
                kl=kl+1;
            else
                while abs(real(kante.edge(kl)))>abs(real(kante.edge(kr)))
                    kr=kr+1;
                end
                kr=kr-1;
            end

        kante.bl=real(kante.edge(kl));
        kante.br=real(kante.edge(kr));
        kante.symm=imag(kante.edge(kl:kr));
        
        if plotb==1
            plot(kante.edge(kl:kr),'.g');
            plot([real(kante.edge(kl)) real(kante.edge(kr))],[ z_bisect z_bisect],'g');
        end
        pg=kr-kl;%number of points in b-section

    else
        pg=0;
        %msgbox('There is only 1 or no point in region b!','Warning','warn')
    end
else
    %manual selection of b-section by clicking on the plot window
    %params_in.bman==1
    kl=1;
    while real(kante.edge(kl))<params_in.bl
        kl=kl+1;
    end
    kr=kl;
    while real(kante.edge(kr))<params_in.br
        kr=kr+1;
    end
    if kr>kl,kr=kr-1;end
    
    kante.bl=real(kante.edge(kl));
    kante.br=real(kante.edge(kr));
    kante.symm=imag(kante.edge(kl:kr));
    plot(kante.edge(kl:kr),'.g');
    plot([real(kante.edge(kl)) real(kante.edge(kr))],[ z_bisect z_bisect],'g');
    pg=kr-kl;%number of points in b-section

end