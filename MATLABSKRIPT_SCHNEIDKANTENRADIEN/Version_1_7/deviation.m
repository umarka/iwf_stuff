%determines the smallest deviations between curve1 and curve2 in every
%point (and its location)

%one point is chosen on curve1 and the closest point on curve 2 is searched
%for. For each point on curve 1 the corresponding, closest point on curve 2
%is determined, and the distance between the points.
%for one cycle of the search loop the point on curve1 is kept constant but the
%internal loop runs through "varr" points on curve2 so that the closest point is found.
%This saves much calculation time since not all points on curve2 have to be
%run through to find the closest point to curve 1.

function [mindev,mindevpos]=deviation(curve1,curve2);

%determine varr (search region on curve2)
if abs(curve1(1)-curve1(2))<abs(curve2(1)-curve2(2)) %determine for which curve the points distances are smaller
    varr=2;
else
    varr=round(abs(curve1(1)-curve1(2))/abs(curve2(1)-curve2(2)))*5;
    if varr==0,varr=2;,end;
end


a=length(curve1);
b=length(curve2);
%the cross section curve is divided into a RHS and LHS
if real(curve1(end))>0;RHS=true;else;RHS=false;end
if sign(real(curve1(1)))==sign(real(curve1(end)));whole_curve=false;else;whole_curve=true;end


%whole_curve is true if the entire cross section is being used for
%calculating deviations. whole_curve is false when only selescted parts of
%the curve (e.g. clearance without kantenbereich) are looked at

%determine start points for curve1 and curve2
%since curve1 and curve2 are never exactly the same length it is important
%to determine which curve is shorter and which two points must be chosen
%to start the comparisson. if the first point of each curve is chosen as
%the starting point and these points do not lie opposite of eachother, then
%the calculated deviation is false

if whole_curve==false
    if RHS==true
        if abs(curve1(1))<abs(curve2(1))
            for i=1:50
                dist(i)=abs(curve2(1)-curve1(i));
            end
            min_dist=min(dist);
            pos=find(dist==min_dist);
            start=1;
            s=pos;
        else
            for i=1:50
                dist(i)=abs(curve1(1)-curve2(i));
            end
            min_dist=min(dist);
            pos=find(dist==min_dist);
            s=1;
            start=pos;
        end
    else
        if abs(curve1(1))>abs(curve2(1))
            for i=1:50
                dist(i)=abs(curve2(1)-curve1(i));
            end
            min_dist=min(dist);
            pos=find(dist==min_dist);
            start=1;
            s=pos;
        else
            for i=1:50
                dist(i)=abs(curve1(1)-curve2(i));
            end
            min_dist=min(dist);
            pos=find(dist==min_dist);
            s=1;
            start=pos;
        end
    end
else
    if abs(curve1(1))>abs(curve2(1))
        for i=1:50
            dist(i)=abs(curve2(1)-curve1(i));
        end
        min_dist=min(dist);
        pos=find(dist==min_dist);
        start=1;
        s=pos;
    else
        for i=1:50
            dist(i)=abs(curve1(1)-curve2(i));
        end
        min_dist=min(dist);
        pos=find(dist==min_dist);
        s=1;
        start=pos;
    end
end

%determine end point for curve1
if whole_curve==false
    if RHS==true
        if abs(curve1(end))<abs(curve2(end))
            e=a;
        else
            t=length(curve1);

            while abs(curve1(t))>abs(curve2(end))
                t=t-1;
            end
            e=t;
        end
    else
        if abs(curve1(end))>abs(curve2(end))
            e=a;
        else
            t=length(curve1);

            while abs(curve1(t))<abs(curve2(end))
                t=t-1;
            end
            e=t;
        end
    end
else
    if abs(curve1(end))<abs(curve2(end))
        e=a;
    else
        t=length(curve1);

        while abs(curve1(t))>abs(curve2(end))
            t=t-1;
        end
        e=t;
    end
end


%determine shortest distances between curve1 and curve2
%s: start value for search
%e: end value for search
mindev=zeros(1,e-s);
mindevpos=zeros(1,e-s);

for i=s:e %s,e: start and end points on curve1

    if (start+varr)<=b,ende=(start+varr);,else,ende=b;,end;
    if (start-varr)>=1, beginn=start-varr;,else,beginn=1;,end;

    for j=beginn:ende %start and end points on curve 2
        deviation_temp.value(j-beginn+1)=abs(curve1(i)-curve2(j));%distance between points
        deviation_temp.pos(j-beginn+1)=j;
    end
    mindev(i)=min(abs(deviation_temp.value));%minimum of all deviations, approximates the orthogonal distance between the two cross sections
    dev_pos=find(deviation_temp.value==mindev(i));
    mindevpos(i)=deviation_temp.pos(dev_pos);%position of smallest deviation
    start=mindevpos(i);%new starting position on curve2
%           v=curve2(mindevpos(i));
%           w=curve1(i);
%           plot([v w],'c');
%           plot(v,'+r');
%           plot(w,'+g');
    deviation_temp.value=0;
    deviation_temp.pos=0;
    start=start+1;

end
