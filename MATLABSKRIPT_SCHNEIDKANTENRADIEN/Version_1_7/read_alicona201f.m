function [Header,Tab,filename] = read_alicona201f(Datei,n);
% Einlesen einer mit Alicona geschriebenen Datei

Datei
a=length(Datei);
while Datei(a)~='\'
    a=a-1;
end
filename=Datei(a+1:end);
%
fid = fopen(Datei, 'r', 'n');

%======================================
% Einlesen der Kopfzeilen-Information
%======================================
% Alicona kein header

tmp = fgetl(fid);

for i=1:length(tmp)
    if (tmp(i)=='x')
        if (tmp(i+2)~='�')&(tmp(i+2)~='�')
            xein=tmp(i+2:i+3)
        else
            xein=tmp(i+3:i+4)
        end
    else
        if (tmp(i)=='y')
            if (tmp(i+2)~='�')&(tmp(i+2)~='�')
                yein=tmp(i+2:i+3)
            else
                yein=tmp(i+3:i+4)
            end
        else
            if (tmp(i)=='l')
                if (tmp(i+2)~='�')&(tmp(i+2)~='�')
                    lein=tmp(i+2:i+3)
                else
                    lein=tmp(i+3:i+4)
                end
            else

                if (tmp(i)=='z')
                    if (tmp(i+2)~='�')&(tmp(i+2)~='�')
                        if (tmp(i+3)=='m')
                            zein=tmp(i+2:i+3)
                        else
                            zein=tmp('�m')
                        end
                    else
                        zein=tmp(i+3:i+4)
                    end
                end
            end
        end
    end
    if tmp(i)=='z', break; end
end


%{
% Einheiten auslesen
einheiten=sscanf(tmp,'%s\t%s\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s');
% 'No.\t%s\t\t%s\t\tl[mm]\t\t%s\t\tpan[�]\t\ttilt[�]')
%}



Header=[xein;yein;zein;lein];

Tab=[];
j=1;
while ~feof(fid) && j<1e6
    j=j+1;
    tmp=fgetl(fid);
    if ~isstr(tmp), break, end
    tmpdata=sscanf(tmp,'%f');
    if isempty(Tab),
        Tab=zeros(n,length(tmpdata));%Tab enth�lt die daten des textfiles
    end
    Tab(j,1:length(tmpdata))=tmpdata';
    if j/1000==floor(j/1000),
        %      j
    end
end;
fclose(fid);
Tab=Tab(1:j,:); %truncation
