
P=polyfit(real(kante.edge(400:470)),imag(kante.edge(400:470)),34);
figure(3)
cla;
hold on, grid on
x=[real(kante.edge(400)):1e-7:real(kante.edge(470))];
plot(real(kante.edge),imag(kante.edge),'.k')
f=polyval(P,x);
plot(x,f,'.r')