function [params_out]=main_conti_profiles(kante)
format long;

ZeigeDiagramme = false; % Diagramme zu Schneidkantenradienbestimmung anzeigen?

filterq = 1;
err=0;%used to detect error in calculation of delta_r
params_in.bman=0;
params_in.lowerlimit1=200e-6;
params_in.upperlimit1=30e-6;
params_in.lowerlimit2=100e-6;
params_in.filter=2;
params_in.polydegree=5;
params_in.u_p=3e-6;

%READ DATA FROM ALICONA OR TALYSURF------------------------------------------

% if params_in.which==1
%     [h,tab,name]=read_alicona201f(sprintf('%s%s',location,kname),5000);
% else
%     %read data from Talysurf text file
%     [h,tab,name]=read_talysurf(sprintf('%s%s',location,kname),5000);
% end

bdwidth = 5; %width of window border at sides�
topbdwidth = 70; %width of window at top


% DETERMINE CROSS SECTION POINT COORDINATES--------------------------------



%SHIFT AND ROTATE KANTE------------------------------------------------------------

for i=1:2 %done twice since measured edge could be skew and therefore in the function 'kantenbereich()' the value
    %abs(kante.zmax-kante.z(k)) is not equivalent to the vertical distance
    %'LS_region'. After a first rotation the value
    %abs(kante.zmax-kante.z(k)) will be a vertical value (distance)
    %shorten cross section's flank and face to 2mm the first time (i==1) and then to 1mm the second time (i==2) (if flank or face is shorter than 1.5mm, then nothing is done)
    [kante]=trim_edge(kante,3e-3/i);
    %LS_toplimit=30e-6;%150m� vertically down from maximum point
    LS_toplimit=10e-6;% Modifikation HK
    %
    %length(kante.z)      % DEBUG HK, Do, 06.06.2019
    %plot(kante.z)          % DEBUG HK, Do, 06.06.2019
    %
    [kante]=kantenbereich(kante,LS_toplimit);
    % LS lines for flank and face calculated from bottom most point of
    % measurement up to 'LS_region'
    [kante]=str_line(kante,'b','c',0,0);
    %[kante]=str_line(kante,'b','c',1,1);    % Modifikation HK, Do, 06.06.2019
    %shift point of straight lines intersection onto (0,0)
    [kante]=shift_any(kante,kante.y_inter,kante.z_inter);
    [kante]=str_line(kante,'b','c',0,0);
    %determine angle for rotation
    alpha=(kante.angle_l+kante.angle_r)/2;
    %rotate
    [kante]=rotate_edge(alpha,kante,1);
    [kante]=str_line(kante,'b','c',0,0);

end
params_out.totalpoints=length(kante.edge);
% FILTER CROSS SECTION DATA------------------------------------------------



%DETRMINE DELTA_R and b-SECTION--------------------------------------------------------

if max(imag(kante.edge))<kante.z_inter


    %first time (LS lines)___________________________________________
    %choose new limits for LS-straight lines (set by user)
    lowerlimit1=max(imag(kante.edge))+params_in.lowerlimit1;
    upperlimit1=max(imag(kante.edge))+params_in.upperlimit1;
    [kante]=startpositions(kante,upperlimit1,lowerlimit1);%determine indexes of points lying withing the limits

    [kante]=re_alignment(kante);

    %determine delta_r------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kante,z_bisect,plc1,plc2]=getdelta_r(kante,0);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %determine b-section-----------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,0);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %second time (LS lines with new regions)______________________________________
    %choose new limits for LS-straight lines (also set by user)
    lowerlimit2=max(imag(kante.edge))+params_in.lowerlimit2;
    if kl>1
        upperlimit2=min(imag(kante.edge(kl-1)),imag(kante.edge(kr+1)));%defined by b-section calculated previously
    else
        upperlimit2=0;
        %msgbox('upperlimit2 cannot be calculated','Warning','warn')
    end
    [kante]=startpositions(kante,upperlimit2,lowerlimit2);%determine point indices for new LS-region

    [kante]=re_alignment(kante);%shift, rotate


    %new delta_r------------------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kante,z_bisect,plc1,plc2]=getdelta_r(kante,0);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %new b-section----------------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [kl,kr,pg,kante]=getb(params_in,kante,z_bisect,0);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%


else
    err=1;%detect error when delta_r cannot be calculated
    legend('delta_r is not defined for this profile!');
end


%CALCULATION OF S, SKEWNESS (Schiefe/Schräge), kurtosis (Wölbung)------------------------------

%calculate Skewness and kurtosis
[p_distr,kante]=convert_to_probability(kante);%converts geometric "distribution" (point coordinate information) into probability distribution

%calculate S
[kante]=calc_S(kante,kl,kr,plc1,plc2,z_bisect);

if err==0 %(there are points in [-b,+b])

    %determine dl_schn and dr_schn-------------------------------------

    %1.dl and dr are determined at the outer most points of the b-section
    sl=kante.bl;
    sr=kante.br;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [dl_b,dr_b]=getdldr(sl,sr,z_bisect,kl,kr,kante);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %2.dl and dr are determined at points of intersection between horizontal (green) line and clearance and
    %rake straight lines
    sl=(z_bisect-kante.strline_left(2))/kante.strline_left(1);
    sr=(z_bisect-kante.strline_right(2))/kante.strline_right(1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [dl_schn,dr_schn]=getdldr(sl,sr,z_bisect,kl,kr,kante);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %Fit (black) circle into edge Geometry (into green region
    %[-b;+b])------------------------------------------------------------
    
    [xc, yc, R, a,R_squared]=circfit([real(kante.edge(kl:kr))'],[imag(kante.edge(kl:kr))']);
    th=[0:0.1:2*pi+0.1];
    xn=xc+R*sin(th);
    yn=yc+R*cos(th);

    if ZeigeDiagramme
        figure
        hold on;
        plot(real(kante.edge),imag(kante.edge));
        plot(real(kante.edge(kl:kr)),imag(kante.edge(kl:kr)),'r');
        viscircles([xc,yc],R)
        hold off;
    end

    if R_squared<=0.90 || imag(R_squared)~=0 %if circle doesnt fit geometry sufficiently assume that there is a chamfer

        [params_out.fase_angle,params_out.fase_length,params_out.r_squared_f]=fasecalc(kl,kr,pg,kante);
    else
        params_out.fase_angle=0;
        params_out.fase_length=0;
        params_out.r_squared_f=0;
    end


    %ideal fitted circle (red) with radius r_n between LS-lines for face
    %and flank
    r_n=kante.delta_r./((1/sin(pi/180*kante.openingangle/2))-1);%radius of ideal circle fitted between face and flank
    xic=0;
    yic=z_bisect-r_n;
    xi=xic+r_n*sin(th);
    yi=yic+r_n*cos(th);

    %determine angular range of points b of fitted circle
    line1=polyfit([xc real(kante.edge(kl))],[yc imag(kante.edge(kl))],1);
    x1=[xc real(kante.edge(kl))];
    y1=line1(1).*x1+line1(2);
    line2=polyfit([xc real(kante.edge(kr))],[yc imag(kante.edge(kr))],1);
    x2=[xc real(kante.edge(kr))];
    y2=line2(1).*x2+line2(2);

    arr=atan(line1(1))-atan(line2(1));
    arr=180-abs(arr/pi*180);%angular range in degrees

end

%"export" parameters sothat they can be displayed in the GUI

params_out.r_n=r_n;%radius of ideal circle
params_out.r=R;%radius of fitted circle
params_out.xc=xc;%position of circle centre point
params_out.yc=yc;%position of circle centre point
params_out.b=pg;%number of points in b-region
params_out.kante=kante;
params_out.zmax=max(imag(kante.edge));
params_out.arr=arr;%angular range of points in b
params_out.dl_schn=dl_schn;
params_out.dr_schn=dr_schn;
params_out.dl_b=dl_b;
params_out.dr_b=dr_b;
params_out.r_squared=R_squared;
