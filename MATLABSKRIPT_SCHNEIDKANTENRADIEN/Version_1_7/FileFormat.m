function [pp]=FileFormat(filename)

str=filename;
for parameter=1:7
    index=1;
    while (str(index)~=' ')
        index=index+1;
    end
    teil1=str(1:index);
    teil2=str(index+1:length(str));
    switch parameter
        case 1 
            p1=sscanf(teil1,'K%f')+0;
        case 2
            p2=sscanf(teil1,'%f')+0;
        case 3
            p3=sscanf(teil1,'%s')+0;
        case 4
            p4=sscanf(teil1,'P%f')+0;
        case 5
            p5=sscanf(teil1,'T%f')+0;
        case 6
            p6=sscanf(teil1,'D%f')+0;
        case 7
            p7=sscanf(teil1,'W%f')+0;
    end
    str=teil2;
end
    p8=sscanf(str,'N%f')+0;
    
    pp=[p1 p2 p3 p4 p5 p6 p7 p8];