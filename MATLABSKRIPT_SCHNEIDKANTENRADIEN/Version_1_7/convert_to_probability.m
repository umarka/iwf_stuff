% convert coordinates to probability distribution
function [p_distr,kante]=convert_to_probability(kante)
geom=kante.symm;
all=0;
min_val=min(geom);

if min_val<0
    for i=1:length(geom)
        geom(i)=geom(i)+abs(min_val);
    end
end

for i=1:length(geom)
    if geom(i)~=0
        if max(geom)>1
            if max(geom)>1000
                factor=1;
            else
                factor=100;
            end
        else
            factor=1000;
        end

        max_i=round(factor*geom(i)/max(geom));%how many single values to add to vector: # occurances in 'vec'
        vec=(ones(1,max_i)*i);
        all=[all vec];
    end
end
p_distr=all;
% figure(3)
% hold on
% hist(p_distr,length(geom));
% h = findobj(gca,'Type','patch');
% set(h,'FaceColor','k','EdgeColor','w');
% %AXIS([-1 1 -1 1]);
kante.skewness=skewness(p_distr);
kante.kurtosis=kurtosis(p_distr);
%legend(sprintf('%s%s%s%s','skewness: ',num2str(kante.skewness),'  kurtosis: ',num2str(kante.kurtosis)));
%picname=sprintf('%s%s%s%s',location,kante.name,'histogram','.bmp'); 
%print('-dbitmap',picname);
% hold off;

end