function [dist]=distances(kante,startt,endd)
% calculates the distance each point in kante.edge has to the LS-line
% fitted throught these points

if startt==1
m=kante.strline_left(1);%gradient of left side (LS-line)
c=kante.strline_left(2);%z-intercept of left side
else
m=kante.strline_right(1);%gradient of right side (LS-line)
c=kante.strline_right(2);%z-intercept of right side
end

    
for i=startt:endd
   
    op=[real(kante.edge(i));imag(kante.edge(i));0];
    a=[0; c;0];
    n_0=1/sqrt(m^2+1^2)*[1;m;0];
    d=(cross(op-a,n_0));
    dist(i-startt+1)=d(3);
    
    
end
% 
% %middle geometry
% %p=polyfit(real(kante.edge(kante.zmax_pos-howmuch),imag(kante.edge(kante.zmax_pos+howmuch)),10);
% dist(kante.zmax_pos-howmuch:kante.zmax_pos+howmuch)=1e-8;
% 
% %right
% for i=kante.zmax_pos+howmuch:length(kante.edge)
%    
%     op=[real(kante.edge(i));imag(kante.edge(i));0];
%     a=[0; kante.strline_right(2);0];
%     n_0=1/sqrt(kante.strline_right(1)^2+1^2)*[1;kante.strline_right(1);0];
%     d=(cross(op-a,n_0));
%     dist(i)=d(3);
%     
%     
% end
% 
% 
%{

%OLD_______________________________________________________________________

m_LS=kante.strline_left(1);
c_LS=kante.strline_left(2);
% figure(3)
% hold on
% grid on



m_Pt=-1/m_LS; %gradient of straight line which is perpendicular to y=m_LS*x+c_LS

%left side
for i=1:kante.zmax_pos-30

    eqtn=sprintf('%e%s%e%s%e%s',imag(kante.edge(i)),'=(',m_Pt,')*(',real(kante.edge(i)),')+c');%set up equation y=mx+c
    c_Pt=double(solve(eqtn));%solve for c, when y, m, x are known

    eqtnl1=sprintf('%e%s%e%s',m_Pt,'*x+(',c_Pt,')');%LS-Line equation
    eqtnl2=sprintf('%e%s%e%s',m_LS,'*x+(',c_LS,')');%perpendicular line equation (through point i)

    eqtnx=sprintf('%s%s%s',eqtnl2,'=',eqtnl1);%set up equation m_LS*x+c_LS=m_Pt*x+c_Pt, solve for x
    xs=double(solve(eqtnx));
    eqtny=sprintf('%s%e%s%e%s%e%s','y=(',m_LS,')*(',xs,')+(',c_LS,')');%set up equation y=m_LS*xs+c_LS, solve for y
    ys=double(solve(eqtny));

    xp=real(kante.edge(i));
    yp=imag(kante.edge(i));
% plot(xp,yp,'.k');
% plot(xs,ys,'.b');
dx=xp-xs;
    dy=yp-ys;
    if dy>0
        dist2(i)=sqrt(dx^2+dy^2);
    else
        dist2(i)=-sqrt(dx^2+dy^2);
    end

%     wb=waitbar(i/length(kante.edge),'Please wait');
% close(wb)
end
dist-dist2

m_LS=kante.strline_right(1);
c_LS=kante.strline_right(2);

m_Pt=-1/m_LS;

for i=kante.zmax_pos+30 :length(kante.edge)

    eqtn=sprintf('%e%s%e%s%e%s',imag(kante.edge(i)),'=(',m_Pt,')*(',real(kante.edge(i)),')+c');%set up equation y=mx+c
    c_Pt=double(solve(eqtn));%solve for c, when y, m, x are known

    eqtnl1=sprintf('%e%s%e%s',m_Pt,'*x+(',c_Pt,')');%LS-Line equation
    eqtnl2=sprintf('%e%s%e%s',m_LS,'*x+(',c_LS,')');%perpendicular line equation (through point i)

    eqtnx=sprintf('%s%s%s',eqtnl2,'=',eqtnl1);%set up equation m_LS*x+c_LS=m_Pt*x+c_Pt, solve for x
    xs=double(solve(eqtnx));
    eqtny=sprintf('%s%e%s%e%s%e%s','y=(',m_LS,')*(',xs,')+(',c_LS,')');%set up equation y=m_LS*xs+c_LS, solve for y
    ys=double(solve(eqtny));

    xp=real(kante.edge(i));
    yp=imag(kante.edge(i));
% plot(xp,yp,'.k');
% plot(xs,ys,'.b');
dx=xp-xs;
    dy=yp-ys;
    if dy>0
        dist(i)=sqrt(dx^2+dy^2);
    else
        dist(i)=-sqrt(dx^2+dy^2);
    end
%     wb=waitbar(i/length(kante.edge),'Please wait');
% close(wb)
end

%}