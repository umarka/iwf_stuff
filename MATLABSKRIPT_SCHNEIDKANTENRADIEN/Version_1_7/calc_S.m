function [kante]= calc_S(kante,kl,kr,plc1,plc2,z_bisect)

if sign(real(kante.edge(kl)))~=sign(real(kante.edge(kr)))
    if length(kante.symm)>1
        %left side A_l (based on distances)
        for i=kl:min(plc1,plc2)
            if sign(imag(kante.edge(i)))==sign(z_bisect)
                if imag(kante.edge(i))>z_bisect,signn=1;,else,signn=-1;,end;
                dl(i-kl+1)=signn*(max(abs(imag(kante.edge(i))),abs(z_bisect))-min(abs(imag(kante.edge(i))),abs(z_bisect)));
            else
                dl(i-kl+1)=max(imag(kante.edge(i)),z_bisect)-min(imag(kante.edge(i)),z_bisect);
            end
            %plot(kante.edge(i),'+r');
        end
        A_l=sum(dl);%only distances

        %left side A_l (based on areas)
        %TRAPEZE: calculate AREA of left side by adding all trapeze areas (two measured points create a trapeze with the horizontal line through z_bisect )
        if length(dl)>=2
            for j=1:length(dl)-1
                if sign(dl(j))==sign(dl(j+1))
                    trapez_a=abs(dl(j));
                    trapez_b=abs(dl(j+1));
                    trapez_h=abs(real(kante.edge(j+kl-1))-real(kante.edge(j+kl)));
                    if dl(j)<0, signn=-1;else,signn=1;end
                    areal(j)=signn*(trapez_a+trapez_b)/2*trapez_h;%area instead of distances for S
                else
                    areal(j)=0;%not exact, but seen as sensible
                end
            end
        else
            areal=0.5*abs(real(kante.edge(kr)))*dl(1);
        end

        %right side A_r (based on distances)
        s=i+1;
        for i=s:kr
            if sign(imag(kante.edge(i)))==sign(z_bisect)
                if imag(kante.edge(i))>z_bisect,signn=1;,else,signn=-1;,end;
                dr(i-s+1)=signn*(max(abs(imag(kante.edge(i))),abs(z_bisect))-min(abs(imag(kante.edge(i))),abs(z_bisect)));
            else
                dr(i-s+1)=max(imag(kante.edge(i)),z_bisect)-min(imag(kante.edge(i)),z_bisect);
            end
            %plot(kante.edge(i),'+r');
        end
        A_r=sum(dr);

        %right side A_r (based on areas)
        %TRAPEZE: calculate area of right side by adding all trapeze areas
        %(two measured points create a trapeze with the horizontal line through z_bisect )
        if length(dr)>=2
            for j=1:length(dr)-1
                if sign(dr(j))==sign(dr(j+1))
                    trapez_a=abs(dr(j));
                    trapez_b=abs(dr(j+1));
                    trapez_h=abs(real(kante.edge(j+s-1))-real(kante.edge(j+s)));
                    if dr(j)<0, signn=-1;else,signn=1;end
                    arear(j)=signn*(trapez_a+trapez_b)/2*trapez_h;%area instead of distances for S
                else
                    arear(j)=0;%not exact, but seen as sensible
                end
            end
        else
            arear=0.5*real(kante.edge(kr))*dr(1);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        kante.S=A_l/A_r;%S based on distances
        kante.Snew=sum(areal)/sum(arear);%S based on areas
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
        kante.S=0;
        kante.Snew=0;
        err=1;
        %legend('Too few points in [-b,+b]');
    end
else
    kante.S=0;
    kante.Snew=0;
    %legend('b is only on one side, S cannot be calculated');
end