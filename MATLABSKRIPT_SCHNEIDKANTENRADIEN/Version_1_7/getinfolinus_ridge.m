function [kante]=getinfolinus_ridge(Y,Z,name)

%read data from tab into kante.y and kante.z



kante.z=Z*10^(-3);
kante.y=Y*10^(-3);


[kante]=det_max(kante);% determine coordinates and point number of maximum
[kante]=get_edge(kante);% determine cross section in complex numbers
kante.name=name;

