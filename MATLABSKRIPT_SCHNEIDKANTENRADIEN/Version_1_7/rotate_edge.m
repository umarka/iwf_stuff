function [kante_rot]=rotate_edge(alpha,kante,normal)

kante_rot=kante;
kante_rot.edge=kante_rot.edge.*exp(j*(-alpha));
[kante_rot]=get_y_z(kante_rot);

if normal==true %if small rotation is done, i.e. sides of the profile remain below the maximum point of the profile 
    [kante_rot]=det_max(kante_rot);
end
