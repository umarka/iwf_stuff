function [kante,n_outlier]=outlier_filter(kante,params)

%filters the measured points in three sections:left, middle and right.The
%left and right are filtered by calculating the deviations of the points
%from the LS straight lines. by using the assumption of normal distribution
%of the deviations all points with deviation >filter*sigma are ignored (filtered out)
%whereby "filter" is a value entered by the user.
%the middle section is filtered differently (not explained in detail)
filter=params.filter;

%LEFT--------------------------------------------------------------------
howmuch=50;
a=1;
b=kante.zmax_pos-howmuch;

[dist_l]=distances(kante,a,b);
mu=mean(dist_l);
sigma=std(dist_l);
tmpl=kante.edge(a:b);

k=1;
l=1;
while k<length(tmpl)
    if (dist_l(l)>mu+filter*sigma)|(dist_l(l)<mu-filter*sigma)
        tmpl(k)=[];
        k=k-1;
    end
    k=k+1;
    l=l+1;
end



%MIDDLE--------------------------------------------------------------------
%FIRST FILTER
tmpm=kante.edge(kante.zmax_pos-howmuch+1:kante.zmax_pos+howmuch-1);

%fit polynomial through data at tip and calculate deviations
% plot(kante.edge,'.k')
p=polyfit(real(tmpm),imag(tmpm),params.polydegree);
xpoly=[real(tmpm(1)):5e-8:real(tmpm(end))];
ypoly=polyval(p,xpoly);
polycurve=xpoly+j*ypoly;
% plot(polycurve,'m');
% plot(polycurve,'.m');
[dist_m,dist_m_pos]=deviation(tmpm,polycurve);

mu_m=mean(dist_m);
sigma_m=std(dist_m);

k=1;
l=1;

while k<length(tmpm)
    if l<length(dist_m)
    if (dist_m(l)>mu_m+filter*sigma_m)|(dist_m(l)<mu_m-filter*sigma_m)
        tmpm(k)=[];
        k=k-1;
    end
    end
    k=k+1;
    l=l+1;
end

%SECOND Filter (done twice to imrove results)
%fit polynomial through data at tip and calculate deviations

p=polyfit(real(tmpm),imag(tmpm),params.polydegree);
xpoly=[real(tmpm(1)):1e-8:real(tmpm(end))];
ypoly=polyval(p,xpoly);
polycurve=xpoly+j*ypoly;
%plot(polycurve,'r');
%plot(polycurve,'.r');
[dist_m,dist_m_pos]=deviation(tmpm,polycurve);

mu_m=mean(dist_m);
sigma_m=std(dist_m);

k=1;
l=1;


while k<length(tmpm)
    if l<length(dist_m)
    if (dist_m(l)>mu_m+filter*sigma_m)|(dist_m(l)<mu_m-filter*sigma_m)
        tmpm(k)=[];
        k=k-1;
    end
    end
    k=k+1;
    l=l+1;
end


%{
%distances between subsequent points dist_m
for i=1:length(tmpm)-1
    dist_m(i)=abs(tmpm(i)-tmpm(i+1));
end
mu_d=mean(dist_m);
sigma_d=std(dist_m);

%change of gradient of line between to subsequent points ch_m
for i=1:length(tmpm)-1
    tmp(i)=(imag(tmpm(i+1))-imag(tmpm(i)))/(real(tmpm(i+1))-real(tmpm(i)));
    if i>1
    ch_m(i)=tmp(i)-tmp(i-1);
    end
end

mu_m=mean(ch_m);
sigma_m=std(ch_m);


k=1;
l=1;

while (k<length(tmpm))&(l<length(ch_m))
        if (dist_m(l)>mu_d+3*sigma_d)|(ch_m(l)>mu_m+0.5*sigma_m)|(ch_m(l)<mu_m-0.5*sigma_m)

            tmpm(k)=[];
            k=k-1;
            is_outlier=1;
        end

    k=k+1;
    l=l+1;

end
%}

%RIGHT--------------------------------------------------------------------

a=kante.zmax_pos+howmuch;
b=length(kante.edge);

[dist_r]=distances(kante,a,b);
mu=mean(dist_r);
sigma=std(dist_r);
tmpr=kante.edge(a:b);

k=1;
l=1;
while k<length(tmpr)
    if (dist_r(l)>mu+filter*sigma)|(dist_r(l)<mu-filter*sigma)
        tmpr(k)=[];
        k=k-1;
    end
    k=k+1;
    l=l+1;
end

%create new edge without the filtered points
tmp_edge=[tmpl tmpm tmpr];
n_outlier=length(kante.edge)-length(tmp_edge);% number of points filtered

%%%%%%%%%%%%%%%%%
kante.edge=tmp_edge;
[kante]=get_y_z(kante);
%%%%%%%%%%%%%%%%

