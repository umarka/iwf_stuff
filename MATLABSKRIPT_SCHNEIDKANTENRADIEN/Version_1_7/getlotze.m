function [ZI]=getlotze()


 figure(1)
 cla

 % oly upto x=100 used
% z=[149 67 36 15 8 3.4 2.4 2.4 81 34 19 8 4 1.9 0.9 0.9 49 21 12 5 2.7 1.1 0.5 0.5 24 11 6 2.5 1.3 0.5 0.3 0.3];
% x=[5 5 5 5 5 5 5 5 10 10 10 10 10 10 10 10 25 25 25 25 25 25 25 25 100 100 100 100 100 100 100 100];
% y=[30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360];
% tx = 5:1:100; 
% ty= 30:1:360;


% all available points used
% z=[149 67 36 15 8 3.4 2.4 2.4 81 34 19 8 4 1.9 0.9 0.9 49 21 12 5 2.7 1.1 0.5 0.5 24 11 6 2.5 1.3 0.5 0.3 0.3 8 4 2 0.8 0.5 0.2 0.1 0.1];
% x=[5 5 5 5 5 5 5 5 10 10 10 10 10 10 10 10 25 25 25 25 25 25 25 25 100 100 100 100 100 100 100 100 1000 1000 1000 1000 1000 1000 1000 1000];
% y=[30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360];
% tx = 5:1:1000; 
% ty= 30:1:360;

%only x100 and 1000 used
% x=[100 100 100 100 100 100 100 100 1000 1000 1000 1000 1000 1000 1000 1000];
% y=[30 45 60 90 120 180 270 360 30 45 60 90 120 180 270 360];
% z=[24 11 6 2.5 1.3 0.5 0.3 0.3 8 4 2 0.8 0.5 0.2 0.1 0.1];
% tx = 100:1:1000; 
% ty= 30:1:360;

%artificially create more values HORIZONTALLY (i.e. in the angular range) to add to lotze's matrix for measurement
%uncertainties

    a_range=[1:1:30];
    for i=0:30
        a_range(i+1)=i*5+30;
    end
    %points=5-----------------------------------------------------------------

    %determine formula for z=f(a_range) from given points of lotze, using
    %lsqcurvefit optimization
    z5=[149 67 36 15 8 3.4 2.4 2.4];
    a=[30 45 60 90 120 180 270 360];
    x0 = [100; -2];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,a,z5);
    z5=x(1).*a_range.^x(2);
%     x_add=[35 40 50 55 65 70 80 90 110 140 180];
%     z_add=x(1).*x_add.^x(2);
%     y_add=[5 5 5 5 5 5 5 5 5 5 5];

    %points=10-----------------------------------------------------------------

    %determine formula for z=f(a_range) from given points of lotze, using
    %lsqcurvefit optimization
    z10=[81 34 19 8 4 1.9 0.9 0.9];
    a=[30 45 60 90 120 180 270 360];
    x0 = [100; -2];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,a,z10);
    z10=x(1).*a_range.^x(2);
%     z_tmp=x(1).*x_add.^x(2);
%     z_add=[z_add z_tmp];
%     y_add=[y_add 10 10 10 10 10 10 10 10 10 10 10];

    %points=25-----------------------------------------------------------------

    %determine formula for z=f(a_range) from given points of lotze, using
    %lsqcurvefit optimization
    z25=[49 21 12 5 2.7 1.1 0.5 0.5];
    a=[30 45 60 90 120 180 270 360];
    x0 = [100; -2];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,a,z25);
    z25=x(1).*a_range.^x(2);
%     z_tmp=x(1).*x_add.^x(2);
%     z_add=[z_add z_tmp];
%     y_add=[y_add 25 25 25 25 25 25 25 25 25 25 25];

    %points=100-----------------------------------------------------------------

    %determine formula for z=f(a_range) from given points of lotze, using
    %lsqcurvefit optimization
    z100=[24 11 6 2.5 1.3 0.5 0.3 0.3];
    a=[30 45 60 90 120 180 270 360];
    x0 = [100; -2];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,a,z100);
    z100=x(1).*a_range.^x(2);
   % z_tmp=x(1).*x_add.^x(2);
   % z_add=[z_add z_tmp];
   % y_add=[y_add 100 100 100 100 100 100 100 100 100 100 100];

    %points=1000-----------------------------------------------------------------

    %determine formula for z=f(a_range) from given points of lotze, using
    %lsqcurvefit optimization
    z1000=[8 4 2 0.8 0.5 0.2 0.1 0.1];
    a=[30 45 60 90 120 180 270 360];
    x0 = [100; -2];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,a,z1000);
    z1000=x(1).*a_range.^x(2);

    %%%%%%%%%%%%%%%%%%%%%%%%
    z_first=[z5 z10 z25 z100 z1000];
    a_range1=[a_range a_range a_range a_range a_range];
    points1=[ones(1,31)*5 ones(1,31)*10 ones(1,31)*25 ones(1,31)*100 ones(1,31)*1000];
    %%%%%%%%%%%%%%%%%%%%%%%%
 
%{    
ty = 5:1:100; 
tx= 30:1:180;
[XI,YI] = meshgrid(tx,ty);
% find ZI with ' ' fitting method
ZI = griddata(a_range1,points1,z_first,XI,YI,'v4');
%}

%artificially create more values VERTICALLY (i.e. in the number of points) to add to lotze's matrix for measurement
%uncertainties

    points=[5:1:100];
    %angular range=30-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z30=[149 81 49 24 8];
    p=[5 10 25 100 1000];
    x0 = [10; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z30);
    z30=x(1).*points.^x(2);


    %angular range=45-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z45=[67 34 21 11 4];
    p=[5 10 25 100 1000];
    x0 = [100; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z45);
    z45=x(1).*points.^x(2);


    %angular range=60-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z60=[36 19 12 6 2];
    p=[5 10 25 100 1000];
    x0 = [100; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z60);
    z60=x(1).*points.^x(2);


    %angular range=90-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z90=[15 8 5 2.5 0.8];
    p=[5 10 25 100 1000];
    x0 = [100; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z90);
    z90=x(1).*points.^x(2);


    %angular range=120-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z120=[8 4 2.7 1.3 0.5];
    p=[5 10 25 100 1000];
    x0 = [100; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z120);
    z120=x(1).*points.^x(2);

    %angular range=180-----------------------------------------------------------------

    %determine formula for z=f(points) from given points of lotze, using
    %lsqcurvefit optimization
    z180=[3.4 1.9 1.1 0.5 0.2];
    p=[5 10 25 100 1000];
    x0 = [100; -1];% Starting guess
    [x,resnorm,residual] = lsqcurvefit(@powerfun,x0,p,z180);
    z180=x(1).*points.^x(2);
    
    z_second=[z30 z45 z60 z90 z120 z180];
    points2=[points points points points points points];
    a_range2=[ones(1,96)*30 ones(1,96)*45 ones(1,96)*60 ones(1,96)*90 ones(1,96)*120 ones(1,96)*180];
    
%x_add=[x_add x_add x_add];

%     z=[z_first z_second z_add];
%     points=[points1 points2 x_add];
%     a_range=[a_range1 a_range2 y_add];

    z=[z_first z_second];
    points=[points1 points2];
    a_range=[a_range1 a_range2];
    
    v=[a_range;points;z]';
    v=remove_doubllines(v);
    
    
    
ty = 5:1:100; 
tx= 30:1:180;
[XI,YI] = meshgrid(tx,ty);
% find ZI with ' ' fitting method
ZI = griddata(v(:,1),v(:,2),v(:,3),XI,YI,'v4');



%Plot the gridded data along with the nonuniform data points used to generate it: 
 mesh(XI,YI,ZI), hold on
 plot3(a_range2,points2,z_second,'.r'),  
% plot3(x_add,y_add,z_add,'.r');
 ylabel('number of points');
 xlabel('angular range of points');
 zlabel('uncertainty u_r');
 hold off